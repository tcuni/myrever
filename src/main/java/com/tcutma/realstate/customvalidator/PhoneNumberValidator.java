package com.tcutma.realstate.customvalidator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneNumberValidator implements
    ConstraintValidator<PhoneNumberConstraint, String> {

    @Override
    public void initialize(PhoneNumberConstraint contactNumber) {
    }

    @Override
    public boolean isValid(String phoneField,
                           ConstraintValidatorContext cxt) {
        return phoneField != null && phoneField.matches("[0-9]+")
            && (phoneField.length() > 8) && (phoneField.length() < 14);
    }

}
