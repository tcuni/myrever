package com.tcutma.realstate.domain.enumeration;

/**
 * The Direction enumeration.
 */
public enum Direction {
    EAST, WEST, NORTH, SOUTH, NORTHEAST, NORTHWEST, SOUTHEAST, SOUTHWEST
}
