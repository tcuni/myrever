package com.tcutma.realstate.domain.enumeration;

/**
 * The SupportType enumeration.
 */
public enum SupportType {
    ACCOUNT, SELL, RENT, BUY, HOUSE, PROJECT, FORRENT
}
