package com.tcutma.realstate.domain.enumeration;

/**
 * The ResourceType enumeration.
 */
public enum ResourceType {
    PROJECT, PROPERTY, EMPLOYEE, RESIDENTIAL_AREA, AVATAR,DRAFT
}
