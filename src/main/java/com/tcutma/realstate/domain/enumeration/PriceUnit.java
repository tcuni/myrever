package com.tcutma.realstate.domain.enumeration;

/**
 * The PriceUnit enumeration.
 */
public enum PriceUnit {
    MILLION_PER_M2, MILLION_PER_MONTH, MILLION_PER_FLAT, BILLION_PER_FLAT
}
