package com.tcutma.realstate.domain;

import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import com.tcutma.realstate.domain.enumeration.ResourceType;

/**
 * Photo - Hinh anh entity
 */
@ApiModel(description = "Photo - Hinh anh entity")
@Entity
@Table(name = "photo")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Photo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "src")
    private String src;

    @Column(name = "name")
    private String name;

    @Column(name = "thumbnail")
    private String thumbnail;

    @Column(name = "thumbnail_width")
    private Integer thumbnailWidth;

    @Column(name = "thumbnail_height")
    private Integer thumbnailHeight;

    @Column(name = "thumbnail_caption")
    private String thumbnailCaption;

    @Column(name = "orientation")
    private Integer orientation;

    @Column(name = "caption")
    private String caption;

    @Column(name = "tags")
    private String tags;

    @Column(name = "alt")
    private String alt;

    @Column(name = "resource_id")
    private Long resourceId;

    @Enumerated(EnumType.STRING)
    @Column(name = "resource_type")
    private ResourceType resourceType;

    @Column(name = "file_size")
    private Long fileSize;

    @Column(name = "resource_order")
    private Integer resourceOrder;

    @Column(name = "mime_type")
    private String mimeType;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSrc() {
        return src;
    }

    public Photo src(String src) {
        this.src = src;
        return this;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public String getName() {
        return name;
    }

    public Photo name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public Photo thumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
        return this;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public Integer getThumbnailWidth() {
        return thumbnailWidth;
    }

    public Photo thumbnailWidth(Integer thumbnailWidth) {
        this.thumbnailWidth = thumbnailWidth;
        return this;
    }

    public void setThumbnailWidth(Integer thumbnailWidth) {
        this.thumbnailWidth = thumbnailWidth;
    }

    public Integer getThumbnailHeight() {
        return thumbnailHeight;
    }

    public Photo thumbnailHeight(Integer thumbnailHeight) {
        this.thumbnailHeight = thumbnailHeight;
        return this;
    }

    public void setThumbnailHeight(Integer thumbnailHeight) {
        this.thumbnailHeight = thumbnailHeight;
    }

    public String getThumbnailCaption() {
        return thumbnailCaption;
    }

    public Photo thumbnailCaption(String thumbnailCaption) {
        this.thumbnailCaption = thumbnailCaption;
        return this;
    }

    public void setThumbnailCaption(String thumbnailCaption) {
        this.thumbnailCaption = thumbnailCaption;
    }

    public Integer getOrientation() {
        return orientation;
    }

    public Photo orientation(Integer orientation) {
        this.orientation = orientation;
        return this;
    }

    public void setOrientation(Integer orientation) {
        this.orientation = orientation;
    }

    public String getCaption() {
        return caption;
    }

    public Photo caption(String caption) {
        this.caption = caption;
        return this;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getTags() {
        return tags;
    }

    public Photo tags(String tags) {
        this.tags = tags;
        return this;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public String getAlt() {
        return alt;
    }

    public Photo alt(String alt) {
        this.alt = alt;
        return this;
    }

    public void setAlt(String alt) {
        this.alt = alt;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public Photo resourceId(Long resourceId) {
        this.resourceId = resourceId;
        return this;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public Photo resourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
        return this;
    }

    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    public Long getFileSize() {
        return fileSize;
    }

    public Photo fileSize(Long fileSize) {
        this.fileSize = fileSize;
        return this;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }

    public Integer getResourceOrder() {
        return resourceOrder;
    }

    public Photo resourceOrder(Integer resourceOrder) {
        this.resourceOrder = resourceOrder;
        return this;
    }

    public void setResourceOrder(Integer resourceOrder) {
        this.resourceOrder = resourceOrder;
    }

    public String getMimeType() {
        return mimeType;
    }

    public Photo mimeType(String mimeType) {
        this.mimeType = mimeType;
        return this;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Photo photo = (Photo) o;
        if (photo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), photo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Photo{" +
            "id=" + getId() +
            ", src='" + getSrc() + "'" +
            ", name='" + getName() + "'" +
            ", thumbnail='" + getThumbnail() + "'" +
            ", thumbnailWidth=" + getThumbnailWidth() +
            ", thumbnailHeight=" + getThumbnailHeight() +
            ", thumbnailCaption='" + getThumbnailCaption() + "'" +
            ", orientation=" + getOrientation() +
            ", caption='" + getCaption() + "'" +
            ", tags='" + getTags() + "'" +
            ", alt='" + getAlt() + "'" +
            ", resourceId=" + getResourceId() +
            ", resourceType='" + getResourceType() + "'" +
            ", fileSize=" + getFileSize() +
            ", resourceOrder=" + getResourceOrder() +
            ", mimeType='" + getMimeType() + "'" +
            "}";
    }
}
