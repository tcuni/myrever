package com.tcutma.realstate.domain;

import com.tcutma.realstate.config.Constants;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;

/**
 * Contractor - nha thau entity
 */
@ApiModel(description = "Contractor - nha thau entity")
@Entity
@Table(name = "contractor")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class Contractor implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Size(max = 128)
    @Column(name = "contractor_name", length = 128, nullable = false)
    private String contractorName;

    @Size(max = 256)
    @Column(name = "contractor_title", length = 256)
    private String contractorTitle;

    @Column(name = "contractor_date")
    @Past
    private LocalDate contractorDate;

    @Column(name = "contractor_description")
    private String contractorDescription;

    @Column(name = "contractor_address")
    private String contractorAddress;

    @Column(name = "contractor_website")
    private String contractorWebsite;

    @Size(max = 16)
    @Pattern(regexp = Constants.PHONENUMBER_PATTERN,message = "Please input a valid phone number")
    @Column(name = "contractor_phone", length = 16)
    private String contractorPhone;

    @Column(name = "contractor_avatar_url")
    private String contractorAvatarUrl;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContractorName() {
        return contractorName;
    }

    public Contractor contractorName(String contractorName) {
        this.contractorName = contractorName;
        return this;
    }

    public void setContractorName(String contractorName) {
        this.contractorName = contractorName;
    }

    public String getContractorTitle() {
        return contractorTitle;
    }

    public Contractor contractorTitle(String contractorTitle) {
        this.contractorTitle = contractorTitle;
        return this;
    }

    public void setContractorTitle(String contractorTitle) {
        this.contractorTitle = contractorTitle;
    }

    public LocalDate getContractorDate() {
        return contractorDate;
    }

    public Contractor contractorDate(LocalDate contractorDate) {
        this.contractorDate = contractorDate;
        return this;
    }

    public void setContractorDate(LocalDate contractorDate) {
        this.contractorDate = contractorDate;
    }

    public String getContractorDescription() {
        return contractorDescription;
    }

    public Contractor contractorDescription(String contractorDescription) {
        this.contractorDescription = contractorDescription;
        return this;
    }

    public void setContractorDescription(String contractorDescription) {
        this.contractorDescription = contractorDescription;
    }

    public String getContractorAddress() {
        return contractorAddress;
    }

    public Contractor contractorAddress(String contractorAddress) {
        this.contractorAddress = contractorAddress;
        return this;
    }

    public void setContractorAddress(String contractorAddress) {
        this.contractorAddress = contractorAddress;
    }

    public String getContractorWebsite() {
        return contractorWebsite;
    }

    public Contractor contractorWebsite(String contractorWebsite) {
        this.contractorWebsite = contractorWebsite;
        return this;
    }

    public void setContractorWebsite(String contractorWebsite) {
        this.contractorWebsite = contractorWebsite;
    }

    public String getContractorPhone() {
        return contractorPhone;
    }

    public Contractor contractorPhone(String contractorPhone) {
        this.contractorPhone = contractorPhone;
        return this;
    }

    public void setContractorPhone(String contractorPhone) {
        this.contractorPhone = contractorPhone;
    }

    public String getContractorAvatarUrl() {
        return contractorAvatarUrl;
    }

    public Contractor contractorAvatarUrl(String contractorAvatarUrl) {
        this.contractorAvatarUrl = contractorAvatarUrl;
        return this;
    }

    public void setContractorAvatarUrl(String contractorAvatarUrl) {
        this.contractorAvatarUrl = contractorAvatarUrl;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Contractor contractor = (Contractor) o;
        if (contractor.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), contractor.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Contractor{" +
            "id=" + getId() +
            ", contractorName='" + getContractorName() + "'" +
            ", contractorTitle='" + getContractorTitle() + "'" +
            ", contractorDate='" + getContractorDate() + "'" +
            ", contractorDescription='" + getContractorDescription() + "'" +
            ", contractorAddress='" + getContractorAddress() + "'" +
            ", contractorWebsite='" + getContractorWebsite() + "'" +
            ", contractorPhone='" + getContractorPhone() + "'" +
            ", contractorAvatarUrl='" + getContractorAvatarUrl() + "'" +
            "}";
    }
}
