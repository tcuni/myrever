package com.tcutma.realstate.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.util.Objects;

/**
 * UserNotification entity
 */
@ApiModel(description = "UserNotification entity")
@Entity
@Table(name = "user_notification")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class UserNotification implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "is_seen")
    private Boolean isSeen;

    @Column(name = "seen_date")
    private Instant seenDate;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Notification notification;

    @ManyToOne
    @JsonIgnoreProperties("")
    private User receiver;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean isIsSeen() {
        return isSeen;
    }

    public UserNotification isSeen(Boolean isSeen) {
        this.isSeen = isSeen;
        return this;
    }

    public void setIsSeen(Boolean isSeen) {
        this.isSeen = isSeen;
    }

    public Instant getSeenDate() {
        return seenDate;
    }

    public UserNotification seenDate(Instant seenDate) {
        this.seenDate = seenDate;
        return this;
    }

    public void setSeenDate(Instant seenDate) {
        this.seenDate = seenDate;
    }

    public Notification getNotification() {
        return notification;
    }

    public UserNotification notification(Notification notification) {
        this.notification = notification;
        return this;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    public User getReceiver() {
        return receiver;
    }

    public UserNotification receiver(User user) {
        this.receiver = user;
        return this;
    }

    public void setReceiver(User user) {
        this.receiver = user;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserNotification userNotification = (UserNotification) o;
        if (userNotification.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), userNotification.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "UserNotification{" +
            "id=" + getId() +
            ", isSeen='" + isIsSeen() + "'" +
            ", seenDate='" + getSeenDate() + "'" +
            "}";
    }
}
