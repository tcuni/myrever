package com.tcutma.realstate.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcutma.realstate.domain.Exchanger;
import com.tcutma.realstate.repository.ExchangerRepository;
import com.tcutma.realstate.web.rest.errors.BadRequestAlertException;
import com.tcutma.realstate.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Exchanger.
 */
@RestController
@RequestMapping("/api/v1")
public class ExchangerResource {

    private final Logger log = LoggerFactory.getLogger(ExchangerResource.class);

    private static final String ENTITY_NAME = "exchanger";

    private final ExchangerRepository exchangerRepository;

    public ExchangerResource(ExchangerRepository exchangerRepository) {
        this.exchangerRepository = exchangerRepository;
    }

    /**
     * POST  /exchangers : Create a new exchanger.
     *
     * @param exchanger the exchanger to create
     * @return the ResponseEntity with status 201 (Created) and with body the new exchanger, or with status 400 (Bad Request) if the exchanger has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/exchangers")
    @Timed
    public ResponseEntity<Exchanger> createExchanger(@Valid @RequestBody Exchanger exchanger) throws URISyntaxException {
        log.debug("REST request to save Exchanger : {}", exchanger);
        if (exchanger.getId() != null) {
            throw new BadRequestAlertException("A new exchanger cannot already have an ID", ENTITY_NAME, "idexists");
        }
        Exchanger result = exchangerRepository.save(exchanger);
        return ResponseEntity.created(new URI("/api/exchangers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /exchangers : Updates an existing exchanger.
     *
     * @param exchanger the exchanger to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated exchanger,
     * or with status 400 (Bad Request) if the exchanger is not valid,
     * or with status 500 (Internal Server Error) if the exchanger couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/exchangers")
    @Timed
    public ResponseEntity<Exchanger> updateExchanger(@Valid @RequestBody Exchanger exchanger) throws URISyntaxException {
        log.debug("REST request to update Exchanger : {}", exchanger);
        if (exchanger.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        Exchanger result = exchangerRepository.save(exchanger);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, exchanger.getId().toString()))
            .body(result);
    }

    /**
     * GET  /exchangers : get all the exchangers.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of exchangers in body
     */
    @GetMapping("/exchangers")
    @Timed
    public List<Exchanger> getAllExchangers() {
        log.debug("REST request to get all Exchangers");
        return exchangerRepository.findAll();
    }

    /**
     * GET  /exchangers/:id : get the "id" exchanger.
     *
     * @param id the id of the exchanger to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the exchanger, or with status 404 (Not Found)
     */
    @GetMapping("/exchangers/{id}")
    @Timed
    public ResponseEntity<Exchanger> getExchanger(@PathVariable Long id) {
        log.debug("REST request to get Exchanger : {}", id);
        Optional<Exchanger> exchanger = exchangerRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(exchanger);
    }

    /**
     * DELETE  /exchangers/:id : delete the "id" exchanger.
     *
     * @param id the id of the exchanger to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/exchangers/{id}")
    @Timed
    public ResponseEntity<Void> deleteExchanger(@PathVariable Long id) {
        log.debug("REST request to delete Exchanger : {}", id);

        exchangerRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
