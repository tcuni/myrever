package com.tcutma.realstate.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcutma.realstate.config.Constants;
import com.tcutma.realstate.domain.ResidentialArea;
import com.tcutma.realstate.domain.Tag;
import com.tcutma.realstate.repository.TagRepository;
import com.tcutma.realstate.service.ResidentialAreaService;
import com.tcutma.realstate.web.rest.errors.BadRequestAlertException;
import com.tcutma.realstate.web.rest.util.HeaderUtil;
import com.tcutma.realstate.web.rest.util.PaginationUtil;
import com.tcutma.realstate.service.dto.ResidentialAreaCriteria;
import com.tcutma.realstate.service.ResidentialAreaQueryService;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.*;
import java.util.stream.Collectors;

/**
 * REST controller for managing ResidentialArea.
 */
@RestController
@RequestMapping("/api/v1")
public class ResidentialAreaResource {

    private final Logger log = LoggerFactory.getLogger(ResidentialAreaResource.class);

    private static final String ENTITY_NAME = "residentialArea";

    private final ResidentialAreaService residentialAreaService;


    private final ResidentialAreaQueryService residentialAreaQueryService;

    public ResidentialAreaResource(ResidentialAreaService residentialAreaService, ResidentialAreaQueryService residentialAreaQueryService) {
        this.residentialAreaService = residentialAreaService;
        this.residentialAreaQueryService = residentialAreaQueryService;
    }

    /**
     * POST  /residential-areas : Create a new residentialArea.
     *
     * @param residentialArea the residentialArea to create
     * @return the ResponseEntity with status 201 (Created) and with body the new residentialArea, or with status 400 (Bad Request) if the residentialArea has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/residential-areas")
    @Timed
    public ResponseEntity<ResidentialArea> createResidentialArea(@Valid @RequestBody ResidentialArea residentialArea) throws URISyntaxException {
        log.debug("REST request to save ResidentialArea : {}", residentialArea);
        if (residentialArea.getId() != null) {
            throw new BadRequestAlertException("A new residentialArea cannot already have an ID", ENTITY_NAME, "idexists");
        }
        //residentialArea = processTags(residentialArea);
        ResidentialArea result = residentialAreaService.save(residentialArea);
        return ResponseEntity.created(new URI("/api/residential-areas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /residential-areas : Updates an existing residentialArea.
     *
     * @param residentialArea the residentialArea to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated residentialArea,
     * or with status 400 (Bad Request) if the residentialArea is not valid,
     * or with status 500 (Internal Server Error) if the residentialArea couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/residential-areas")
    @Timed
    public ResponseEntity<ResidentialArea> updateResidentialArea(@Valid @RequestBody ResidentialArea residentialArea) throws URISyntaxException {
        log.debug("REST request to update ResidentialArea : {}", residentialArea);
        if (residentialArea.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        //residentialArea = processTags(residentialArea);
        ResidentialArea result = residentialAreaService.save(residentialArea);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, residentialArea.getId().toString()))
            .body(result);
    }

    /**
     * GET  /residential-areas : get all the residentialAreas.
     *
     * @param pageable the pagination information
     * @param criteria the criterias which the requested entities should match
     * @return the ResponseEntity with status 200 (OK) and the list of residentialAreas in body
     */
    @GetMapping("/residential-areas")
    @Timed
    public ResponseEntity<List<ResidentialArea>> getAllResidentialAreas(ResidentialAreaCriteria criteria, Pageable pageable) {
        log.debug("REST request to get ResidentialAreas by criteria: {}", criteria);
        Page<ResidentialArea> page = residentialAreaQueryService.findByCriteria(criteria, pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/residential-areas");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /residential-areas/:id : get the "id" residentialArea.
     *
     * @param id the id of the residentialArea to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the residentialArea, or with status 404 (Not Found)
     */
    @GetMapping("/residential-areas/{id}")
    @Timed
    public ResponseEntity<ResidentialArea> getResidentialArea(@PathVariable Long id) {
        log.debug("REST request to get ResidentialArea : {}", id);
        Optional<ResidentialArea> residentialArea = residentialAreaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(residentialArea);
    }

    /**
     * DELETE  /residential-areas/:id : delete the "id" residentialArea.
     *
     * @param id the id of the residentialArea to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/residential-areas/{id}")
    @Timed
    public ResponseEntity<Void> deleteResidentialArea(@PathVariable Long id) {
        log.debug("REST request to delete ResidentialArea : {}", id);
        residentialAreaService.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
    /*
    public ResidentialArea processTags(ResidentialArea residentialArea){
        Set<Tag> tags =
            residentialArea.getTags().stream()
                .map(tag -> {

                    // if tag id exist then don't modify it
                    if(tag.getId()!=null) return tag;
                    // Get tagname
                    String tagName =tag.getTagName() ;

                    // Truncate tagName
                    tag.setTagName(tagName.substring(0,Math.min(tagName.length(),Constants.NAME_LENGTH)));

                    // Find tag by tagName
                    Optional<Tag> opTag = tagRepository.findOneByTagName(tag.getTagName());

                    log.info("after get tag: {}",opTag);

                    // Create new tag if not exist
                    return opTag.orElseGet(()->tagRepository.save(tag));
                })
                //.filter(tag-> Objects.nonNull(tag.getId()))
                .collect(Collectors.toSet());
        residentialArea.setTags(tags);
        log.info("And all tags are: {}",residentialArea.getTags());
        return  residentialArea;
    }
    */
}
