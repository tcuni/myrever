package com.tcutma.realstate.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcutma.realstate.domain.JobTitle;
import com.tcutma.realstate.repository.JobTitleRepository;
import com.tcutma.realstate.web.rest.errors.BadRequestAlertException;
import com.tcutma.realstate.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing JobTitle.
 */
@RestController
@RequestMapping("/api/v1")
public class JobTitleResource {

    private final Logger log = LoggerFactory.getLogger(JobTitleResource.class);

    private static final String ENTITY_NAME = "jobTitle";

    private final JobTitleRepository jobTitleRepository;

    public JobTitleResource(JobTitleRepository jobTitleRepository) {
        this.jobTitleRepository = jobTitleRepository;
    }

    /**
     * POST  /job-titles : Create a new jobTitle.
     *
     * @param jobTitle the jobTitle to create
     * @return the ResponseEntity with status 201 (Created) and with body the new jobTitle, or with status 400 (Bad Request) if the jobTitle has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/job-titles")
    @Timed
    public ResponseEntity<JobTitle> createJobTitle(@Valid @RequestBody JobTitle jobTitle) throws URISyntaxException {
        log.debug("REST request to save JobTitle : {}", jobTitle);
        if (jobTitle.getId() != null) {
            throw new BadRequestAlertException("A new jobTitle cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JobTitle result = jobTitleRepository.save(jobTitle);
        return ResponseEntity.created(new URI("/api/job-titles/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /job-titles : Updates an existing jobTitle.
     *
     * @param jobTitle the jobTitle to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated jobTitle,
     * or with status 400 (Bad Request) if the jobTitle is not valid,
     * or with status 500 (Internal Server Error) if the jobTitle couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/job-titles")
    @Timed
    public ResponseEntity<JobTitle> updateJobTitle(@Valid @RequestBody JobTitle jobTitle) throws URISyntaxException {
        log.debug("REST request to update JobTitle : {}", jobTitle);
        if (jobTitle.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JobTitle result = jobTitleRepository.save(jobTitle);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, jobTitle.getId().toString()))
            .body(result);
    }

    /**
     * GET  /job-titles : get all the jobTitles.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of jobTitles in body
     */
    @GetMapping("/job-titles")
    @Timed
    public List<JobTitle> getAllJobTitles() {
        log.debug("REST request to get all JobTitles");
        return jobTitleRepository.findAll();
    }

    /**
     * GET  /job-titles/:id : get the "id" jobTitle.
     *
     * @param id the id of the jobTitle to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the jobTitle, or with status 404 (Not Found)
     */
    @GetMapping("/job-titles/{id}")
    @Timed
    public ResponseEntity<JobTitle> getJobTitle(@PathVariable Long id) {
        log.debug("REST request to get JobTitle : {}", id);
        Optional<JobTitle> jobTitle = jobTitleRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(jobTitle);
    }

    /**
     * DELETE  /job-titles/:id : delete the "id" jobTitle.
     *
     * @param id the id of the jobTitle to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/job-titles/{id}")
    @Timed
    public ResponseEntity<Void> deleteJobTitle(@PathVariable Long id) {
        log.debug("REST request to delete JobTitle : {}", id);

        jobTitleRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
