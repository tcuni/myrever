package com.tcutma.realstate.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcutma.realstate.domain.FavouriteItem;
import com.tcutma.realstate.repository.FavouriteItemRepository;
import com.tcutma.realstate.web.rest.errors.BadRequestAlertException;
import com.tcutma.realstate.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing FavouriteItem.
 */
@RestController
@RequestMapping("/api/v1")
public class FavouriteItemResource {

    private final Logger log = LoggerFactory.getLogger(FavouriteItemResource.class);

    private static final String ENTITY_NAME = "favouriteItem";

    private final FavouriteItemRepository favouriteItemRepository;

    public FavouriteItemResource(FavouriteItemRepository favouriteItemRepository) {
        this.favouriteItemRepository = favouriteItemRepository;
    }

    /**
     * POST  /favourite-items : Create a new favouriteItem.
     *
     * @param favouriteItem the favouriteItem to create
     * @return the ResponseEntity with status 201 (Created) and with body the new favouriteItem, or with status 400 (Bad Request) if the favouriteItem has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/favourite-items")
    @Timed
    public ResponseEntity<FavouriteItem> createFavouriteItem(@RequestBody FavouriteItem favouriteItem) throws URISyntaxException {
        log.debug("REST request to save FavouriteItem : {}", favouriteItem);
        if (favouriteItem.getId() != null) {
            throw new BadRequestAlertException("A new favouriteItem cannot already have an ID", ENTITY_NAME, "idexists");
        }
        FavouriteItem result = favouriteItemRepository.save(favouriteItem);
        return ResponseEntity.created(new URI("/api/favourite-items/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /favourite-items : Updates an existing favouriteItem.
     *
     * @param favouriteItem the favouriteItem to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated favouriteItem,
     * or with status 400 (Bad Request) if the favouriteItem is not valid,
     * or with status 500 (Internal Server Error) if the favouriteItem couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/favourite-items")
    @Timed
    public ResponseEntity<FavouriteItem> updateFavouriteItem(@RequestBody FavouriteItem favouriteItem) throws URISyntaxException {
        log.debug("REST request to update FavouriteItem : {}", favouriteItem);
        if (favouriteItem.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        FavouriteItem result = favouriteItemRepository.save(favouriteItem);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, favouriteItem.getId().toString()))
            .body(result);
    }

    /**
     * GET  /favourite-items : get all the favouriteItems.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of favouriteItems in body
     */
    @GetMapping("/favourite-items")
    @Timed
    public List<FavouriteItem> getAllFavouriteItems() {
        log.debug("REST request to get all FavouriteItems");
        return favouriteItemRepository.findAll();
    }

    /**
     * GET  /favourite-items/:id : get the "id" favouriteItem.
     *
     * @param id the id of the favouriteItem to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the favouriteItem, or with status 404 (Not Found)
     */
    @GetMapping("/favourite-items/{id}")
    @Timed
    public ResponseEntity<FavouriteItem> getFavouriteItem(@PathVariable Long id) {
        log.debug("REST request to get FavouriteItem : {}", id);
        Optional<FavouriteItem> favouriteItem = favouriteItemRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(favouriteItem);
    }

    /**
     * DELETE  /favourite-items/:id : delete the "id" favouriteItem.
     *
     * @param id the id of the favouriteItem to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/favourite-items/{id}")
    @Timed
    public ResponseEntity<Void> deleteFavouriteItem(@PathVariable Long id) {
        log.debug("REST request to delete FavouriteItem : {}", id);

        favouriteItemRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
