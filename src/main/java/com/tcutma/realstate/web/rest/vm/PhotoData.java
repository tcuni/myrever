package com.tcutma.realstate.web.rest.vm;

import com.tcutma.realstate.domain.enumeration.ResourceType;
import org.springframework.web.multipart.MultipartFile;

public class PhotoData {

    private String caption;
    private MultipartFile file;
    private ResourceType resourceType;
    private Long resourceId;
    private String thumbnailCaption;
    private String tags;
    private Integer order;

    public PhotoData() {
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public ResourceType getResourceType() {
        return resourceType;
    }

    public void setResourceType(ResourceType resourceType) {
        this.resourceType = resourceType;
    }

    public Long getResourceId() {
        return resourceId;
    }

    public void setResourceId(Long resourceId) {
        this.resourceId = resourceId;
    }

    public String getThumbnailCaption() {
        return thumbnailCaption;
    }

    public void setThumbnailCaption(String thumbnailCaption) {
        this.thumbnailCaption = thumbnailCaption;
    }

    public String getTags() {
        return tags;
    }

    public void setTags(String tags) {
        this.tags = tags;
    }

    public Integer getOrder() {
        return order;
    }

    public void setOrder(Integer order) {
        this.order = order;
    }
}
