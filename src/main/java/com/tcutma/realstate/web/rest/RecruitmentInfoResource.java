package com.tcutma.realstate.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcutma.realstate.domain.RecruitmentInfo;
import com.tcutma.realstate.repository.RecruitmentInfoRepository;
import com.tcutma.realstate.web.rest.errors.BadRequestAlertException;
import com.tcutma.realstate.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing RecruitmentInfo.
 */
@RestController
@RequestMapping("/api/v1")
public class RecruitmentInfoResource {

    private final Logger log = LoggerFactory.getLogger(RecruitmentInfoResource.class);

    private static final String ENTITY_NAME = "recruitmentInfo";

    private final RecruitmentInfoRepository recruitmentInfoRepository;

    public RecruitmentInfoResource(RecruitmentInfoRepository recruitmentInfoRepository) {
        this.recruitmentInfoRepository = recruitmentInfoRepository;
    }

    /**
     * POST  /recruitment-infos : Create a new recruitmentInfo.
     *
     * @param recruitmentInfo the recruitmentInfo to create
     * @return the ResponseEntity with status 201 (Created) and with body the new recruitmentInfo, or with status 400 (Bad Request) if the recruitmentInfo has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/recruitment-infos")
    @Timed
    public ResponseEntity<RecruitmentInfo> createRecruitmentInfo(@Valid @RequestBody RecruitmentInfo recruitmentInfo) throws URISyntaxException {
        log.debug("REST request to save RecruitmentInfo : {}", recruitmentInfo);
        if (recruitmentInfo.getId() != null) {
            throw new BadRequestAlertException("A new recruitmentInfo cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RecruitmentInfo result = recruitmentInfoRepository.save(recruitmentInfo);
        return ResponseEntity.created(new URI("/api/recruitment-infos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /recruitment-infos : Updates an existing recruitmentInfo.
     *
     * @param recruitmentInfo the recruitmentInfo to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated recruitmentInfo,
     * or with status 400 (Bad Request) if the recruitmentInfo is not valid,
     * or with status 500 (Internal Server Error) if the recruitmentInfo couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/recruitment-infos")
    @Timed
    public ResponseEntity<RecruitmentInfo> updateRecruitmentInfo(@Valid @RequestBody RecruitmentInfo recruitmentInfo) throws URISyntaxException {
        log.debug("REST request to update RecruitmentInfo : {}", recruitmentInfo);
        if (recruitmentInfo.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RecruitmentInfo result = recruitmentInfoRepository.save(recruitmentInfo);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, recruitmentInfo.getId().toString()))
            .body(result);
    }

    /**
     * GET  /recruitment-infos : get all the recruitmentInfos.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of recruitmentInfos in body
     */
    @GetMapping("/recruitment-infos")
    @Timed
    public List<RecruitmentInfo> getAllRecruitmentInfos() {
        log.debug("REST request to get all RecruitmentInfos");
        return recruitmentInfoRepository.findAll();
    }

    /**
     * GET  /recruitment-infos/:id : get the "id" recruitmentInfo.
     *
     * @param id the id of the recruitmentInfo to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the recruitmentInfo, or with status 404 (Not Found)
     */
    @GetMapping("/recruitment-infos/{id}")
    @Timed
    public ResponseEntity<RecruitmentInfo> getRecruitmentInfo(@PathVariable Long id) {
        log.debug("REST request to get RecruitmentInfo : {}", id);
        Optional<RecruitmentInfo> recruitmentInfo = recruitmentInfoRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(recruitmentInfo);
    }

    /**
     * DELETE  /recruitment-infos/:id : delete the "id" recruitmentInfo.
     *
     * @param id the id of the recruitmentInfo to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/recruitment-infos/{id}")
    @Timed
    public ResponseEntity<Void> deleteRecruitmentInfo(@PathVariable Long id) {
        log.debug("REST request to delete RecruitmentInfo : {}", id);

        recruitmentInfoRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
