package com.tcutma.realstate.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.tcutma.realstate.domain.SupportCategory;
import com.tcutma.realstate.repository.SupportCategoryRepository;
import com.tcutma.realstate.web.rest.errors.BadRequestAlertException;
import com.tcutma.realstate.web.rest.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;

import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing SupportCategory.
 */
@RestController
@RequestMapping("/api/v1")
public class SupportCategoryResource {

    private final Logger log = LoggerFactory.getLogger(SupportCategoryResource.class);

    private static final String ENTITY_NAME = "supportCategory";

    private final SupportCategoryRepository supportCategoryRepository;

    public SupportCategoryResource(SupportCategoryRepository supportCategoryRepository) {
        this.supportCategoryRepository = supportCategoryRepository;
    }

    /**
     * POST  /support-categories : Create a new supportCategory.
     *
     * @param supportCategory the supportCategory to create
     * @return the ResponseEntity with status 201 (Created) and with body the new supportCategory, or with status 400 (Bad Request) if the supportCategory has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/support-categories")
    @Timed
    public ResponseEntity<SupportCategory> createSupportCategory(@Valid @RequestBody SupportCategory supportCategory) throws URISyntaxException {
        log.debug("REST request to save SupportCategory : {}", supportCategory);
        if (supportCategory.getId() != null) {
            throw new BadRequestAlertException("A new supportCategory cannot already have an ID", ENTITY_NAME, "idexists");
        }
        SupportCategory result = supportCategoryRepository.save(supportCategory);
        return ResponseEntity.created(new URI("/api/support-categories/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /support-categories : Updates an existing supportCategory.
     *
     * @param supportCategory the supportCategory to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated supportCategory,
     * or with status 400 (Bad Request) if the supportCategory is not valid,
     * or with status 500 (Internal Server Error) if the supportCategory couldn't be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/support-categories")
    @Timed
    public ResponseEntity<SupportCategory> updateSupportCategory(@Valid @RequestBody SupportCategory supportCategory) throws URISyntaxException {
        log.debug("REST request to update SupportCategory : {}", supportCategory);
        if (supportCategory.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        SupportCategory result = supportCategoryRepository.save(supportCategory);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, supportCategory.getId().toString()))
            .body(result);
    }

    /**
     * GET  /support-categories : get all the supportCategories.
     *
     * @return the ResponseEntity with status 200 (OK) and the list of supportCategories in body
     */
    @GetMapping("/support-categories")
    @Timed
    public List<SupportCategory> getAllSupportCategories() {
        log.debug("REST request to get all SupportCategories");
        return supportCategoryRepository.findAll();
    }

    /**
     * GET  /support-categories/:id : get the "id" supportCategory.
     *
     * @param id the id of the supportCategory to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the supportCategory, or with status 404 (Not Found)
     */
    @GetMapping("/support-categories/{id}")
    @Timed
    public ResponseEntity<SupportCategory> getSupportCategory(@PathVariable Long id) {
        log.debug("REST request to get SupportCategory : {}", id);
        Optional<SupportCategory> supportCategory = supportCategoryRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(supportCategory);
    }

    /**
     * DELETE  /support-categories/:id : delete the "id" supportCategory.
     *
     * @param id the id of the supportCategory to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/support-categories/{id}")
    @Timed
    public ResponseEntity<Void> deleteSupportCategory(@PathVariable Long id) {
        log.debug("REST request to delete SupportCategory : {}", id);

        supportCategoryRepository.deleteById(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }
}
