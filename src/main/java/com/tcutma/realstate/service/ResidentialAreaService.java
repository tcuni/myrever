package com.tcutma.realstate.service;

import com.tcutma.realstate.domain.ResidentialArea;
import com.tcutma.realstate.repository.ResidentialAreaRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.Optional;
/**
 * Service Implementation for managing ResidentialArea.
 */
@Service
@Transactional
public class ResidentialAreaService {

    private final Logger log = LoggerFactory.getLogger(ResidentialAreaService.class);

    private final ResidentialAreaRepository residentialAreaRepository;

    private final TagService tagService;

    public ResidentialAreaService(ResidentialAreaRepository residentialAreaRepository, TagService tagService) {
        this.residentialAreaRepository = residentialAreaRepository;
        this.tagService = tagService;
    }

    /**
     * Save a residentialArea.
     *
     * @param residentialArea the entity to save
     * @return the persisted entity
     */
    public ResidentialArea save(ResidentialArea residentialArea) {
        log.debug("Request to save ResidentialArea : {}", residentialArea);
        residentialArea.setTags(tagService.processTags(residentialArea.getTags()));
        return residentialAreaRepository.save(residentialArea);
    }

    /**
     * Get all the residentialAreas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<ResidentialArea> findAll(Pageable pageable) {
        log.debug("Request to get all ResidentialAreas");
        return residentialAreaRepository.findAll(pageable);
    }

    /**
     * Get all the ResidentialArea with eager load of many-to-many relationships.
     *
     * @return the list of entities
     */
    public Page<ResidentialArea> findAllWithEagerRelationships(Pageable pageable) {
        return residentialAreaRepository.findAllWithEagerRelationships(pageable);
    }


    /**
     * Get one residentialArea by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<ResidentialArea> findOne(Long id) {
        log.debug("Request to get ResidentialArea : {}", id);
        return residentialAreaRepository.findOneWithEagerRelationships(id);
    }

    /**
     * Delete the residentialArea by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete ResidentialArea : {}", id);
        residentialAreaRepository.deleteById(id);
    }
}
