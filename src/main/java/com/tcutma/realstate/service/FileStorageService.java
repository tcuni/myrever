package com.tcutma.realstate.service;

import com.tcutma.realstate.config.ApplicationProperties;
import com.tcutma.realstate.domain.Document;
import com.tcutma.realstate.domain.Photo;
import com.tcutma.realstate.domain.enumeration.UploadType;
import com.tcutma.realstate.exception.FileStorageException;
import com.tcutma.realstate.exception.MyFileNotFoundException;
import com.tcutma.realstate.repository.DocumentRepository;
import com.tcutma.realstate.repository.PhotoRepository;
import com.tcutma.realstate.web.rest.vm.DocumentData;
import com.tcutma.realstate.web.rest.vm.PhotoData;
import com.tcutma.realstate.web.rest.vm.UploadFileResponse;

import org.apache.commons.io.FilenameUtils;
import org.imgscalr.Scalr;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ImagingOpException;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;
import java.util.stream.Stream;

@Service
@Transactional
public class FileStorageService{

    private final Logger log = LoggerFactory.getLogger(FileStorageService.class);

    private final Path fileLocation;

    private  final Path photoLocation;

    private final Path documentLocation;

    private final Path thumbnailLocation;

    private final PhotoRepository photoRepository; // my added code

    private final DocumentRepository documentRepository;

    @Autowired
    public FileStorageService(ApplicationProperties properties, PhotoRepository photoRepository, DocumentRepository documentRepository) {
        this.fileLocation = Paths.get(properties.getUploadDir());
        this.photoLocation = Paths.get(properties.getPhotoDir());
        this.documentLocation = Paths.get(properties.getDocumentDir());
        this.thumbnailLocation = Paths.get(properties.getThumbDir());
        this.photoRepository = photoRepository;
        this.documentRepository = documentRepository;
    }


    public UploadFileResponse storeFile(UploadType type, MultipartFile file) {

        // Get root path belong to file type
        Path rootLocation = type==UploadType.FILE?fileLocation:type==UploadType.PHOTO?photoLocation:documentLocation;

        // Get file path for uri
        String typePath = type==UploadType.THUMBNAIL?"3/":type==UploadType.PHOTO?"1/":"2/";

        //Normalize file name
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());

        // get current time stamp
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());

        // Convert current timestamp to String
        String currTimeStamp = String.valueOf(timestamp.getTime());

        // Create file name contain timestamp
        fileName = currTimeStamp +"-"+ fileName;

        // Create file download uri from filename
        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
            .path("/api/v1/files/")
            .path(typePath)
            .path("/")
            .path(fileName)
            .toUriString();

        try {

            // Check if file is empty
            if(file.isEmpty()){
                throw new FileStorageException("Failed to store empty file "+ fileName);
            }

            // Check if the file's name contains invalid characters
            if (fileName.contains("..")){
                throw  new FileStorageException("File name contains invalid characters" +fileName);
            }

            // Get target location
            //Path targetLocation = fileStorageLocation.resolve(fileName);

            // Copy file to the target location (Replacing exist file with the same name)
            try(InputStream inputStream = file.getInputStream()){

                Files.copy(inputStream, rootLocation.resolve(fileName), StandardCopyOption.REPLACE_EXISTING);
            }

            String fileThumbUri = "";
            // return filename, fileDownloadUri, contentType, fileSize



            // if uploaded photo
            if (type==UploadType.PHOTO){

                // Get path to image file
                Path filePath = loadFile(type,fileName);

                // Create new file from image file path
                File image = new File(filePath.toString());

                // Load image file into buffer
                BufferedImage img = ImageIO.read(image); // load image

                // resize to 128 pixels max
                BufferedImage thumbnail = Scalr.resize(img, 128);

                // Create path to thumbnail folder
                String thumbFile = thumbnailLocation.resolve(fileName).toString();
                log.info("Duong dan den thumbnail: {} ",thumbFile);

                // Write thumbnail image to thumbanail folder
                ImageIO.write(thumbnail,"jpg",new File(thumbFile));
/*
                BufferedImage thumbnail = Scalr.resize(filePath.,
                    Scalr.Method.SPEED,
                    Scalr.Mode.FIT_TO_WIDTH,
                    150,
                    100,
                    Scalr.OP_ANTIALIAS);

                // convert upload file response to Photo
                Photo photo = PhotoMapper.INSTANCE.toPhoto(ufr);

                // Create new photo entity
                photo = photoRepository.save(photo);

                // map photo to ufr
                ufr = PhotoMapper.INSTANCE.toResponse(photo);

    */

                // Create thumbnail download uri from filename
                fileThumbUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path("/api/v1/files/")
                    .path("3/")
                    .path(fileName)
                    .toUriString();
            } else if(type==UploadType.DOCUMENT){
                // Get file extension
                String ext = FilenameUtils.getExtension(fileName).toLowerCase();
                String pathToThumbnail = (ext.contains("zip") || ext.contains("rar")) ? "/content/images/zip-documents.svg": "/content/images/documents.svg";
                // Create thumbnail download uri from filename
                fileThumbUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                    .path(pathToThumbnail)
                    .toUriString();
            }
            UploadFileResponse ufr = new UploadFileResponse(fileName, fileDownloadUri,
                fileThumbUri, file.getContentType(),file.getSize());

            return  ufr;

        }catch(IOException|IllegalArgumentException | ImagingOpException ex){

            ex.printStackTrace();
            throw  new FileStorageException("Could not store file "+ fileName + "Please try again",ex);
        }
    }


    public Resource loadFileAsResource(UploadType type, String fileName) {
        try{

            // Get file path
            Path file = loadFile(type,fileName);
            log.debug("After load file type {} and filename: {}",type,fileName);

            // Get resource
            Resource resource = new UrlResource(file.toUri());
            log.debug("And resource of the file: {}",resource);

            if (resource.exists() || resource.isReadable()){

                return resource;
            }else{

                throw new MyFileNotFoundException("Could not read file " + fileName);
            }
        } catch (MalformedURLException ex){

            throw new MyFileNotFoundException("File not found "+ fileName, ex);
        }
    }


    public Stream<Path> loadAllFiles(UploadType type) {
        Path rootLocation = type==UploadType.FILE?fileLocation:type==UploadType.PHOTO?photoLocation:documentLocation;
        log.debug("View all uploaded {} from: {}",type,rootLocation);
        try{
            return Files.walk(rootLocation,1)
                .filter(path->!path.equals(rootLocation))
                .map(rootLocation::relativize);
        } catch (IOException e){
            throw  new FileStorageException("Failed to read stored files ",e);
        }
    }


    public Path loadFile(UploadType type, String fileName) {
        Path rootLocation = type==UploadType.THUMBNAIL?thumbnailLocation:type==UploadType.PHOTO?photoLocation:documentLocation;
        return rootLocation.resolve(fileName);
    }


    public void deleteAllFiles() {
        FileSystemUtils.deleteRecursively(fileLocation.toFile());
    }

    /**
     *  Create upload folders
     */

    public void init() {
        try {
            Files.createDirectories(fileLocation);
            Files.createDirectories(photoLocation);
            Files.createDirectories(documentLocation);
            Files.createDirectories(thumbnailLocation);
        }
        catch (IOException e) {
            throw new FileStorageException("Could not initialize storage", e);
        }
    }

    /**
     * Save a file and photo.
     *
     * @param photoData the entity to save
     * @return the persisted photo
     */

    public Photo savePhoto(PhotoData photoData) {

        UploadFileResponse uploadedFile = storeFile(UploadType.PHOTO, photoData.getFile());
        Photo photo = new Photo();
	if(uploadedFile.getFileName() != null)
        photo.setName(uploadedFile.getFileName());
	if(uploadedFile.getFileType() != null)
        photo.setMimeType(uploadedFile.getFileType());
	if(uploadedFile.getSize() != null)
        photo.setFileSize(uploadedFile.getSize());
	if(uploadedFile.getFileDownloadUri() != null)
        photo.setSrc(uploadedFile.getFileDownloadUri());
	if(uploadedFile.getFileThumbUri() != null)
        photo.setThumbnail(uploadedFile.getFileThumbUri());

	if(photoData.getCaption() != null)
        photo.setCaption(photoData.getCaption());
	if(photoData.getOrder() != null)
        photo.setResourceOrder(photoData.getOrder());
	if(photoData.getResourceId() != null)
        photo.setResourceId(photoData.getResourceId());
	if(photoData.getResourceType() != null)
        photo.setResourceType(photoData.getResourceType());
	if(photoData.getThumbnailCaption() != null)
        photo.setThumbnailCaption(photoData.getThumbnailCaption());
        photo.setThumbnailWidth(128);
        photo.setThumbnailHeight(128);
	if(photoData.getTags() != null)
        photo.setTags(photoData.getTags());

        return photoRepository.save(photo);
    }

    /**
     * Save a file and document.
     *
     * @param documentData the document data to save
     * @return the persisted document
     */

    public Document saveDocument(DocumentData documentData) {

        UploadFileResponse uploadedFile = storeFile(UploadType.DOCUMENT, documentData.getFile());
        Document document = new Document();
    if(uploadedFile.getFileName() != null)
        document.setName(uploadedFile.getFileName());
    if(uploadedFile.getFileType() != null)
        document.setMimeType(uploadedFile.getFileType());
    if(uploadedFile.getSize() != null)
        document.setFileSize(uploadedFile.getSize());
    if(uploadedFile.getFileDownloadUri() != null)
        document.setSrc(uploadedFile.getFileDownloadUri());
    if(uploadedFile.getFileThumbUri() != null)
        document.setThumbnail(uploadedFile.getFileThumbUri());

    if(documentData.getCaption() != null)
        document.setCaption(documentData.getCaption());
    if(documentData.getOrder() != null)
        document.setResourceOrder(documentData.getOrder());
    if(documentData.getResourceId() != null)
        document.setResourceId(documentData.getResourceId());
    if(documentData.getResourceType() != null)
        document.setResourceType(documentData.getResourceType());
    if(documentData.getPages() != null)
        document.setPages(documentData.getPages());
	if(documentData.getTags() != null)
        document.setTags(documentData.getTags());

        return documentRepository.save(document);
    }
}
