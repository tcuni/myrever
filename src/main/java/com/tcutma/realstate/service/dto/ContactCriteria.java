package com.tcutma.realstate.service.dto;

import java.io.Serializable;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the Contact entity. This class is used in ContactResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /contacts?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class ContactCriteria implements Serializable {
    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter contactName;

    private StringFilter contactPhone;

    private StringFilter contactAddress;

    private StringFilter contactWebsite;

    private StringFilter contactAvatarUrl;

    private StringFilter contactFacebook;

    private StringFilter contactTwitter;

    private StringFilter contactInstagram;

    private StringFilter contactLinkedin;

    private StringFilter contactGooglePlus;

    private StringFilter contactYoutube;

    private BooleanFilter contactStatus;

    private LongFilter userId;

    public ContactCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getContactName() {
        return contactName;
    }

    public void setContactName(StringFilter contactName) {
        this.contactName = contactName;
    }

    public StringFilter getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(StringFilter contactPhone) {
        this.contactPhone = contactPhone;
    }

    public StringFilter getContactAddress() {
        return contactAddress;
    }

    public void setContactAddress(StringFilter contactAddress) {
        this.contactAddress = contactAddress;
    }

    public StringFilter getContactWebsite() {
        return contactWebsite;
    }

    public void setContactWebsite(StringFilter contactWebsite) {
        this.contactWebsite = contactWebsite;
    }

    public StringFilter getContactAvatarUrl() {
        return contactAvatarUrl;
    }

    public void setContactAvatarUrl(StringFilter contactAvatarUrl) {
        this.contactAvatarUrl = contactAvatarUrl;
    }

    public StringFilter getContactFacebook() {
        return contactFacebook;
    }

    public void setContactFacebook(StringFilter contactFacebook) {
        this.contactFacebook = contactFacebook;
    }

    public StringFilter getContactTwitter() {
        return contactTwitter;
    }

    public void setContactTwitter(StringFilter contactTwitter) {
        this.contactTwitter = contactTwitter;
    }

    public StringFilter getContactInstagram() {
        return contactInstagram;
    }

    public void setContactInstagram(StringFilter contactInstagram) {
        this.contactInstagram = contactInstagram;
    }

    public StringFilter getContactLinkedin() {
        return contactLinkedin;
    }

    public void setContactLinkedin(StringFilter contactLinkedin) {
        this.contactLinkedin = contactLinkedin;
    }

    public StringFilter getContactGooglePlus() {
        return contactGooglePlus;
    }

    public void setContactGooglePlus(StringFilter contactGooglePlus) {
        this.contactGooglePlus = contactGooglePlus;
    }

    public StringFilter getContactYoutube() {
        return contactYoutube;
    }

    public void setContactYoutube(StringFilter contactYoutube) {
        this.contactYoutube = contactYoutube;
    }

    public BooleanFilter getContactStatus() {
        return contactStatus;
    }

    public void setContactStatus(BooleanFilter contactStatus) {
        this.contactStatus = contactStatus;
    }

    public LongFilter getUserId() {
        return userId;
    }

    public void setUserId(LongFilter userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "ContactCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (contactName != null ? "contactName=" + contactName + ", " : "") +
                (contactPhone != null ? "contactPhone=" + contactPhone + ", " : "") +
                (contactAddress != null ? "contactAddress=" + contactAddress + ", " : "") +
                (contactWebsite != null ? "contactWebsite=" + contactWebsite + ", " : "") +
                (contactAvatarUrl != null ? "contactAvatarUrl=" + contactAvatarUrl + ", " : "") +
                (contactFacebook != null ? "contactFacebook=" + contactFacebook + ", " : "") +
                (contactTwitter != null ? "contactTwitter=" + contactTwitter + ", " : "") +
                (contactInstagram != null ? "contactInstagram=" + contactInstagram + ", " : "") +
                (contactLinkedin != null ? "contactLinkedin=" + contactLinkedin + ", " : "") +
                (contactGooglePlus != null ? "contactGooglePlus=" + contactGooglePlus + ", " : "") +
                (contactYoutube != null ? "contactYoutube=" + contactYoutube + ", " : "") +
                (contactStatus != null ? "contactStatus=" + contactStatus + ", " : "") +
                (userId != null ? "userId=" + userId + ", " : "") +
            "}";
    }

}
