package com.tcutma.realstate.service.dto;

import java.io.Serializable;
import com.tcutma.realstate.domain.enumeration.ResourceType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the Document entity. This class is used in DocumentResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /documents?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class DocumentCriteria implements Serializable {
    /**
     * Class for filtering ResourceType
     */
    public static class ResourceTypeFilter extends Filter<ResourceType> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter name;

    private StringFilter src;

    private StringFilter mimeType;

    private LongFilter fileSize;

    private ResourceTypeFilter resourceType;

    private LongFilter resourceId;

    private IntegerFilter resourceOrder;

    private StringFilter thumbnail;

    private IntegerFilter pages;

    private StringFilter caption;

    private StringFilter tags;

    public DocumentCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getSrc() {
        return src;
    }

    public void setSrc(StringFilter src) {
        this.src = src;
    }

    public StringFilter getMimeType() {
        return mimeType;
    }

    public void setMimeType(StringFilter mimeType) {
        this.mimeType = mimeType;
    }

    public LongFilter getFileSize() {
        return fileSize;
    }

    public void setFileSize(LongFilter fileSize) {
        this.fileSize = fileSize;
    }

    public ResourceTypeFilter getResourceType() {
        return resourceType;
    }

    public void setResourceType(ResourceTypeFilter resourceType) {
        this.resourceType = resourceType;
    }

    public LongFilter getResourceId() {
        return resourceId;
    }

    public void setResourceId(LongFilter resourceId) {
        this.resourceId = resourceId;
    }

    public IntegerFilter getResourceOrder() {
        return resourceOrder;
    }

    public void setResourceOrder(IntegerFilter resourceOrder) {
        this.resourceOrder = resourceOrder;
    }

    public StringFilter getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(StringFilter thumbnail) {
        this.thumbnail = thumbnail;
    }

    public IntegerFilter getPages() {
        return pages;
    }

    public void setPages(IntegerFilter pages) {
        this.pages = pages;
    }

    public StringFilter getCaption() {
        return caption;
    }

    public void setCaption(StringFilter caption) {
        this.caption = caption;
    }

    public StringFilter getTags() {
        return tags;
    }

    public void setTags(StringFilter tags) {
        this.tags = tags;
    }

    @Override
    public String toString() {
        return "DocumentCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (src != null ? "src=" + src + ", " : "") +
                (mimeType != null ? "mimeType=" + mimeType + ", " : "") +
                (fileSize != null ? "fileSize=" + fileSize + ", " : "") +
                (resourceType != null ? "resourceType=" + resourceType + ", " : "") +
                (resourceId != null ? "resourceId=" + resourceId + ", " : "") +
                (resourceOrder != null ? "resourceOrder=" + resourceOrder + ", " : "") +
                (thumbnail != null ? "thumbnail=" + thumbnail + ", " : "") +
                (pages != null ? "pages=" + pages + ", " : "") +
                (caption != null ? "caption=" + caption + ", " : "") +
                (tags != null ? "tags=" + tags + ", " : "") +
            "}";
    }

}
