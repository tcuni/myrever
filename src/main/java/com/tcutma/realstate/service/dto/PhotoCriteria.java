package com.tcutma.realstate.service.dto;

import java.io.Serializable;
import com.tcutma.realstate.domain.enumeration.ResourceType;
import io.github.jhipster.service.filter.BooleanFilter;
import io.github.jhipster.service.filter.DoubleFilter;
import io.github.jhipster.service.filter.Filter;
import io.github.jhipster.service.filter.FloatFilter;
import io.github.jhipster.service.filter.IntegerFilter;
import io.github.jhipster.service.filter.LongFilter;
import io.github.jhipster.service.filter.StringFilter;






/**
 * Criteria class for the Photo entity. This class is used in PhotoResource to
 * receive all the possible filtering options from the Http GET request parameters.
 * For example the following could be a valid requests:
 * <code> /photos?id.greaterThan=5&amp;attr1.contains=something&amp;attr2.specified=false</code>
 * As Spring is unable to properly convert the types, unless specific {@link Filter} class are used, we need to use
 * fix type specific filters.
 */
public class PhotoCriteria implements Serializable {
    /**
     * Class for filtering ResourceType
     */
    public static class ResourceTypeFilter extends Filter<ResourceType> {
    }

    private static final long serialVersionUID = 1L;


    private LongFilter id;

    private StringFilter src;

    private StringFilter name;

    private StringFilter thumbnail;

    private IntegerFilter thumbnailWidth;

    private IntegerFilter thumbnailHeight;

    private StringFilter thumbnailCaption;

    private IntegerFilter orientation;

    private StringFilter caption;

    private StringFilter tags;

    private StringFilter alt;

    private LongFilter resourceId;

    private ResourceTypeFilter resourceType;

    private LongFilter fileSize;

    private IntegerFilter resourceOrder;

    private StringFilter mimeType;

    public PhotoCriteria() {
    }

    public LongFilter getId() {
        return id;
    }

    public void setId(LongFilter id) {
        this.id = id;
    }

    public StringFilter getSrc() {
        return src;
    }

    public void setSrc(StringFilter src) {
        this.src = src;
    }

    public StringFilter getName() {
        return name;
    }

    public void setName(StringFilter name) {
        this.name = name;
    }

    public StringFilter getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(StringFilter thumbnail) {
        this.thumbnail = thumbnail;
    }

    public IntegerFilter getThumbnailWidth() {
        return thumbnailWidth;
    }

    public void setThumbnailWidth(IntegerFilter thumbnailWidth) {
        this.thumbnailWidth = thumbnailWidth;
    }

    public IntegerFilter getThumbnailHeight() {
        return thumbnailHeight;
    }

    public void setThumbnailHeight(IntegerFilter thumbnailHeight) {
        this.thumbnailHeight = thumbnailHeight;
    }

    public StringFilter getThumbnailCaption() {
        return thumbnailCaption;
    }

    public void setThumbnailCaption(StringFilter thumbnailCaption) {
        this.thumbnailCaption = thumbnailCaption;
    }

    public IntegerFilter getOrientation() {
        return orientation;
    }

    public void setOrientation(IntegerFilter orientation) {
        this.orientation = orientation;
    }

    public StringFilter getCaption() {
        return caption;
    }

    public void setCaption(StringFilter caption) {
        this.caption = caption;
    }

    public StringFilter getTags() {
        return tags;
    }

    public void setTags(StringFilter tags) {
        this.tags = tags;
    }

    public StringFilter getAlt() {
        return alt;
    }

    public void setAlt(StringFilter alt) {
        this.alt = alt;
    }

    public LongFilter getResourceId() {
        return resourceId;
    }

    public void setResourceId(LongFilter resourceId) {
        this.resourceId = resourceId;
    }

    public ResourceTypeFilter getResourceType() {
        return resourceType;
    }

    public void setResourceType(ResourceTypeFilter resourceType) {
        this.resourceType = resourceType;
    }

    public LongFilter getFileSize() {
        return fileSize;
    }

    public void setFileSize(LongFilter fileSize) {
        this.fileSize = fileSize;
    }

    public IntegerFilter getResourceOrder() {
        return resourceOrder;
    }

    public void setResourceOrder(IntegerFilter resourceOrder) {
        this.resourceOrder = resourceOrder;
    }

    public StringFilter getMimeType() {
        return mimeType;
    }

    public void setMimeType(StringFilter mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public String toString() {
        return "PhotoCriteria{" +
                (id != null ? "id=" + id + ", " : "") +
                (src != null ? "src=" + src + ", " : "") +
                (name != null ? "name=" + name + ", " : "") +
                (thumbnail != null ? "thumbnail=" + thumbnail + ", " : "") +
                (thumbnailWidth != null ? "thumbnailWidth=" + thumbnailWidth + ", " : "") +
                (thumbnailHeight != null ? "thumbnailHeight=" + thumbnailHeight + ", " : "") +
                (thumbnailCaption != null ? "thumbnailCaption=" + thumbnailCaption + ", " : "") +
                (orientation != null ? "orientation=" + orientation + ", " : "") +
                (caption != null ? "caption=" + caption + ", " : "") +
                (tags != null ? "tags=" + tags + ", " : "") +
                (alt != null ? "alt=" + alt + ", " : "") +
                (resourceId != null ? "resourceId=" + resourceId + ", " : "") +
                (resourceType != null ? "resourceType=" + resourceType + ", " : "") +
                (fileSize != null ? "fileSize=" + fileSize + ", " : "") +
                (resourceOrder != null ? "resourceOrder=" + resourceOrder + ", " : "") +
                (mimeType != null ? "mimeType=" + mimeType + ", " : "") +
            "}";
    }

}
