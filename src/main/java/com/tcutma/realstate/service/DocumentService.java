package com.tcutma.realstate.service;

import com.tcutma.realstate.config.ApplicationProperties;
import com.tcutma.realstate.domain.Document;
import com.tcutma.realstate.repository.DocumentRepository;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
/**
 * Service Implementation for managing Document.
 */
@Service
@Transactional
public class DocumentService {

    private final Logger log = LoggerFactory.getLogger(DocumentService.class);

    private final DocumentRepository documentRepository;

    private  final Path documentLocation;

    public DocumentService(ApplicationProperties properties, DocumentRepository documentRepository) {
        this.documentLocation = Paths.get(properties.getDocumentDir());
        this.documentRepository = documentRepository;
    }

    /**
     * Save a document.
     *
     * @param document the entity to save
     * @return the persisted entity
     */
    public Document save(Document document) {
        log.debug("Request to save Document : {}", document);        return documentRepository.save(document);
    }

    /**
     * Get all the documents.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Document> findAll(Pageable pageable) {
        log.debug("Request to get all Documents");
        return documentRepository.findAll(pageable);
    }


    /**
     * Get one document by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Document> findOne(Long id) {
        log.debug("Request to get Document : {}", id);
        return documentRepository.findById(id);
    }

    /**
     * Delete the document by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Document : {}", id);

        Document deleteRecord = documentRepository.getOne(id);

        if(deleteRecord.getFileSize() != null){

            File documentToDel = FileUtils.getFile(documentLocation.resolve(deleteRecord.getName()).toString());

            FileUtils.deleteQuietly(documentToDel);

        }
        documentRepository.deleteById(id);
    }
}
