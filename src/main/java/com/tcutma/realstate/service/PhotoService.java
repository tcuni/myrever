package com.tcutma.realstate.service;

import com.tcutma.realstate.config.ApplicationProperties;
import com.tcutma.realstate.domain.Photo;
import com.tcutma.realstate.repository.PhotoRepository;
import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileSystemUtils;


import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Optional;
/**
 * Service Implementation for managing Photo.
 */
@Service
@Transactional
public class PhotoService {

    private final Logger log = LoggerFactory.getLogger(PhotoService.class);

    private  final Path photoLocation;

    private final Path thumbnailLocation;

    private final PhotoRepository photoRepository;

    public PhotoService(ApplicationProperties properties, PhotoRepository photoRepository) {
        this.photoLocation = Paths.get(properties.getPhotoDir());
        this.thumbnailLocation = Paths.get(properties.getThumbDir());
        this.photoRepository = photoRepository;
    }

    /**
     * Save a photo.
     *
     * @param photo the entity to save
     * @return the persisted entity
     */
    public Photo save(Photo photo) {
        log.debug("Request to save Photo : {}", photo);        return photoRepository.save(photo);
    }

    /**
     * Get all the photos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public Page<Photo> findAll(Pageable pageable) {
        log.debug("Request to get all Photos");
        return photoRepository.findAll(pageable);
    }


    /**
     * Get one photo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Photo> findOne(Long id) {
        log.debug("Request to get Photo : {}", id);
        return photoRepository.findById(id);
    }

    /**
     * Delete the photo by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Photo : {}", id);
        Photo deleteRecord = photoRepository.getOne(id);
        if(deleteRecord.getFileSize() != null){
            File photoToDel = FileUtils.getFile(photoLocation.resolve(deleteRecord.getName()).toString());

            File thumbToDel = FileUtils.getFile(thumbnailLocation.resolve(deleteRecord.getName()).toString());

            FileUtils.deleteQuietly(photoToDel);

            FileUtils.deleteQuietly(thumbToDel);
        }
        photoRepository.deleteById(id);

    }
}
