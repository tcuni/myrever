package com.tcutma.realstate.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.tcutma.realstate.domain.Contact;
import com.tcutma.realstate.domain.*; // for static metamodels
import com.tcutma.realstate.repository.ContactRepository;
import com.tcutma.realstate.service.dto.ContactCriteria;


/**
 * Service for executing complex queries for Contact entities in the database.
 * The main input is a {@link ContactCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Contact} or a {@link Page} of {@link Contact} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class ContactQueryService extends QueryService<Contact> {

    private final Logger log = LoggerFactory.getLogger(ContactQueryService.class);

    private final ContactRepository contactRepository;

    public ContactQueryService(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    /**
     * Return a {@link List} of {@link Contact} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Contact> findByCriteria(ContactCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Contact> specification = createSpecification(criteria);
        return contactRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Contact} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Contact> findByCriteria(ContactCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Contact> specification = createSpecification(criteria);
        return contactRepository.findAll(specification, page);
    }

    /**
     * Function to convert ContactCriteria to a {@link Specification}
     */
    private Specification<Contact> createSpecification(ContactCriteria criteria) {
        Specification<Contact> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Contact_.id));
            }
            if (criteria.getContactName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactName(), Contact_.contactName));
            }
            if (criteria.getContactPhone() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactPhone(), Contact_.contactPhone));
            }
            if (criteria.getContactAddress() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactAddress(), Contact_.contactAddress));
            }
            if (criteria.getContactWebsite() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactWebsite(), Contact_.contactWebsite));
            }
            if (criteria.getContactAvatarUrl() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactAvatarUrl(), Contact_.contactAvatarUrl));
            }
            if (criteria.getContactFacebook() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactFacebook(), Contact_.contactFacebook));
            }
            if (criteria.getContactTwitter() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactTwitter(), Contact_.contactTwitter));
            }
            if (criteria.getContactInstagram() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactInstagram(), Contact_.contactInstagram));
            }
            if (criteria.getContactLinkedin() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactLinkedin(), Contact_.contactLinkedin));
            }
            if (criteria.getContactGooglePlus() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactGooglePlus(), Contact_.contactGooglePlus));
            }
            if (criteria.getContactYoutube() != null) {
                specification = specification.and(buildStringSpecification(criteria.getContactYoutube(), Contact_.contactYoutube));
            }
            if (criteria.getContactStatus() != null) {
                specification = specification.and(buildSpecification(criteria.getContactStatus(), Contact_.contactStatus));
            }
            if (criteria.getUserId() != null) {
                specification = specification.and(buildReferringEntitySpecification(criteria.getUserId(), Contact_.user, User_.id));
            }
        }
        return specification;
    }

}
