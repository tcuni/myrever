package com.tcutma.realstate.service;

import com.tcutma.realstate.config.Constants;
import com.tcutma.realstate.domain.Tag;
import com.tcutma.realstate.repository.TagRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing Tag.
 */
@Service
@Transactional
public class TagService {

    private final Logger log = LoggerFactory.getLogger(TagService.class);

    private final TagRepository tagRepository;

    public TagService(TagRepository tagRepository) {
        this.tagRepository = tagRepository;
    }

    /**
     * Save a tag.
     *
     * @param tag the entity to save
     * @return the persisted entity
     */
    public Tag save(Tag tag) {
        log.debug("Request to save Tag : {}", tag);        return tagRepository.save(tag);
    }

    /**
     * Get all the tags.
     *
     * @return the list of entities
     */
    @Transactional(readOnly = true)
    public List<Tag> findAll() {
        log.debug("Request to get all Tags");
        return tagRepository.findAll();
    }


    /**
     * Get one tag by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Transactional(readOnly = true)
    public Optional<Tag> findOne(Long id) {
        log.debug("Request to get Tag : {}", id);
        return tagRepository.findById(id);
    }

    /**
     * Count tags by name.
     *
     * @param tagName the id of the entity
     * @return number of tags have same name
     */
    @Transactional(readOnly = true)
    public int countTagsByName(String tagName) {

        return tagRepository.countTagsByTagNameEquals(tagName);
    }



    /**
     * Travers over set of tags and add new tag if not exist.
     *
     * @param tags a set of tags
     * @return number of tags have same name
     */
    @Transactional(readOnly = true)
    public Set<Tag> processTags(Set<Tag> tags) {

        return tags.stream()
            .map(tag -> {

                // if tag id exist then don't modify it
                if(tag.getId()!=null) return tag;

                // Get tagname
                String tagName =tag.getTagName() ;

                // Truncate tagName
                tag.setTagName(tagName.substring(0,Math.min(tagName.length(),Constants.NAME_LENGTH)));

                // Find tag by tagName
                Optional<Tag> opTag = this.findOneByTagName(tag.getTagName());

                log.info("after get tag: {}",opTag);

                // Create new tag if not exist
                return opTag.orElseGet(()->this.save(tag));
            })
            //.filter(tag-> Objects.nonNull(tag.getId()))
            .collect(Collectors.toSet());
    }

    /**
     * Find one tag by tag name
     *
     * @param tagName the id of the entity
     * @return a tag
     */

    private Optional<Tag> findOneByTagName(String tagName) {
        return tagRepository.findTagByTagName(tagName);
    }


    /**
     * Delete the tag by id.
     *
     * @param id the id of the entity
     */
    public void delete(Long id) {
        log.debug("Request to delete Tag : {}", id);
        tagRepository.deleteById(id);
    }
}
