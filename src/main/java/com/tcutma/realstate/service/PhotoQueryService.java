package com.tcutma.realstate.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import io.github.jhipster.service.QueryService;

import com.tcutma.realstate.domain.Photo;
import com.tcutma.realstate.domain.*; // for static metamodels
import com.tcutma.realstate.repository.PhotoRepository;
import com.tcutma.realstate.service.dto.PhotoCriteria;


/**
 * Service for executing complex queries for Photo entities in the database.
 * The main input is a {@link PhotoCriteria} which gets converted to {@link Specification},
 * in a way that all the filters must apply.
 * It returns a {@link List} of {@link Photo} or a {@link Page} of {@link Photo} which fulfills the criteria.
 */
@Service
@Transactional(readOnly = true)
public class PhotoQueryService extends QueryService<Photo> {

    private final Logger log = LoggerFactory.getLogger(PhotoQueryService.class);

    private final PhotoRepository photoRepository;

    public PhotoQueryService(PhotoRepository photoRepository) {
        this.photoRepository = photoRepository;
    }

    /**
     * Return a {@link List} of {@link Photo} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public List<Photo> findByCriteria(PhotoCriteria criteria) {
        log.debug("find by criteria : {}", criteria);
        final Specification<Photo> specification = createSpecification(criteria);
        return photoRepository.findAll(specification);
    }

    /**
     * Return a {@link Page} of {@link Photo} which matches the criteria from the database
     * @param criteria The object which holds all the filters, which the entities should match.
     * @param page The page, which should be returned.
     * @return the matching entities.
     */
    @Transactional(readOnly = true)
    public Page<Photo> findByCriteria(PhotoCriteria criteria, Pageable page) {
        log.debug("find by criteria : {}, page: {}", criteria, page);
        final Specification<Photo> specification = createSpecification(criteria);
        return photoRepository.findAll(specification, page);
    }

    /**
     * Function to convert PhotoCriteria to a {@link Specification}
     */
    private Specification<Photo> createSpecification(PhotoCriteria criteria) {
        Specification<Photo> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getId() != null) {
                specification = specification.and(buildSpecification(criteria.getId(), Photo_.id));
            }
            if (criteria.getSrc() != null) {
                specification = specification.and(buildStringSpecification(criteria.getSrc(), Photo_.src));
            }
            if (criteria.getName() != null) {
                specification = specification.and(buildStringSpecification(criteria.getName(), Photo_.name));
            }
            if (criteria.getThumbnail() != null) {
                specification = specification.and(buildStringSpecification(criteria.getThumbnail(), Photo_.thumbnail));
            }
            if (criteria.getThumbnailWidth() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThumbnailWidth(), Photo_.thumbnailWidth));
            }
            if (criteria.getThumbnailHeight() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getThumbnailHeight(), Photo_.thumbnailHeight));
            }
            if (criteria.getThumbnailCaption() != null) {
                specification = specification.and(buildStringSpecification(criteria.getThumbnailCaption(), Photo_.thumbnailCaption));
            }
            if (criteria.getOrientation() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getOrientation(), Photo_.orientation));
            }
            if (criteria.getCaption() != null) {
                specification = specification.and(buildStringSpecification(criteria.getCaption(), Photo_.caption));
            }
            if (criteria.getTags() != null) {
                specification = specification.and(buildStringSpecification(criteria.getTags(), Photo_.tags));
            }
            if (criteria.getAlt() != null) {
                specification = specification.and(buildStringSpecification(criteria.getAlt(), Photo_.alt));
            }
            if (criteria.getResourceId() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getResourceId(), Photo_.resourceId));
            }
            if (criteria.getResourceType() != null) {
                specification = specification.and(buildSpecification(criteria.getResourceType(), Photo_.resourceType));
            }
            if (criteria.getFileSize() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getFileSize(), Photo_.fileSize));
            }
            if (criteria.getResourceOrder() != null) {
                specification = specification.and(buildRangeSpecification(criteria.getResourceOrder(), Photo_.resourceOrder));
            }
            if (criteria.getMimeType() != null) {
                specification = specification.and(buildStringSpecification(criteria.getMimeType(), Photo_.mimeType));
            }
        }
        return specification;
    }

}
