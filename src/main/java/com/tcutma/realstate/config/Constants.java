package com.tcutma.realstate.config;

/**
 * Application constants.
 */
public final class Constants {

    // Regex for acceptable logins
    public static final String LOGIN_REGEX = "^[_.@A-Za-z0-9-]*$";

    // Regex for acceptable Email
    public static final String EMAIL_PATTERN =
        "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    // Regex for acceptable Phone Number
    public static final String PHONENUMBER_PATTERN = "^(\\+\\d{1,4}[- ]?)?\\d{9,11}$";
        //"^\\+?\\d{1,3}?[- .]?\\(?(?:\\d{2,3})\\)?[- .]?\\d\\d\\d[- .]?\\d\\d\\d\\d$";

    // Regex for acceptable image file
    public static final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp|jpeg|cgm|tiff))$)";

    // Regex for acceptable image file
    public static final String DOCUMENT_PATTERN = "([^\\s]+(\\.(?i)(pdf|doc|docx|rtf|odt|xls|xlsx|djvu|ppt|pptx))$)";

    // Regex for Ip address
    public static final String IPADDRESS_PATTERN =
        "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
            "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";

    // Regex for Date "dd/mm/yyyy"
    public static final String DATE_PATTERN =
        "(0?[1-9]|[12][0-9]|3[01])/(0?[1-9]|1[012])/((19|20)\\d\\d)";

    // Regex for Html tag
    public static final String HTML_TAG_PATTERN = "<(\"[^\"]*\"|'[^']*'|[^'\">])*>";

    // Regex for Html link pattern
    public static final String HTML_A_TAG_PATTERN = "(?i)<a([^>]+)>(.+?)</a>";
    public static final String HTML_A_HREF_TAG_PATTERN =
        "\\s*(?i)href\\s*=\\s*(\"([^\"]*\")|'[^']*'|([^'\">\\s]+))";

    // Regex for Time 24h
    public static final String TIME24HOURS_PATTERN =
        "([01]?[0-9]|2[0-3]):[0-5][0-9]";

    // Regex for Time 12h
    public static final String TIME12HOURS_PATTERN =
        "(1[012]|[1-9]):[0-5][0-9](\\s)?(?i)(am|pm)";

    // Regex for password: at least 6, maximum 20, contains digit,upper, lower case, special characters.
    public static final String PASSWORD_PATTERN =
        "((?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%]).{6,20})";

    public static final String SYSTEM_ACCOUNT = "system";
    public static final String ANONYMOUS_USER = "anonymoususer";
    public static final String DEFAULT_LANGUAGE = "en";



    public static final int NAME_LENGTH = 128;

    private Constants() {
    }
}
