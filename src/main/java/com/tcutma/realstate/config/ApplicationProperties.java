package com.tcutma.realstate.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Properties specific to River App.
 * <p>
 * Properties are configured in the application.yml file.
 * See {@link io.github.jhipster.config.JHipsterProperties} for a good example.
 */
@ConfigurationProperties(prefix = "application", ignoreUnknownFields = false)
public class ApplicationProperties {
    private String uploadDir;

    private String photoDir;

    private String documentDir;
    private String thumbDir;

    public ApplicationProperties() {
    }

    public ApplicationProperties(String uploadDir) {
        this.uploadDir = uploadDir;
    }

    public String getThumbDir() {
        return thumbDir;
    }

    public void setThumbDir(String thumbnailDir) {
        this.thumbDir = thumbnailDir;
    }

    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }

    public String getPhotoDir() {
        return photoDir;
    }

    public void setPhotoDir(String photoDir) {
        this.photoDir = photoDir;
    }

    public String getDocumentDir() {
        return documentDir;
    }

    public void setDocumentDir(String documentDir) {
        this.documentDir = documentDir;
    }
}
