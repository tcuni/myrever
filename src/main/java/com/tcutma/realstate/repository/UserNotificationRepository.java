package com.tcutma.realstate.repository;

import com.tcutma.realstate.domain.UserNotification;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the UserNotification entity.
 */
@SuppressWarnings("unused")
@Repository
public interface UserNotificationRepository extends JpaRepository<UserNotification, Long> {

    @Query("select user_notification from UserNotification user_notification where user_notification.receiver.login = ?#{principal.username}")
    List<UserNotification> findByReceiverIsCurrentUser();

}
