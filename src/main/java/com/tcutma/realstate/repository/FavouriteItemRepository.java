package com.tcutma.realstate.repository;

import com.tcutma.realstate.domain.FavouriteItem;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the FavouriteItem entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FavouriteItemRepository extends JpaRepository<FavouriteItem, Long> {

    @Query("select favourite_item from FavouriteItem favourite_item where favourite_item.user.login = ?#{principal.username}")
    List<FavouriteItem> findByUserIsCurrentUser();

}
