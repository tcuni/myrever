package com.tcutma.realstate.repository;

import com.tcutma.realstate.domain.Tag;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

import java.util.Optional;


/**
 * Spring Data  repository for the Tag entity.
 */
@SuppressWarnings("unused")
@Repository
public interface TagRepository extends JpaRepository<Tag, Long> {

    int countTagsByTagNameEquals(String tagName);

    Optional<Tag> findTagByTagName(String tagName);

}
