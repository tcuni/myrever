import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import FavouriteItem from './favourite-item';
import FavouriteItemDetail from './favourite-item-detail';
import FavouriteItemUpdate from './favourite-item-update';
import FavouriteItemDeleteDialog from './favourite-item-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={FavouriteItemUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={FavouriteItemUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={FavouriteItemDetail} />
      <ErrorBoundaryRoute path={match.url} component={FavouriteItem} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={FavouriteItemDeleteDialog} />
  </>
);

export default Routes;
