import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './favourite-item.reducer';
import { IFavouriteItem } from 'app/shared/model/favourite-item.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IFavouriteItemProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class FavouriteItem extends React.Component<IFavouriteItemProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { favouriteItemList, match } = this.props;
    return (
      <div>
        <h2 id="favourite-item-heading">
          <Translate contentKey="riverApp.favouriteItem.home.title">Favourite Items</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="riverApp.favouriteItem.home.createLabel">Create new Favourite Item</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.favouriteItem.favourited">Favourited</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.favouriteItem.resourceId">Resource Id</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.favouriteItem.resourceType">Resource Type</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.favouriteItem.user">User</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {favouriteItemList.map((favouriteItem, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${favouriteItem.id}`} color="link" size="sm">
                      {favouriteItem.id}
                    </Button>
                  </td>
                  <td>{favouriteItem.favourited ? 'true' : 'false'}</td>
                  <td>{favouriteItem.resourceId}</td>
                  <td>{favouriteItem.resourceType}</td>
                  <td>{favouriteItem.user ? favouriteItem.user.login : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${favouriteItem.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${favouriteItem.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${favouriteItem.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ favouriteItem }: IRootState) => ({
  favouriteItemList: favouriteItem.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FavouriteItem);
