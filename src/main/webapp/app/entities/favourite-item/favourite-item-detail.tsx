import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './favourite-item.reducer';
import { IFavouriteItem } from 'app/shared/model/favourite-item.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IFavouriteItemDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export class FavouriteItemDetail extends React.Component<IFavouriteItemDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { favouriteItemEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="riverApp.favouriteItem.detail.title">FavouriteItem</Translate> [<b>{favouriteItemEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="favourited">
                <Translate contentKey="riverApp.favouriteItem.favourited">Favourited</Translate>
              </span>
            </dt>
            <dd>{favouriteItemEntity.favourited ? 'true' : 'false'}</dd>
            <dt>
              <span id="resourceId">
                <Translate contentKey="riverApp.favouriteItem.resourceId">Resource Id</Translate>
              </span>
            </dt>
            <dd>{favouriteItemEntity.resourceId}</dd>
            <dt>
              <span id="resourceType">
                <Translate contentKey="riverApp.favouriteItem.resourceType">Resource Type</Translate>
              </span>
            </dt>
            <dd>{favouriteItemEntity.resourceType}</dd>
            <dt>
              <Translate contentKey="riverApp.favouriteItem.user">User</Translate>
            </dt>
            <dd>{favouriteItemEntity.user ? favouriteItemEntity.user.login : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/favourite-item" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/favourite-item/${favouriteItemEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ favouriteItem }: IRootState) => ({
  favouriteItemEntity: favouriteItem.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FavouriteItemDetail);
