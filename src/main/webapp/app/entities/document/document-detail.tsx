import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './document.reducer';
import { IDocument } from 'app/shared/model/document.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IDocumentDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export class DocumentDetail extends React.Component<IDocumentDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { documentEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="riverApp.document.detail.title">Document</Translate> [<b>{documentEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="name">
                <Translate contentKey="riverApp.document.name">Name</Translate>
              </span>
            </dt>
            <dd>{documentEntity.name}</dd>
            <dt>
              <span id="src">
                <Translate contentKey="riverApp.document.src">Src</Translate>
              </span>
            </dt>
            <dd>{documentEntity.src}</dd>
            <dt>
              <span id="mimeType">
                <Translate contentKey="riverApp.document.mimeType">Mime Type</Translate>
              </span>
            </dt>
            <dd>{documentEntity.mimeType}</dd>
            <dt>
              <span id="fileSize">
                <Translate contentKey="riverApp.document.fileSize">File Size</Translate>
              </span>
            </dt>
            <dd>{documentEntity.fileSize}</dd>
            <dt>
              <span id="resourceType">
                <Translate contentKey="riverApp.document.resourceType">Resource Type</Translate>
              </span>
            </dt>
            <dd>{documentEntity.resourceType}</dd>
            <dt>
              <span id="resourceId">
                <Translate contentKey="riverApp.document.resourceId">Resource Id</Translate>
              </span>
            </dt>
            <dd>{documentEntity.resourceId}</dd>
            <dt>
              <span id="resourceOrder">
                <Translate contentKey="riverApp.document.resourceOrder">Resource Order</Translate>
              </span>
            </dt>
            <dd>{documentEntity.resourceOrder}</dd>
            <dt>
              <span id="thumbnail">
                <Translate contentKey="riverApp.document.thumbnail">Thumbnail</Translate>
              </span>
            </dt>
            <dd>{documentEntity.thumbnail}</dd>
            <dt>
              <span id="pages">
                <Translate contentKey="riverApp.document.pages">Pages</Translate>
              </span>
            </dt>
            <dd>{documentEntity.pages}</dd>
            <dt>
              <span id="caption">
                <Translate contentKey="riverApp.document.caption">Caption</Translate>
              </span>
            </dt>
            <dd>{documentEntity.caption}</dd>
            <dt>
              <span id="tags">
                <Translate contentKey="riverApp.document.tags">Tags</Translate>
              </span>
            </dt>
            <dd>{documentEntity.tags}</dd>
          </dl>
          <Button tag={Link} to="/entity/document" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/document/${documentEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ document }: IRootState) => ({
  documentEntity: document.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DocumentDetail);
