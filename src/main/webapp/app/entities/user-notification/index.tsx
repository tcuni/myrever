import React from 'react';
import { Switch } from 'react-router-dom';

import ErrorBoundaryRoute from 'app/shared/error/error-boundary-route';

import UserNotification from './user-notification';
import UserNotificationDetail from './user-notification-detail';
import UserNotificationUpdate from './user-notification-update';
import UserNotificationDeleteDialog from './user-notification-delete-dialog';

const Routes = ({ match }) => (
  <>
    <Switch>
      <ErrorBoundaryRoute exact path={`${match.url}/new`} component={UserNotificationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id/edit`} component={UserNotificationUpdate} />
      <ErrorBoundaryRoute exact path={`${match.url}/:id`} component={UserNotificationDetail} />
      <ErrorBoundaryRoute path={match.url} component={UserNotification} />
    </Switch>
    <ErrorBoundaryRoute path={`${match.url}/:id/delete`} component={UserNotificationDeleteDialog} />
  </>
);

export default Routes;
