import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './user-notification.reducer';
import { IUserNotification } from 'app/shared/model/user-notification.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IUserNotificationDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export class UserNotificationDetail extends React.Component<IUserNotificationDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { userNotificationEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="riverApp.userNotification.detail.title">UserNotification</Translate> [<b>{userNotificationEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="isSeen">
                <Translate contentKey="riverApp.userNotification.isSeen">Is Seen</Translate>
              </span>
            </dt>
            <dd>{userNotificationEntity.isSeen ? 'true' : 'false'}</dd>
            <dt>
              <span id="seenDate">
                <Translate contentKey="riverApp.userNotification.seenDate">Seen Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={userNotificationEntity.seenDate} type="date" format={APP_DATE_FORMAT} />
            </dd>
            <dt>
              <Translate contentKey="riverApp.userNotification.notification">Notification</Translate>
            </dt>
            <dd>{userNotificationEntity.notification ? userNotificationEntity.notification.notificationTitle : ''}</dd>
            <dt>
              <Translate contentKey="riverApp.userNotification.receiver">Receiver</Translate>
            </dt>
            <dd>{userNotificationEntity.receiver ? userNotificationEntity.receiver.login : ''}</dd>
          </dl>
          <Button tag={Link} to="/entity/user-notification" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/user-notification/${userNotificationEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ userNotification }: IRootState) => ({
  userNotificationEntity: userNotification.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserNotificationDetail);
