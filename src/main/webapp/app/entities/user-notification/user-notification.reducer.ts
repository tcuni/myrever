import axios from 'axios';
import { ICrudGetAction, ICrudGetAllAction, ICrudPutAction, ICrudDeleteAction } from 'react-jhipster';

import { cleanEntity } from 'app/shared/util/entity-utils';
import { REQUEST, SUCCESS, FAILURE } from 'app/shared/reducers/action-type.util';
import { SERVER_API_URL } from 'app/config/constants';

import { IUserNotification, defaultValue } from 'app/shared/model/user-notification.model';

export const ACTION_TYPES = {
  FETCH_USERNOTIFICATION_LIST: 'userNotification/FETCH_USERNOTIFICATION_LIST',
  FETCH_USERNOTIFICATION: 'userNotification/FETCH_USERNOTIFICATION',
  CREATE_USERNOTIFICATION: 'userNotification/CREATE_USERNOTIFICATION',
  UPDATE_USERNOTIFICATION: 'userNotification/UPDATE_USERNOTIFICATION',
  DELETE_USERNOTIFICATION: 'userNotification/DELETE_USERNOTIFICATION',
  RESET: 'userNotification/RESET'
};

const initialState = {
  loading: false,
  errorMessage: null,
  entities: [] as ReadonlyArray<IUserNotification>,
  entity: defaultValue,
  updating: false,
  updateSuccess: false
};

export type UserNotificationState = Readonly<typeof initialState>;

// Reducer

export default (state: UserNotificationState = initialState, action): UserNotificationState => {
  switch (action.type) {
    case REQUEST(ACTION_TYPES.FETCH_USERNOTIFICATION_LIST):
    case REQUEST(ACTION_TYPES.FETCH_USERNOTIFICATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        loading: true
      };
    case REQUEST(ACTION_TYPES.CREATE_USERNOTIFICATION):
    case REQUEST(ACTION_TYPES.UPDATE_USERNOTIFICATION):
    case REQUEST(ACTION_TYPES.DELETE_USERNOTIFICATION):
      return {
        ...state,
        errorMessage: null,
        updateSuccess: false,
        updating: true
      };
    case FAILURE(ACTION_TYPES.FETCH_USERNOTIFICATION_LIST):
    case FAILURE(ACTION_TYPES.FETCH_USERNOTIFICATION):
    case FAILURE(ACTION_TYPES.CREATE_USERNOTIFICATION):
    case FAILURE(ACTION_TYPES.UPDATE_USERNOTIFICATION):
    case FAILURE(ACTION_TYPES.DELETE_USERNOTIFICATION):
      return {
        ...state,
        loading: false,
        updating: false,
        updateSuccess: false,
        errorMessage: action.payload
      };
    case SUCCESS(ACTION_TYPES.FETCH_USERNOTIFICATION_LIST):
      return {
        ...state,
        loading: false,
        entities: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.FETCH_USERNOTIFICATION):
      return {
        ...state,
        loading: false,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.CREATE_USERNOTIFICATION):
    case SUCCESS(ACTION_TYPES.UPDATE_USERNOTIFICATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: action.payload.data
      };
    case SUCCESS(ACTION_TYPES.DELETE_USERNOTIFICATION):
      return {
        ...state,
        updating: false,
        updateSuccess: true,
        entity: {}
      };
    case ACTION_TYPES.RESET:
      return {
        ...initialState
      };
    default:
      return state;
  }
};

const apiUrl = SERVER_API_URL + '/api/v1/user-notifications';

// Actions

export const getEntities: ICrudGetAllAction<IUserNotification> = (page, size, sort) => ({
  type: ACTION_TYPES.FETCH_USERNOTIFICATION_LIST,
  payload: axios.get<IUserNotification>(`${apiUrl}?cacheBuster=${new Date().getTime()}`)
});

export const getEntity: ICrudGetAction<IUserNotification> = id => {
  const requestUrl = `${apiUrl}/${id}`;
  return {
    type: ACTION_TYPES.FETCH_USERNOTIFICATION,
    payload: axios.get<IUserNotification>(requestUrl)
  };
};

export const createEntity: ICrudPutAction<IUserNotification> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.CREATE_USERNOTIFICATION,
    payload: axios.post(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const updateEntity: ICrudPutAction<IUserNotification> = entity => async dispatch => {
  const result = await dispatch({
    type: ACTION_TYPES.UPDATE_USERNOTIFICATION,
    payload: axios.put(apiUrl, cleanEntity(entity))
  });
  dispatch(getEntities());
  return result;
};

export const deleteEntity: ICrudDeleteAction<IUserNotification> = id => async dispatch => {
  const requestUrl = `${apiUrl}/${id}`;
  const result = await dispatch({
    type: ACTION_TYPES.DELETE_USERNOTIFICATION,
    payload: axios.delete(requestUrl)
  });
  dispatch(getEntities());
  return result;
};

export const reset = () => ({
  type: ACTION_TYPES.RESET
});
