import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { INotification } from 'app/shared/model/notification.model';
import { getEntities as getNotifications } from 'app/entities/notification/notification.reducer';
import { IUser } from 'app/shared/model/user.model';
import { getUsers } from 'app/modules/administration/user-management/user-management.reducer';
import { getEntity, updateEntity, createEntity, reset } from './user-notification.reducer';
import { IUserNotification } from 'app/shared/model/user-notification.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IUserNotificationUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export interface IUserNotificationUpdateState {
  isNew: boolean;
  notificationId: number;
  receiverId: number;
}

export class UserNotificationUpdate extends React.Component<IUserNotificationUpdateProps, IUserNotificationUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      notificationId: 0,
      receiverId: 0,
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }

    this.props.getNotifications();
    this.props.getUsers();
  }

  saveEntity = (event, errors, values) => {
    values.seenDate = new Date(values.seenDate);

    if (errors.length === 0) {
      const { userNotificationEntity } = this.props;
      const entity = {
        ...userNotificationEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/user-notification');
  };

  notificationUpdate = element => {
    const notificationTitle = element.target.value.toString();
    if (notificationTitle === '') {
      this.setState({
        notificationId: -1
      });
    } else {
      for (const i in this.props.notifications) {
        if (notificationTitle === this.props.notifications[i].notificationTitle.toString()) {
          this.setState({
            notificationId: this.props.notifications[i].id
          });
        }
      }
    }
  };

  receiverUpdate = element => {
    const login = element.target.value.toString();
    if (login === '') {
      this.setState({
        receiverId: -1
      });
    } else {
      for (const i in this.props.users) {
        if (login === this.props.users[i].login.toString()) {
          this.setState({
            receiverId: this.props.users[i].id
          });
        }
      }
    }
  };

  render() {
    const isInvalid = false;
    const { userNotificationEntity, notifications, users, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="riverApp.userNotification.home.createOrEditLabel">
              <Translate contentKey="riverApp.userNotification.home.createOrEditLabel">Create or edit a UserNotification</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : userNotificationEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="user-notification-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="isSeenLabel" check>
                    <AvInput id="user-notification-isSeen" type="checkbox" className="form-control" name="isSeen" />
                    <Translate contentKey="riverApp.userNotification.isSeen">Is Seen</Translate>
                  </Label>
                </AvGroup>
                <AvGroup>
                  <Label id="seenDateLabel" for="seenDate">
                    <Translate contentKey="riverApp.userNotification.seenDate">Seen Date</Translate>
                  </Label>
                  <AvInput
                    id="user-notification-seenDate"
                    type="datetime-local"
                    className="form-control"
                    name="seenDate"
                    value={isNew ? null : convertDateTimeFromServer(this.props.userNotificationEntity.seenDate)}
                  />
                </AvGroup>
                <AvGroup>
                  <Label for="notification.notificationTitle">
                    <Translate contentKey="riverApp.userNotification.notification">Notification</Translate>
                  </Label>
                  <AvInput
                    id="user-notification-notification"
                    type="select"
                    className="form-control"
                    name="notification.notificationTitle"
                    onChange={this.notificationUpdate}
                  >
                    <option value="" key="0" />
                    {notifications
                      ? notifications.map(otherEntity => (
                          <option value={otherEntity.notificationTitle} key={otherEntity.id}>
                            {otherEntity.notificationTitle}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label for="receiver.login">
                    <Translate contentKey="riverApp.userNotification.receiver">Receiver</Translate>
                  </Label>
                  <AvInput
                    id="user-notification-receiver"
                    type="select"
                    className="form-control"
                    name="receiver.login"
                    onChange={this.receiverUpdate}
                  >
                    <option value="" key="0" />
                    {users
                      ? users.map(otherEntity => (
                          <option value={otherEntity.login} key={otherEntity.id}>
                            {otherEntity.login}
                          </option>
                        ))
                      : null}
                  </AvInput>
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/user-notification" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={isInvalid || updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  notifications: storeState.notification.entities,
  users: storeState.userManagement.users,
  userNotificationEntity: storeState.userNotification.entity,
  loading: storeState.userNotification.loading,
  updating: storeState.userNotification.updating
});

const mapDispatchToProps = {
  getNotifications,
  getUsers,
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserNotificationUpdate);
