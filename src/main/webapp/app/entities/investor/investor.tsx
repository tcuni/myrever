import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './investor.reducer';
import { IInvestor } from 'app/shared/model/investor.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IInvestorProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Investor extends React.Component<IInvestorProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { investorList, match } = this.props;
    return (
      <div>
        <h2 id="investor-heading">
          <Translate contentKey="riverApp.investor.home.title">Investors</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="riverApp.investor.home.createLabel">Create new Investor</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.investor.investorName">Investor Name</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.investor.investorTitle">Investor Title</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.investor.investorDate">Investor Date</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.investor.investorDescription">Investor Description</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.investor.investorAddress">Investor Address</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.investor.investorWebsite">Investor Website</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.investor.investorPhone">Investor Phone</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.investor.investorAvatarUrl">Investor Avatar Url</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {investorList.map((investor, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${investor.id}`} color="link" size="sm">
                      {investor.id}
                    </Button>
                  </td>
                  <td>{investor.investorName}</td>
                  <td>{investor.investorTitle}</td>
                  <td>
                    <TextFormat type="date" value={investor.investorDate} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{investor.investorDescription}</td>
                  <td>{investor.investorAddress}</td>
                  <td>{investor.investorWebsite}</td>
                  <td>{investor.investorPhone}</td>
                  <td>{investor.investorAvatarUrl}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${investor.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${investor.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${investor.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ investor }: IRootState) => ({
  investorList: investor.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Investor);
