import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './employee.reducer';
import { IEmployee } from 'app/shared/model/employee.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IEmployeeProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Employee extends React.Component<IEmployeeProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { employeeList, match } = this.props;
    return (
      <div>
        <h2 id="employee-heading">
          <Translate contentKey="riverApp.employee.home.title">Employees</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="riverApp.employee.home.createLabel">Create new Employee</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.employee.employeeFirstName">Employee First Name</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.employee.employeeLastName">Employee Last Name</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.employee.employeeDob">Employee Dob</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.employee.employeeSex">Employee Sex</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.employee.employeeIdentityCard">Employee Identity Card</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.employee.employeePhone">Employee Phone</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.employee.employeeEmail">Employee Email</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.employee.account">Account</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.employee.department">Department</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.employee.jobtitle">Jobtitle</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {employeeList.map((employee, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${employee.id}`} color="link" size="sm">
                      {employee.id}
                    </Button>
                  </td>
                  <td>{employee.employeeFirstName}</td>
                  <td>{employee.employeeLastName}</td>
                  <td>
                    <TextFormat type="date" value={employee.employeeDob} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{employee.employeeSex}</td>
                  <td>{employee.employeeIdentityCard}</td>
                  <td>{employee.employeePhone}</td>
                  <td>{employee.employeeEmail}</td>
                  <td>{employee.account ? employee.account.login : ''}</td>
                  <td>
                    {employee.department ? (
                      <Link to={`department/${employee.department.id}`}>{employee.department.departmentName}</Link>
                    ) : (
                      ''
                    )}
                  </td>
                  <td>{employee.jobtitle ? <Link to={`jobTitle/${employee.jobtitle.id}`}>{employee.jobtitle.titleName}</Link> : ''}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${employee.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${employee.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${employee.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ employee }: IRootState) => ({
  employeeList: employee.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Employee);
