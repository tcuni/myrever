import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './photo.reducer';
import { IPhoto } from 'app/shared/model/photo.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IPhotoUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export interface IPhotoUpdateState {
  isNew: boolean;
}

export class PhotoUpdate extends React.Component<IPhotoUpdateProps, IPhotoUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { photoEntity } = this.props;
      const entity = {
        ...photoEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/photo');
  };

  render() {
    const isInvalid = false;
    const { photoEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="riverApp.photo.home.createOrEditLabel">
              <Translate contentKey="riverApp.photo.home.createOrEditLabel">Create or edit a Photo</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : photoEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="photo-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="srcLabel" for="src">
                    <Translate contentKey="riverApp.photo.src">Src</Translate>
                  </Label>
                  <AvField id="photo-src" type="text" name="src" />
                </AvGroup>
                <AvGroup>
                  <Label id="nameLabel" for="name">
                    <Translate contentKey="riverApp.photo.name">Name</Translate>
                  </Label>
                  <AvField id="photo-name" type="text" name="name" />
                </AvGroup>
                <AvGroup>
                  <Label id="thumbnailLabel" for="thumbnail">
                    <Translate contentKey="riverApp.photo.thumbnail">Thumbnail</Translate>
                  </Label>
                  <AvField id="photo-thumbnail" type="text" name="thumbnail" />
                </AvGroup>
                <AvGroup>
                  <Label id="thumbnailWidthLabel" for="thumbnailWidth">
                    <Translate contentKey="riverApp.photo.thumbnailWidth">Thumbnail Width</Translate>
                  </Label>
                  <AvField id="photo-thumbnailWidth" type="number" className="form-control" name="thumbnailWidth" />
                </AvGroup>
                <AvGroup>
                  <Label id="thumbnailHeightLabel" for="thumbnailHeight">
                    <Translate contentKey="riverApp.photo.thumbnailHeight">Thumbnail Height</Translate>
                  </Label>
                  <AvField id="photo-thumbnailHeight" type="number" className="form-control" name="thumbnailHeight" />
                </AvGroup>
                <AvGroup>
                  <Label id="thumbnailCaptionLabel" for="thumbnailCaption">
                    <Translate contentKey="riverApp.photo.thumbnailCaption">Thumbnail Caption</Translate>
                  </Label>
                  <AvField id="photo-thumbnailCaption" type="text" name="thumbnailCaption" />
                </AvGroup>
                <AvGroup>
                  <Label id="orientationLabel" for="orientation">
                    <Translate contentKey="riverApp.photo.orientation">Orientation</Translate>
                  </Label>
                  <AvField id="photo-orientation" type="number" className="form-control" name="orientation" />
                </AvGroup>
                <AvGroup>
                  <Label id="captionLabel" for="caption">
                    <Translate contentKey="riverApp.photo.caption">Caption</Translate>
                  </Label>
                  <AvField id="photo-caption" type="text" name="caption" />
                </AvGroup>
                <AvGroup>
                  <Label id="tagsLabel" for="tags">
                    <Translate contentKey="riverApp.photo.tags">Tags</Translate>
                  </Label>
                  <AvField id="photo-tags" type="text" name="tags" />
                </AvGroup>
                <AvGroup>
                  <Label id="altLabel" for="alt">
                    <Translate contentKey="riverApp.photo.alt">Alt</Translate>
                  </Label>
                  <AvField id="photo-alt" type="text" name="alt" />
                </AvGroup>
                <AvGroup>
                  <Label id="resourceIdLabel" for="resourceId">
                    <Translate contentKey="riverApp.photo.resourceId">Resource Id</Translate>
                  </Label>
                  <AvField id="photo-resourceId" type="number" className="form-control" name="resourceId" />
                </AvGroup>
                <AvGroup>
                  <Label id="resourceTypeLabel">
                    <Translate contentKey="riverApp.photo.resourceType">Resource Type</Translate>
                  </Label>
                  <AvInput
                    id="photo-resourceType"
                    type="select"
                    className="form-control"
                    name="resourceType"
                    value={(!isNew && photoEntity.resourceType) || 'PROJECT'}
                  >
                    <option value="PROJECT">PROJECT</option>
                    <option value="PROPERTY">PROPERTY</option>
                    <option value="EMPLOYEE">EMPLOYEE</option>
                    <option value="RESIDENTIAL_AREA">RESIDENTIAL_AREA</option>
                  </AvInput>
                </AvGroup>
                <AvGroup>
                  <Label id="fileSizeLabel" for="fileSize">
                    <Translate contentKey="riverApp.photo.fileSize">File Size</Translate>
                  </Label>
                  <AvField id="photo-fileSize" type="number" className="form-control" name="fileSize" />
                </AvGroup>
                <AvGroup>
                  <Label id="resourceOrderLabel" for="resourceOrder">
                    <Translate contentKey="riverApp.photo.resourceOrder">Resource Order</Translate>
                  </Label>
                  <AvField id="photo-resourceOrder" type="number" className="form-control" name="resourceOrder" />
                </AvGroup>
                <AvGroup>
                  <Label id="mimeTypeLabel" for="mimeType">
                    <Translate contentKey="riverApp.photo.mimeType">Mime Type</Translate>
                  </Label>
                  <AvField id="photo-mimeType" type="text" name="mimeType" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/photo" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={isInvalid || updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  photoEntity: storeState.photo.entity,
  loading: storeState.photo.loading,
  updating: storeState.photo.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhotoUpdate);
