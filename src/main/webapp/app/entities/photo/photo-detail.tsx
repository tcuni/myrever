import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './photo.reducer';
import { IPhoto } from 'app/shared/model/photo.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IPhotoDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export class PhotoDetail extends React.Component<IPhotoDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { photoEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="riverApp.photo.detail.title">Photo</Translate> [<b>{photoEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="src">
                <Translate contentKey="riverApp.photo.src">Src</Translate>
              </span>
            </dt>
            <dd>{photoEntity.src}</dd>
            <dt>
              <span id="name">
                <Translate contentKey="riverApp.photo.name">Name</Translate>
              </span>
            </dt>
            <dd>{photoEntity.name}</dd>
            <dt>
              <span id="thumbnail">
                <Translate contentKey="riverApp.photo.thumbnail">Thumbnail</Translate>
              </span>
            </dt>
            <dd>{photoEntity.thumbnail}</dd>
            <dt>
              <span id="thumbnailWidth">
                <Translate contentKey="riverApp.photo.thumbnailWidth">Thumbnail Width</Translate>
              </span>
            </dt>
            <dd>{photoEntity.thumbnailWidth}</dd>
            <dt>
              <span id="thumbnailHeight">
                <Translate contentKey="riverApp.photo.thumbnailHeight">Thumbnail Height</Translate>
              </span>
            </dt>
            <dd>{photoEntity.thumbnailHeight}</dd>
            <dt>
              <span id="thumbnailCaption">
                <Translate contentKey="riverApp.photo.thumbnailCaption">Thumbnail Caption</Translate>
              </span>
            </dt>
            <dd>{photoEntity.thumbnailCaption}</dd>
            <dt>
              <span id="orientation">
                <Translate contentKey="riverApp.photo.orientation">Orientation</Translate>
              </span>
            </dt>
            <dd>{photoEntity.orientation}</dd>
            <dt>
              <span id="caption">
                <Translate contentKey="riverApp.photo.caption">Caption</Translate>
              </span>
            </dt>
            <dd>{photoEntity.caption}</dd>
            <dt>
              <span id="tags">
                <Translate contentKey="riverApp.photo.tags">Tags</Translate>
              </span>
            </dt>
            <dd>{photoEntity.tags}</dd>
            <dt>
              <span id="alt">
                <Translate contentKey="riverApp.photo.alt">Alt</Translate>
              </span>
            </dt>
            <dd>{photoEntity.alt}</dd>
            <dt>
              <span id="resourceId">
                <Translate contentKey="riverApp.photo.resourceId">Resource Id</Translate>
              </span>
            </dt>
            <dd>{photoEntity.resourceId}</dd>
            <dt>
              <span id="resourceType">
                <Translate contentKey="riverApp.photo.resourceType">Resource Type</Translate>
              </span>
            </dt>
            <dd>{photoEntity.resourceType}</dd>
            <dt>
              <span id="fileSize">
                <Translate contentKey="riverApp.photo.fileSize">File Size</Translate>
              </span>
            </dt>
            <dd>{photoEntity.fileSize}</dd>
            <dt>
              <span id="resourceOrder">
                <Translate contentKey="riverApp.photo.resourceOrder">Resource Order</Translate>
              </span>
            </dt>
            <dd>{photoEntity.resourceOrder}</dd>
            <dt>
              <span id="mimeType">
                <Translate contentKey="riverApp.photo.mimeType">Mime Type</Translate>
              </span>
            </dt>
            <dd>{photoEntity.mimeType}</dd>
          </dl>
          <Button tag={Link} to="/entity/photo" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/photo/${photoEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ photo }: IRootState) => ({
  photoEntity: photo.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PhotoDetail);
