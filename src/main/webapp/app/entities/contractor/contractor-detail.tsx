import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntity } from './contractor.reducer';
import { IContractor } from 'app/shared/model/contractor.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContractorDetailProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export class ContractorDetail extends React.Component<IContractorDetailProps> {
  componentDidMount() {
    this.props.getEntity(this.props.match.params.id);
  }

  render() {
    const { contractorEntity } = this.props;
    return (
      <Row>
        <Col md="8">
          <h2>
            <Translate contentKey="riverApp.contractor.detail.title">Contractor</Translate> [<b>{contractorEntity.id}</b>]
          </h2>
          <dl className="jh-entity-details">
            <dt>
              <span id="contractorName">
                <Translate contentKey="riverApp.contractor.contractorName">Contractor Name</Translate>
              </span>
            </dt>
            <dd>{contractorEntity.contractorName}</dd>
            <dt>
              <span id="contractorTitle">
                <Translate contentKey="riverApp.contractor.contractorTitle">Contractor Title</Translate>
              </span>
            </dt>
            <dd>{contractorEntity.contractorTitle}</dd>
            <dt>
              <span id="contractorDate">
                <Translate contentKey="riverApp.contractor.contractorDate">Contractor Date</Translate>
              </span>
            </dt>
            <dd>
              <TextFormat value={contractorEntity.contractorDate} type="date" format={APP_LOCAL_DATE_FORMAT} />
            </dd>
            <dt>
              <span id="contractorDescription">
                <Translate contentKey="riverApp.contractor.contractorDescription">Contractor Description</Translate>
              </span>
            </dt>
            <dd>{contractorEntity.contractorDescription}</dd>
            <dt>
              <span id="contractorAddress">
                <Translate contentKey="riverApp.contractor.contractorAddress">Contractor Address</Translate>
              </span>
            </dt>
            <dd>{contractorEntity.contractorAddress}</dd>
            <dt>
              <span id="contractorWebsite">
                <Translate contentKey="riverApp.contractor.contractorWebsite">Contractor Website</Translate>
              </span>
            </dt>
            <dd>{contractorEntity.contractorWebsite}</dd>
            <dt>
              <span id="contractorPhone">
                <Translate contentKey="riverApp.contractor.contractorPhone">Contractor Phone</Translate>
              </span>
            </dt>
            <dd>{contractorEntity.contractorPhone}</dd>
            <dt>
              <span id="contractorAvatarUrl">
                <Translate contentKey="riverApp.contractor.contractorAvatarUrl">Contractor Avatar Url</Translate>
              </span>
            </dt>
            <dd>{contractorEntity.contractorAvatarUrl}</dd>
          </dl>
          <Button tag={Link} to="/entity/contractor" replace color="info">
            <FontAwesomeIcon icon="arrow-left" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.back">Back</Translate>
            </span>
          </Button>&nbsp;
          <Button tag={Link} to={`/entity/contractor/${contractorEntity.id}/edit`} replace color="primary">
            <FontAwesomeIcon icon="pencil-alt" />{' '}
            <span className="d-none d-md-inline">
              <Translate contentKey="entity.action.edit">Edit</Translate>
            </span>
          </Button>
        </Col>
      </Row>
    );
  }
}

const mapStateToProps = ({ contractor }: IRootState) => ({
  contractorEntity: contractor.entity
});

const mapDispatchToProps = { getEntity };

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContractorDetail);
