import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Col, Row, Table } from 'reactstrap';
// tslint:disable-next-line:no-unused-variable
import { Translate, ICrudGetAllAction, TextFormat } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import { IRootState } from 'app/shared/reducers';
import { getEntities } from './contractor.reducer';
import { IContractor } from 'app/shared/model/contractor.model';
// tslint:disable-next-line:no-unused-variable
import { APP_DATE_FORMAT, APP_LOCAL_DATE_FORMAT } from 'app/config/constants';

export interface IContractorProps extends StateProps, DispatchProps, RouteComponentProps<{ url: string }> {}

export class Contractor extends React.Component<IContractorProps> {
  componentDidMount() {
    this.props.getEntities();
  }

  render() {
    const { contractorList, match } = this.props;
    return (
      <div>
        <h2 id="contractor-heading">
          <Translate contentKey="riverApp.contractor.home.title">Contractors</Translate>
          <Link to={`${match.url}/new`} className="btn btn-primary float-right jh-create-entity" id="jh-create-entity">
            <FontAwesomeIcon icon="plus" />&nbsp;
            <Translate contentKey="riverApp.contractor.home.createLabel">Create new Contractor</Translate>
          </Link>
        </h2>
        <div className="table-responsive">
          <Table responsive>
            <thead>
              <tr>
                <th>
                  <Translate contentKey="global.field.id">ID</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.contractor.contractorName">Contractor Name</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.contractor.contractorTitle">Contractor Title</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.contractor.contractorDate">Contractor Date</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.contractor.contractorDescription">Contractor Description</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.contractor.contractorAddress">Contractor Address</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.contractor.contractorWebsite">Contractor Website</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.contractor.contractorPhone">Contractor Phone</Translate>
                </th>
                <th>
                  <Translate contentKey="riverApp.contractor.contractorAvatarUrl">Contractor Avatar Url</Translate>
                </th>
                <th />
              </tr>
            </thead>
            <tbody>
              {contractorList.map((contractor, i) => (
                <tr key={`entity-${i}`}>
                  <td>
                    <Button tag={Link} to={`${match.url}/${contractor.id}`} color="link" size="sm">
                      {contractor.id}
                    </Button>
                  </td>
                  <td>{contractor.contractorName}</td>
                  <td>{contractor.contractorTitle}</td>
                  <td>
                    <TextFormat type="date" value={contractor.contractorDate} format={APP_LOCAL_DATE_FORMAT} />
                  </td>
                  <td>{contractor.contractorDescription}</td>
                  <td>{contractor.contractorAddress}</td>
                  <td>{contractor.contractorWebsite}</td>
                  <td>{contractor.contractorPhone}</td>
                  <td>{contractor.contractorAvatarUrl}</td>
                  <td className="text-right">
                    <div className="btn-group flex-btn-group-container">
                      <Button tag={Link} to={`${match.url}/${contractor.id}`} color="info" size="sm">
                        <FontAwesomeIcon icon="eye" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.view">View</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${contractor.id}/edit`} color="primary" size="sm">
                        <FontAwesomeIcon icon="pencil-alt" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.edit">Edit</Translate>
                        </span>
                      </Button>
                      <Button tag={Link} to={`${match.url}/${contractor.id}/delete`} color="danger" size="sm">
                        <FontAwesomeIcon icon="trash" />{' '}
                        <span className="d-none d-md-inline">
                          <Translate contentKey="entity.action.delete">Delete</Translate>
                        </span>
                      </Button>
                    </div>
                  </td>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
      </div>
    );
  }
}

const mapStateToProps = ({ contractor }: IRootState) => ({
  contractorList: contractor.entities
});

const mapDispatchToProps = {
  getEntities
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Contractor);
