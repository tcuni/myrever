import React from 'react';
import { connect } from 'react-redux';
import { Link, RouteComponentProps } from 'react-router-dom';
import { Button, Row, Col, Label } from 'reactstrap';
import { AvForm, AvGroup, AvInput, AvField } from 'availity-reactstrap-validation';
// tslint:disable-next-line:no-unused-variable
import { Translate, translate, ICrudGetAction, ICrudGetAllAction, ICrudPutAction } from 'react-jhipster';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IRootState } from 'app/shared/reducers';

import { getEntity, updateEntity, createEntity, reset } from './contractor.reducer';
import { IContractor } from 'app/shared/model/contractor.model';
// tslint:disable-next-line:no-unused-variable
import { convertDateTimeFromServer } from 'app/shared/util/date-utils';
import { keysToValues } from 'app/shared/util/entity-utils';

export interface IContractorUpdateProps extends StateProps, DispatchProps, RouteComponentProps<{ id: number }> {}

export interface IContractorUpdateState {
  isNew: boolean;
}

export class ContractorUpdate extends React.Component<IContractorUpdateProps, IContractorUpdateState> {
  constructor(props) {
    super(props);
    this.state = {
      isNew: !this.props.match.params || !this.props.match.params.id
    };
  }

  componentDidMount() {
    if (this.state.isNew) {
      this.props.reset();
    } else {
      this.props.getEntity(this.props.match.params.id);
    }
  }

  saveEntity = (event, errors, values) => {
    if (errors.length === 0) {
      const { contractorEntity } = this.props;
      const entity = {
        ...contractorEntity,
        ...values
      };

      if (this.state.isNew) {
        this.props.createEntity(entity);
      } else {
        this.props.updateEntity(entity);
      }
      this.handleClose();
    }
  };

  handleClose = () => {
    this.props.history.push('/entity/contractor');
  };

  render() {
    const isInvalid = false;
    const { contractorEntity, loading, updating } = this.props;
    const { isNew } = this.state;

    return (
      <div>
        <Row className="justify-content-center">
          <Col md="8">
            <h2 id="riverApp.contractor.home.createOrEditLabel">
              <Translate contentKey="riverApp.contractor.home.createOrEditLabel">Create or edit a Contractor</Translate>
            </h2>
          </Col>
        </Row>
        <Row className="justify-content-center">
          <Col md="8">
            {loading ? (
              <p>Loading...</p>
            ) : (
              <AvForm model={isNew ? {} : contractorEntity} onSubmit={this.saveEntity}>
                {!isNew ? (
                  <AvGroup>
                    <Label for="id">
                      <Translate contentKey="global.field.id">ID</Translate>
                    </Label>
                    <AvInput id="contractor-id" type="text" className="form-control" name="id" required readOnly />
                  </AvGroup>
                ) : null}
                <AvGroup>
                  <Label id="contractorNameLabel" for="contractorName">
                    <Translate contentKey="riverApp.contractor.contractorName">Contractor Name</Translate>
                  </Label>
                  <AvField
                    id="contractor-contractorName"
                    type="text"
                    name="contractorName"
                    validate={{
                      required: { value: true, errorMessage: translate('entity.validation.required') },
                      maxLength: { value: 128, errorMessage: translate('entity.validation.maxlength', { max: 128 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="contractorTitleLabel" for="contractorTitle">
                    <Translate contentKey="riverApp.contractor.contractorTitle">Contractor Title</Translate>
                  </Label>
                  <AvField
                    id="contractor-contractorTitle"
                    type="text"
                    name="contractorTitle"
                    validate={{
                      maxLength: { value: 256, errorMessage: translate('entity.validation.maxlength', { max: 256 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="contractorDateLabel" for="contractorDate">
                    <Translate contentKey="riverApp.contractor.contractorDate">Contractor Date</Translate>
                  </Label>
                  <AvField id="contractor-contractorDate" type="date" className="form-control" name="contractorDate" />
                </AvGroup>
                <AvGroup>
                  <Label id="contractorDescriptionLabel" for="contractorDescription">
                    <Translate contentKey="riverApp.contractor.contractorDescription">Contractor Description</Translate>
                  </Label>
                  <AvField id="contractor-contractorDescription" type="text" name="contractorDescription" />
                </AvGroup>
                <AvGroup>
                  <Label id="contractorAddressLabel" for="contractorAddress">
                    <Translate contentKey="riverApp.contractor.contractorAddress">Contractor Address</Translate>
                  </Label>
                  <AvField id="contractor-contractorAddress" type="text" name="contractorAddress" />
                </AvGroup>
                <AvGroup>
                  <Label id="contractorWebsiteLabel" for="contractorWebsite">
                    <Translate contentKey="riverApp.contractor.contractorWebsite">Contractor Website</Translate>
                  </Label>
                  <AvField id="contractor-contractorWebsite" type="text" name="contractorWebsite" />
                </AvGroup>
                <AvGroup>
                  <Label id="contractorPhoneLabel" for="contractorPhone">
                    <Translate contentKey="riverApp.contractor.contractorPhone">Contractor Phone</Translate>
                  </Label>
                  <AvField
                    id="contractor-contractorPhone"
                    type="text"
                    name="contractorPhone"
                    validate={{
                      maxLength: { value: 16, errorMessage: translate('entity.validation.maxlength', { max: 16 }) }
                    }}
                  />
                </AvGroup>
                <AvGroup>
                  <Label id="contractorAvatarUrlLabel" for="contractorAvatarUrl">
                    <Translate contentKey="riverApp.contractor.contractorAvatarUrl">Contractor Avatar Url</Translate>
                  </Label>
                  <AvField id="contractor-contractorAvatarUrl" type="text" name="contractorAvatarUrl" />
                </AvGroup>
                <Button tag={Link} id="cancel-save" to="/entity/contractor" replace color="info">
                  <FontAwesomeIcon icon="arrow-left" />&nbsp;
                  <span className="d-none d-md-inline">
                    <Translate contentKey="entity.action.back">Back</Translate>
                  </span>
                </Button>
                &nbsp;
                <Button color="primary" id="save-entity" type="submit" disabled={isInvalid || updating}>
                  <FontAwesomeIcon icon="save" />&nbsp;
                  <Translate contentKey="entity.action.save">Save</Translate>
                </Button>
              </AvForm>
            )}
          </Col>
        </Row>
      </div>
    );
  }
}

const mapStateToProps = (storeState: IRootState) => ({
  contractorEntity: storeState.contractor.entity,
  loading: storeState.contractor.loading,
  updating: storeState.contractor.updating
});

const mapDispatchToProps = {
  getEntity,
  updateEntity,
  createEntity,
  reset
};

type StateProps = ReturnType<typeof mapStateToProps>;
type DispatchProps = typeof mapDispatchToProps;

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ContractorUpdate);
