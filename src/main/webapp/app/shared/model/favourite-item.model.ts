import { IUser } from './user.model';

export const enum ResourceType {
  PROJECT = 'PROJECT',
  PROPERTY = 'PROPERTY',
  EMPLOYEE = 'EMPLOYEE',
  RESIDENTIAL_AREA = 'RESIDENTIAL_AREA'
}

export interface IFavouriteItem {
  id?: number;
  favourited?: boolean;
  resourceId?: number;
  resourceType?: ResourceType;
  user?: IUser;
}

export const defaultValue: Readonly<IFavouriteItem> = {
  favourited: false
};
