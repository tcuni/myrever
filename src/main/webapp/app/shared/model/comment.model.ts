import { Moment } from 'moment';
import { IUser } from './user.model';
import { IBlogPost } from './blog-post.model';

export interface IComment {
  id?: number;
  commentContent?: string;
  commentTimeStamp?: Moment;
  user?: IUser;
  post?: IBlogPost;
}

export const defaultValue: Readonly<IComment> = {};
