import { Moment } from 'moment';
import { INotification } from './notification.model';
import { IUser } from './user.model';

export interface IUserNotification {
  id?: number;
  isSeen?: boolean;
  seenDate?: Moment;
  notification?: INotification;
  receiver?: IUser;
}

export const defaultValue: Readonly<IUserNotification> = {
  isSeen: false
};
