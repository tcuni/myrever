export const enum ResourceType {
  PROJECT = 'PROJECT',
  PROPERTY = 'PROPERTY',
  EMPLOYEE = 'EMPLOYEE',
  RESIDENTIAL_AREA = 'RESIDENTIAL_AREA'
}

export interface IPhoto {
  id?: number;
  src?: string;
  name?: string;
  thumbnail?: string;
  thumbnailWidth?: number;
  thumbnailHeight?: number;
  thumbnailCaption?: string;
  orientation?: number;
  caption?: string;
  tags?: string;
  alt?: string;
  resourceId?: number;
  resourceType?: ResourceType;
  fileSize?: number;
  resourceOrder?: number;
  mimeType?: string;
}

export const defaultValue: Readonly<IPhoto> = {};
