import { Moment } from 'moment';
import { IUser } from './user.model';
import { IDepartment } from './department.model';
import { IJobTitle } from './job-title.model';

export const enum Gender {
  MALE = 'MALE',
  FEMALE = 'FEMALE',
  OTHER = 'OTHER'
}

export interface IEmployee {
  id?: number;
  employeeFirstName?: string;
  employeeLastName?: string;
  employeeDob?: Moment;
  employeeSex?: Gender;
  employeeIdentityCard?: string;
  employeePhone?: string;
  employeeEmail?: string;
  account?: IUser;
  department?: IDepartment;
  jobtitle?: IJobTitle;
}

export const defaultValue: Readonly<IEmployee> = {};
