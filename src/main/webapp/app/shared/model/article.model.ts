import { Moment } from 'moment';
import { IUser } from './user.model';
import { ISupportCategory } from './support-category.model';

export const enum BlogStatus {
  PUBLISHED = 'PUBLISHED',
  DRAFT = 'DRAFT',
  DELETED = 'DELETED'
}

export interface IArticle {
  id?: number;
  articleTitle?: string;
  articleStatus?: BlogStatus;
  articleDate?: Moment;
  articleSeenCount?: number;
  articleContent?: any;
  author?: IUser;
  category?: ISupportCategory;
}

export const defaultValue: Readonly<IArticle> = {};
