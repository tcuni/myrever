export const enum ResourceType {
  PROJECT = 'PROJECT',
  PROPERTY = 'PROPERTY',
  EMPLOYEE = 'EMPLOYEE',
  RESIDENTIAL_AREA = 'RESIDENTIAL_AREA'
}

export interface IDocument {
  id?: number;
  name?: string;
  src?: string;
  mimeType?: string;
  fileSize?: number;
  resourceType?: ResourceType;
  resourceId?: number;
  resourceOrder?: number;
  thumbnail?: string;
  pages?: number;
  caption?: string;
  tags?: string;
}

export const defaultValue: Readonly<IDocument> = {};
