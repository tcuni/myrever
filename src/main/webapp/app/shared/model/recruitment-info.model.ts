import { Moment } from 'moment';
import { IUser } from './user.model';
import { IJobTitle } from './job-title.model';

export interface IRecruitmentInfo {
  id?: number;
  recruitmentTitle?: string;
  recruitmentAvatarUrl?: string;
  recruitmentContent?: any;
  recruitmentNotes?: string;
  recruitmentDate?: Moment;
  recruitmentSeenCount?: number;
  recruitmentStatus?: boolean;
  user?: IUser;
  jobtitle?: IJobTitle;
}

export const defaultValue: Readonly<IRecruitmentInfo> = {
  recruitmentStatus: false
};
