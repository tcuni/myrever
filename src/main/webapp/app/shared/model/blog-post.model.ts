import { Moment } from 'moment';
import { ICategory } from './category.model';
import { IUser } from './user.model';
import { IProject } from './project.model';

export const enum BlogStatus {
  PUBLISHED = 'PUBLISHED',
  DRAFT = 'DRAFT',
  DELETED = 'DELETED'
}

export interface IBlogPost {
  id?: number;
  postTitle?: string;
  postStatus?: BlogStatus;
  postCreatedDate?: Moment;
  postSeenCount?: number;
  postContent?: any;
  category?: ICategory;
  user?: IUser;
  project?: IProject;
}

export const defaultValue: Readonly<IBlogPost> = {};
