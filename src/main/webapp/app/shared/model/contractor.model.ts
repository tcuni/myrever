import { Moment } from 'moment';

export interface IContractor {
  id?: number;
  contractorName?: string;
  contractorTitle?: string;
  contractorDate?: Moment;
  contractorDescription?: string;
  contractorAddress?: string;
  contractorWebsite?: string;
  contractorPhone?: string;
  contractorAvatarUrl?: string;
}

export const defaultValue: Readonly<IContractor> = {};
