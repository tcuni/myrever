/* tslint:disable no-unused-expression */
import { browser } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import PhotoComponentsPage from './photo.page-object';
import PhotoUpdatePage from './photo-update.page-object';

const expect = chai.expect;

describe('Photo e2e test', () => {
  let navBarPage: NavBarPage;
  let photoUpdatePage: PhotoUpdatePage;
  let photoComponentsPage: PhotoComponentsPage;

  before(() => {
    browser.get('/');
    navBarPage = new NavBarPage();
    navBarPage.autoSignIn();
  });

  it('should load Photos', async () => {
    navBarPage.getEntityPage('photo');
    photoComponentsPage = new PhotoComponentsPage();
    expect(await photoComponentsPage.getTitle().getText()).to.match(/Photos/);
  });

  it('should load create Photo page', async () => {
    photoComponentsPage.clickOnCreateButton();
    photoUpdatePage = new PhotoUpdatePage();
    expect(await photoUpdatePage.getPageTitle().getAttribute('id')).to.match(/riverApp.photo.home.createOrEditLabel/);
  });

  it('should create and save Photos', async () => {
    photoUpdatePage.setSrcInput('src');
    expect(await photoUpdatePage.getSrcInput()).to.match(/src/);
    photoUpdatePage.setNameInput('name');
    expect(await photoUpdatePage.getNameInput()).to.match(/name/);
    photoUpdatePage.setThumbnailInput('thumbnail');
    expect(await photoUpdatePage.getThumbnailInput()).to.match(/thumbnail/);
    photoUpdatePage.setThumbnailWidthInput('5');
    expect(await photoUpdatePage.getThumbnailWidthInput()).to.eq('5');
    photoUpdatePage.setThumbnailHeightInput('5');
    expect(await photoUpdatePage.getThumbnailHeightInput()).to.eq('5');
    photoUpdatePage.setThumbnailCaptionInput('thumbnailCaption');
    expect(await photoUpdatePage.getThumbnailCaptionInput()).to.match(/thumbnailCaption/);
    photoUpdatePage.setOrientationInput('5');
    expect(await photoUpdatePage.getOrientationInput()).to.eq('5');
    photoUpdatePage.setCaptionInput('caption');
    expect(await photoUpdatePage.getCaptionInput()).to.match(/caption/);
    photoUpdatePage.setTagsInput('tags');
    expect(await photoUpdatePage.getTagsInput()).to.match(/tags/);
    photoUpdatePage.setAltInput('alt');
    expect(await photoUpdatePage.getAltInput()).to.match(/alt/);
    photoUpdatePage.setResourceIdInput('5');
    expect(await photoUpdatePage.getResourceIdInput()).to.eq('5');
    photoUpdatePage.resourceTypeSelectLastOption();
    photoUpdatePage.setFileSizeInput('5');
    expect(await photoUpdatePage.getFileSizeInput()).to.eq('5');
    photoUpdatePage.setResourceOrderInput('5');
    expect(await photoUpdatePage.getResourceOrderInput()).to.eq('5');
    photoUpdatePage.setMimeTypeInput('mimeType');
    expect(await photoUpdatePage.getMimeTypeInput()).to.match(/mimeType/);
    await photoUpdatePage.save();
    expect(await photoUpdatePage.getSaveButton().isPresent()).to.be.false;
  });

  after(() => {
    navBarPage.autoSignOut();
  });
});
