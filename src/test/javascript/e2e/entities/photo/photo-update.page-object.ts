import { element, by, ElementFinder } from 'protractor';

export default class PhotoUpdatePage {
  pageTitle: ElementFinder = element(by.id('riverApp.photo.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  srcInput: ElementFinder = element(by.css('input#photo-src'));
  nameInput: ElementFinder = element(by.css('input#photo-name'));
  thumbnailInput: ElementFinder = element(by.css('input#photo-thumbnail'));
  thumbnailWidthInput: ElementFinder = element(by.css('input#photo-thumbnailWidth'));
  thumbnailHeightInput: ElementFinder = element(by.css('input#photo-thumbnailHeight'));
  thumbnailCaptionInput: ElementFinder = element(by.css('input#photo-thumbnailCaption'));
  orientationInput: ElementFinder = element(by.css('input#photo-orientation'));
  captionInput: ElementFinder = element(by.css('input#photo-caption'));
  tagsInput: ElementFinder = element(by.css('input#photo-tags'));
  altInput: ElementFinder = element(by.css('input#photo-alt'));
  resourceIdInput: ElementFinder = element(by.css('input#photo-resourceId'));
  resourceTypeSelect: ElementFinder = element(by.css('select#photo-resourceType'));
  fileSizeInput: ElementFinder = element(by.css('input#photo-fileSize'));
  resourceOrderInput: ElementFinder = element(by.css('input#photo-resourceOrder'));
  mimeTypeInput: ElementFinder = element(by.css('input#photo-mimeType'));

  getPageTitle() {
    return this.pageTitle;
  }

  setSrcInput(src) {
    this.srcInput.sendKeys(src);
  }

  getSrcInput() {
    return this.srcInput.getAttribute('value');
  }

  setNameInput(name) {
    this.nameInput.sendKeys(name);
  }

  getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  setThumbnailInput(thumbnail) {
    this.thumbnailInput.sendKeys(thumbnail);
  }

  getThumbnailInput() {
    return this.thumbnailInput.getAttribute('value');
  }

  setThumbnailWidthInput(thumbnailWidth) {
    this.thumbnailWidthInput.sendKeys(thumbnailWidth);
  }

  getThumbnailWidthInput() {
    return this.thumbnailWidthInput.getAttribute('value');
  }

  setThumbnailHeightInput(thumbnailHeight) {
    this.thumbnailHeightInput.sendKeys(thumbnailHeight);
  }

  getThumbnailHeightInput() {
    return this.thumbnailHeightInput.getAttribute('value');
  }

  setThumbnailCaptionInput(thumbnailCaption) {
    this.thumbnailCaptionInput.sendKeys(thumbnailCaption);
  }

  getThumbnailCaptionInput() {
    return this.thumbnailCaptionInput.getAttribute('value');
  }

  setOrientationInput(orientation) {
    this.orientationInput.sendKeys(orientation);
  }

  getOrientationInput() {
    return this.orientationInput.getAttribute('value');
  }

  setCaptionInput(caption) {
    this.captionInput.sendKeys(caption);
  }

  getCaptionInput() {
    return this.captionInput.getAttribute('value');
  }

  setTagsInput(tags) {
    this.tagsInput.sendKeys(tags);
  }

  getTagsInput() {
    return this.tagsInput.getAttribute('value');
  }

  setAltInput(alt) {
    this.altInput.sendKeys(alt);
  }

  getAltInput() {
    return this.altInput.getAttribute('value');
  }

  setResourceIdInput(resourceId) {
    this.resourceIdInput.sendKeys(resourceId);
  }

  getResourceIdInput() {
    return this.resourceIdInput.getAttribute('value');
  }

  setResourceTypeSelect(resourceType) {
    this.resourceTypeSelect.sendKeys(resourceType);
  }

  getResourceTypeSelect() {
    return this.resourceTypeSelect.element(by.css('option:checked')).getText();
  }

  resourceTypeSelectLastOption() {
    this.resourceTypeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }
  setFileSizeInput(fileSize) {
    this.fileSizeInput.sendKeys(fileSize);
  }

  getFileSizeInput() {
    return this.fileSizeInput.getAttribute('value');
  }

  setResourceOrderInput(resourceOrder) {
    this.resourceOrderInput.sendKeys(resourceOrder);
  }

  getResourceOrderInput() {
    return this.resourceOrderInput.getAttribute('value');
  }

  setMimeTypeInput(mimeType) {
    this.mimeTypeInput.sendKeys(mimeType);
  }

  getMimeTypeInput() {
    return this.mimeTypeInput.getAttribute('value');
  }

  save() {
    return this.saveButton.click();
  }

  cancel() {
    this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
