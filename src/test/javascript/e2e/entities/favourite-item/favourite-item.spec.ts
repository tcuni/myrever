/* tslint:disable no-unused-expression */
import { browser } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import FavouriteItemComponentsPage from './favourite-item.page-object';
import FavouriteItemUpdatePage from './favourite-item-update.page-object';

const expect = chai.expect;

describe('FavouriteItem e2e test', () => {
  let navBarPage: NavBarPage;
  let favouriteItemUpdatePage: FavouriteItemUpdatePage;
  let favouriteItemComponentsPage: FavouriteItemComponentsPage;

  before(() => {
    browser.get('/');
    navBarPage = new NavBarPage();
    navBarPage.autoSignIn();
  });

  it('should load FavouriteItems', async () => {
    navBarPage.getEntityPage('favourite-item');
    favouriteItemComponentsPage = new FavouriteItemComponentsPage();
    expect(await favouriteItemComponentsPage.getTitle().getText()).to.match(/Favourite Items/);
  });

  it('should load create FavouriteItem page', async () => {
    favouriteItemComponentsPage.clickOnCreateButton();
    favouriteItemUpdatePage = new FavouriteItemUpdatePage();
    expect(await favouriteItemUpdatePage.getPageTitle().getAttribute('id')).to.match(/riverApp.favouriteItem.home.createOrEditLabel/);
  });

  it('should create and save FavouriteItems', async () => {
    const selectedFavourited = await favouriteItemUpdatePage.getFavouritedInput().isSelected();
    if (selectedFavourited) {
      favouriteItemUpdatePage.getFavouritedInput().click();
      expect(await favouriteItemUpdatePage.getFavouritedInput().isSelected()).to.be.false;
    } else {
      favouriteItemUpdatePage.getFavouritedInput().click();
      expect(await favouriteItemUpdatePage.getFavouritedInput().isSelected()).to.be.true;
    }
    favouriteItemUpdatePage.setResourceIdInput('5');
    expect(await favouriteItemUpdatePage.getResourceIdInput()).to.eq('5');
    favouriteItemUpdatePage.resourceTypeSelectLastOption();
    favouriteItemUpdatePage.userSelectLastOption();
    await favouriteItemUpdatePage.save();
    expect(await favouriteItemUpdatePage.getSaveButton().isPresent()).to.be.false;
  });

  after(() => {
    navBarPage.autoSignOut();
  });
});
