import { element, by, ElementFinder } from 'protractor';

export default class FavouriteItemUpdatePage {
  pageTitle: ElementFinder = element(by.id('riverApp.favouriteItem.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  favouritedInput: ElementFinder = element(by.css('input#favourite-item-favourited'));
  resourceIdInput: ElementFinder = element(by.css('input#favourite-item-resourceId'));
  resourceTypeSelect: ElementFinder = element(by.css('select#favourite-item-resourceType'));
  userSelect: ElementFinder = element(by.css('select#favourite-item-user'));

  getPageTitle() {
    return this.pageTitle;
  }

  getFavouritedInput() {
    return this.favouritedInput;
  }
  setResourceIdInput(resourceId) {
    this.resourceIdInput.sendKeys(resourceId);
  }

  getResourceIdInput() {
    return this.resourceIdInput.getAttribute('value');
  }

  setResourceTypeSelect(resourceType) {
    this.resourceTypeSelect.sendKeys(resourceType);
  }

  getResourceTypeSelect() {
    return this.resourceTypeSelect.element(by.css('option:checked')).getText();
  }

  resourceTypeSelectLastOption() {
    this.resourceTypeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }
  userSelectLastOption() {
    this.userSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  userSelectOption(option) {
    this.userSelect.sendKeys(option);
  }

  getUserSelect() {
    return this.userSelect;
  }

  getUserSelectedOption() {
    return this.userSelect.element(by.css('option:checked')).getText();
  }

  save() {
    return this.saveButton.click();
  }

  cancel() {
    this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
