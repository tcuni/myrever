/* tslint:disable no-unused-expression */
import { browser } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import DocumentComponentsPage from './document.page-object';
import DocumentUpdatePage from './document-update.page-object';

const expect = chai.expect;

describe('Document e2e test', () => {
  let navBarPage: NavBarPage;
  let documentUpdatePage: DocumentUpdatePage;
  let documentComponentsPage: DocumentComponentsPage;

  before(() => {
    browser.get('/');
    navBarPage = new NavBarPage();
    navBarPage.autoSignIn();
  });

  it('should load Documents', async () => {
    navBarPage.getEntityPage('document');
    documentComponentsPage = new DocumentComponentsPage();
    expect(await documentComponentsPage.getTitle().getText()).to.match(/Documents/);
  });

  it('should load create Document page', async () => {
    documentComponentsPage.clickOnCreateButton();
    documentUpdatePage = new DocumentUpdatePage();
    expect(await documentUpdatePage.getPageTitle().getAttribute('id')).to.match(/riverApp.document.home.createOrEditLabel/);
  });

  it('should create and save Documents', async () => {
    documentUpdatePage.setNameInput('name');
    expect(await documentUpdatePage.getNameInput()).to.match(/name/);
    documentUpdatePage.setSrcInput('src');
    expect(await documentUpdatePage.getSrcInput()).to.match(/src/);
    documentUpdatePage.setMimeTypeInput('mimeType');
    expect(await documentUpdatePage.getMimeTypeInput()).to.match(/mimeType/);
    documentUpdatePage.setFileSizeInput('5');
    expect(await documentUpdatePage.getFileSizeInput()).to.eq('5');
    documentUpdatePage.resourceTypeSelectLastOption();
    documentUpdatePage.setResourceIdInput('5');
    expect(await documentUpdatePage.getResourceIdInput()).to.eq('5');
    documentUpdatePage.setResourceOrderInput('5');
    expect(await documentUpdatePage.getResourceOrderInput()).to.eq('5');
    documentUpdatePage.setThumbnailInput('thumbnail');
    expect(await documentUpdatePage.getThumbnailInput()).to.match(/thumbnail/);
    documentUpdatePage.setPagesInput('5');
    expect(await documentUpdatePage.getPagesInput()).to.eq('5');
    documentUpdatePage.setCaptionInput('caption');
    expect(await documentUpdatePage.getCaptionInput()).to.match(/caption/);
    documentUpdatePage.setTagsInput('tags');
    expect(await documentUpdatePage.getTagsInput()).to.match(/tags/);
    await documentUpdatePage.save();
    expect(await documentUpdatePage.getSaveButton().isPresent()).to.be.false;
  });

  after(() => {
    navBarPage.autoSignOut();
  });
});
