import { element, by, ElementFinder } from 'protractor';

export default class DocumentUpdatePage {
  pageTitle: ElementFinder = element(by.id('riverApp.document.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  nameInput: ElementFinder = element(by.css('input#document-name'));
  srcInput: ElementFinder = element(by.css('input#document-src'));
  mimeTypeInput: ElementFinder = element(by.css('input#document-mimeType'));
  fileSizeInput: ElementFinder = element(by.css('input#document-fileSize'));
  resourceTypeSelect: ElementFinder = element(by.css('select#document-resourceType'));
  resourceIdInput: ElementFinder = element(by.css('input#document-resourceId'));
  resourceOrderInput: ElementFinder = element(by.css('input#document-resourceOrder'));
  thumbnailInput: ElementFinder = element(by.css('input#document-thumbnail'));
  pagesInput: ElementFinder = element(by.css('input#document-pages'));
  captionInput: ElementFinder = element(by.css('input#document-caption'));
  tagsInput: ElementFinder = element(by.css('input#document-tags'));

  getPageTitle() {
    return this.pageTitle;
  }

  setNameInput(name) {
    this.nameInput.sendKeys(name);
  }

  getNameInput() {
    return this.nameInput.getAttribute('value');
  }

  setSrcInput(src) {
    this.srcInput.sendKeys(src);
  }

  getSrcInput() {
    return this.srcInput.getAttribute('value');
  }

  setMimeTypeInput(mimeType) {
    this.mimeTypeInput.sendKeys(mimeType);
  }

  getMimeTypeInput() {
    return this.mimeTypeInput.getAttribute('value');
  }

  setFileSizeInput(fileSize) {
    this.fileSizeInput.sendKeys(fileSize);
  }

  getFileSizeInput() {
    return this.fileSizeInput.getAttribute('value');
  }

  setResourceTypeSelect(resourceType) {
    this.resourceTypeSelect.sendKeys(resourceType);
  }

  getResourceTypeSelect() {
    return this.resourceTypeSelect.element(by.css('option:checked')).getText();
  }

  resourceTypeSelectLastOption() {
    this.resourceTypeSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }
  setResourceIdInput(resourceId) {
    this.resourceIdInput.sendKeys(resourceId);
  }

  getResourceIdInput() {
    return this.resourceIdInput.getAttribute('value');
  }

  setResourceOrderInput(resourceOrder) {
    this.resourceOrderInput.sendKeys(resourceOrder);
  }

  getResourceOrderInput() {
    return this.resourceOrderInput.getAttribute('value');
  }

  setThumbnailInput(thumbnail) {
    this.thumbnailInput.sendKeys(thumbnail);
  }

  getThumbnailInput() {
    return this.thumbnailInput.getAttribute('value');
  }

  setPagesInput(pages) {
    this.pagesInput.sendKeys(pages);
  }

  getPagesInput() {
    return this.pagesInput.getAttribute('value');
  }

  setCaptionInput(caption) {
    this.captionInput.sendKeys(caption);
  }

  getCaptionInput() {
    return this.captionInput.getAttribute('value');
  }

  setTagsInput(tags) {
    this.tagsInput.sendKeys(tags);
  }

  getTagsInput() {
    return this.tagsInput.getAttribute('value');
  }

  save() {
    return this.saveButton.click();
  }

  cancel() {
    this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
