/* tslint:disable no-unused-expression */
import { browser, protractor } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import UserNotificationComponentsPage from './user-notification.page-object';
import UserNotificationUpdatePage from './user-notification-update.page-object';

const expect = chai.expect;

describe('UserNotification e2e test', () => {
  let navBarPage: NavBarPage;
  let userNotificationUpdatePage: UserNotificationUpdatePage;
  let userNotificationComponentsPage: UserNotificationComponentsPage;

  before(() => {
    browser.get('/');
    navBarPage = new NavBarPage();
    navBarPage.autoSignIn();
  });

  it('should load UserNotifications', async () => {
    navBarPage.getEntityPage('user-notification');
    userNotificationComponentsPage = new UserNotificationComponentsPage();
    expect(await userNotificationComponentsPage.getTitle().getText()).to.match(/User Notifications/);
  });

  it('should load create UserNotification page', async () => {
    userNotificationComponentsPage.clickOnCreateButton();
    userNotificationUpdatePage = new UserNotificationUpdatePage();
    expect(await userNotificationUpdatePage.getPageTitle().getAttribute('id')).to.match(/riverApp.userNotification.home.createOrEditLabel/);
  });

  it('should create and save UserNotifications', async () => {
    const selectedIsSeen = await userNotificationUpdatePage.getIsSeenInput().isSelected();
    if (selectedIsSeen) {
      userNotificationUpdatePage.getIsSeenInput().click();
      expect(await userNotificationUpdatePage.getIsSeenInput().isSelected()).to.be.false;
    } else {
      userNotificationUpdatePage.getIsSeenInput().click();
      expect(await userNotificationUpdatePage.getIsSeenInput().isSelected()).to.be.true;
    }
    userNotificationUpdatePage.setSeenDateInput('01/01/2001' + protractor.Key.TAB + '02:30AM');
    expect(await userNotificationUpdatePage.getSeenDateInput()).to.contain('2001-01-01T02:30');
    userNotificationUpdatePage.notificationSelectLastOption();
    userNotificationUpdatePage.receiverSelectLastOption();
    await userNotificationUpdatePage.save();
    expect(await userNotificationUpdatePage.getSaveButton().isPresent()).to.be.false;
  });

  after(() => {
    navBarPage.autoSignOut();
  });
});
