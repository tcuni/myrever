import { element, by, ElementFinder } from 'protractor';

export default class UserNotificationUpdatePage {
  pageTitle: ElementFinder = element(by.id('riverApp.userNotification.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  isSeenInput: ElementFinder = element(by.css('input#user-notification-isSeen'));
  seenDateInput: ElementFinder = element(by.css('input#user-notification-seenDate'));
  notificationSelect: ElementFinder = element(by.css('select#user-notification-notification'));
  receiverSelect: ElementFinder = element(by.css('select#user-notification-receiver'));

  getPageTitle() {
    return this.pageTitle;
  }

  getIsSeenInput() {
    return this.isSeenInput;
  }
  setSeenDateInput(seenDate) {
    this.seenDateInput.sendKeys(seenDate);
  }

  getSeenDateInput() {
    return this.seenDateInput.getAttribute('value');
  }

  notificationSelectLastOption() {
    this.notificationSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  notificationSelectOption(option) {
    this.notificationSelect.sendKeys(option);
  }

  getNotificationSelect() {
    return this.notificationSelect;
  }

  getNotificationSelectedOption() {
    return this.notificationSelect.element(by.css('option:checked')).getText();
  }

  receiverSelectLastOption() {
    this.receiverSelect
      .all(by.tagName('option'))
      .last()
      .click();
  }

  receiverSelectOption(option) {
    this.receiverSelect.sendKeys(option);
  }

  getReceiverSelect() {
    return this.receiverSelect;
  }

  getReceiverSelectedOption() {
    return this.receiverSelect.element(by.css('option:checked')).getText();
  }

  save() {
    return this.saveButton.click();
  }

  cancel() {
    this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
