/* tslint:disable no-unused-expression */
import { browser } from 'protractor';

import NavBarPage from './../../page-objects/navbar-page';
import ContractorComponentsPage from './contractor.page-object';
import ContractorUpdatePage from './contractor-update.page-object';

const expect = chai.expect;

describe('Contractor e2e test', () => {
  let navBarPage: NavBarPage;
  let contractorUpdatePage: ContractorUpdatePage;
  let contractorComponentsPage: ContractorComponentsPage;

  before(() => {
    browser.get('/');
    navBarPage = new NavBarPage();
    navBarPage.autoSignIn();
  });

  it('should load Contractors', async () => {
    navBarPage.getEntityPage('contractor');
    contractorComponentsPage = new ContractorComponentsPage();
    expect(await contractorComponentsPage.getTitle().getText()).to.match(/Contractors/);
  });

  it('should load create Contractor page', async () => {
    contractorComponentsPage.clickOnCreateButton();
    contractorUpdatePage = new ContractorUpdatePage();
    expect(await contractorUpdatePage.getPageTitle().getAttribute('id')).to.match(/riverApp.contractor.home.createOrEditLabel/);
  });

  it('should create and save Contractors', async () => {
    contractorUpdatePage.setContractorNameInput('contractorName');
    expect(await contractorUpdatePage.getContractorNameInput()).to.match(/contractorName/);
    contractorUpdatePage.setContractorTitleInput('contractorTitle');
    expect(await contractorUpdatePage.getContractorTitleInput()).to.match(/contractorTitle/);
    contractorUpdatePage.setContractorDateInput('01-01-2001');
    expect(await contractorUpdatePage.getContractorDateInput()).to.eq('2001-01-01');
    contractorUpdatePage.setContractorDescriptionInput('contractorDescription');
    expect(await contractorUpdatePage.getContractorDescriptionInput()).to.match(/contractorDescription/);
    contractorUpdatePage.setContractorAddressInput('contractorAddress');
    expect(await contractorUpdatePage.getContractorAddressInput()).to.match(/contractorAddress/);
    contractorUpdatePage.setContractorWebsiteInput('contractorWebsite');
    expect(await contractorUpdatePage.getContractorWebsiteInput()).to.match(/contractorWebsite/);
    contractorUpdatePage.setContractorPhoneInput('contractorPhone');
    expect(await contractorUpdatePage.getContractorPhoneInput()).to.match(/contractorPhone/);
    contractorUpdatePage.setContractorAvatarUrlInput('contractorAvatarUrl');
    expect(await contractorUpdatePage.getContractorAvatarUrlInput()).to.match(/contractorAvatarUrl/);
    await contractorUpdatePage.save();
    expect(await contractorUpdatePage.getSaveButton().isPresent()).to.be.false;
  });

  after(() => {
    navBarPage.autoSignOut();
  });
});
