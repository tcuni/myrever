import { element, by, ElementFinder } from 'protractor';

export default class ContractorUpdatePage {
  pageTitle: ElementFinder = element(by.id('riverApp.contractor.home.createOrEditLabel'));
  saveButton: ElementFinder = element(by.id('save-entity'));
  cancelButton: ElementFinder = element(by.id('cancel-save'));
  contractorNameInput: ElementFinder = element(by.css('input#contractor-contractorName'));
  contractorTitleInput: ElementFinder = element(by.css('input#contractor-contractorTitle'));
  contractorDateInput: ElementFinder = element(by.css('input#contractor-contractorDate'));
  contractorDescriptionInput: ElementFinder = element(by.css('input#contractor-contractorDescription'));
  contractorAddressInput: ElementFinder = element(by.css('input#contractor-contractorAddress'));
  contractorWebsiteInput: ElementFinder = element(by.css('input#contractor-contractorWebsite'));
  contractorPhoneInput: ElementFinder = element(by.css('input#contractor-contractorPhone'));
  contractorAvatarUrlInput: ElementFinder = element(by.css('input#contractor-contractorAvatarUrl'));

  getPageTitle() {
    return this.pageTitle;
  }

  setContractorNameInput(contractorName) {
    this.contractorNameInput.sendKeys(contractorName);
  }

  getContractorNameInput() {
    return this.contractorNameInput.getAttribute('value');
  }

  setContractorTitleInput(contractorTitle) {
    this.contractorTitleInput.sendKeys(contractorTitle);
  }

  getContractorTitleInput() {
    return this.contractorTitleInput.getAttribute('value');
  }

  setContractorDateInput(contractorDate) {
    this.contractorDateInput.sendKeys(contractorDate);
  }

  getContractorDateInput() {
    return this.contractorDateInput.getAttribute('value');
  }

  setContractorDescriptionInput(contractorDescription) {
    this.contractorDescriptionInput.sendKeys(contractorDescription);
  }

  getContractorDescriptionInput() {
    return this.contractorDescriptionInput.getAttribute('value');
  }

  setContractorAddressInput(contractorAddress) {
    this.contractorAddressInput.sendKeys(contractorAddress);
  }

  getContractorAddressInput() {
    return this.contractorAddressInput.getAttribute('value');
  }

  setContractorWebsiteInput(contractorWebsite) {
    this.contractorWebsiteInput.sendKeys(contractorWebsite);
  }

  getContractorWebsiteInput() {
    return this.contractorWebsiteInput.getAttribute('value');
  }

  setContractorPhoneInput(contractorPhone) {
    this.contractorPhoneInput.sendKeys(contractorPhone);
  }

  getContractorPhoneInput() {
    return this.contractorPhoneInput.getAttribute('value');
  }

  setContractorAvatarUrlInput(contractorAvatarUrl) {
    this.contractorAvatarUrlInput.sendKeys(contractorAvatarUrl);
  }

  getContractorAvatarUrlInput() {
    return this.contractorAvatarUrlInput.getAttribute('value');
  }

  save() {
    return this.saveButton.click();
  }

  cancel() {
    this.cancelButton.click();
  }

  getSaveButton() {
    return this.saveButton;
  }
}
