package com.tcutma.realstate.web.rest;

import com.tcutma.realstate.RiverApp;

import com.tcutma.realstate.domain.Contractor;
import com.tcutma.realstate.repository.ContractorRepository;
import com.tcutma.realstate.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;


import static com.tcutma.realstate.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ContractorResource REST controller.
 *
 * @see ContractorResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RiverApp.class)
public class ContractorResourceIntTest {

    private static final String DEFAULT_CONTRACTOR_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CONTRACTOR_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CONTRACTOR_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_CONTRACTOR_TITLE = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_CONTRACTOR_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CONTRACTOR_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_CONTRACTOR_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_CONTRACTOR_DESCRIPTION = "BBBBBBBBBB";

    private static final String DEFAULT_CONTRACTOR_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_CONTRACTOR_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_CONTRACTOR_WEBSITE = "AAAAAAAAAA";
    private static final String UPDATED_CONTRACTOR_WEBSITE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTRACTOR_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_CONTRACTOR_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTRACTOR_AVATAR_URL = "AAAAAAAAAA";
    private static final String UPDATED_CONTRACTOR_AVATAR_URL = "BBBBBBBBBB";

    @Autowired
    private ContractorRepository contractorRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restContractorMockMvc;

    private Contractor contractor;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ContractorResource contractorResource = new ContractorResource(contractorRepository);
        this.restContractorMockMvc = MockMvcBuilders.standaloneSetup(contractorResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Contractor createEntity(EntityManager em) {
        Contractor contractor = new Contractor()
            .contractorName(DEFAULT_CONTRACTOR_NAME)
            .contractorTitle(DEFAULT_CONTRACTOR_TITLE)
            .contractorDate(DEFAULT_CONTRACTOR_DATE)
            .contractorDescription(DEFAULT_CONTRACTOR_DESCRIPTION)
            .contractorAddress(DEFAULT_CONTRACTOR_ADDRESS)
            .contractorWebsite(DEFAULT_CONTRACTOR_WEBSITE)
            .contractorPhone(DEFAULT_CONTRACTOR_PHONE)
            .contractorAvatarUrl(DEFAULT_CONTRACTOR_AVATAR_URL);
        return contractor;
    }

    @Before
    public void initTest() {
        contractor = createEntity(em);
    }

    @Test
    @Transactional
    public void createContractor() throws Exception {
        int databaseSizeBeforeCreate = contractorRepository.findAll().size();

        // Create the Contractor
        restContractorMockMvc.perform(post("/api/contractors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contractor)))
            .andExpect(status().isCreated());

        // Validate the Contractor in the database
        List<Contractor> contractorList = contractorRepository.findAll();
        assertThat(contractorList).hasSize(databaseSizeBeforeCreate + 1);
        Contractor testContractor = contractorList.get(contractorList.size() - 1);
        assertThat(testContractor.getContractorName()).isEqualTo(DEFAULT_CONTRACTOR_NAME);
        assertThat(testContractor.getContractorTitle()).isEqualTo(DEFAULT_CONTRACTOR_TITLE);
        assertThat(testContractor.getContractorDate()).isEqualTo(DEFAULT_CONTRACTOR_DATE);
        assertThat(testContractor.getContractorDescription()).isEqualTo(DEFAULT_CONTRACTOR_DESCRIPTION);
        assertThat(testContractor.getContractorAddress()).isEqualTo(DEFAULT_CONTRACTOR_ADDRESS);
        assertThat(testContractor.getContractorWebsite()).isEqualTo(DEFAULT_CONTRACTOR_WEBSITE);
        assertThat(testContractor.getContractorPhone()).isEqualTo(DEFAULT_CONTRACTOR_PHONE);
        assertThat(testContractor.getContractorAvatarUrl()).isEqualTo(DEFAULT_CONTRACTOR_AVATAR_URL);
    }

    @Test
    @Transactional
    public void createContractorWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contractorRepository.findAll().size();

        // Create the Contractor with an existing ID
        contractor.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContractorMockMvc.perform(post("/api/contractors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contractor)))
            .andExpect(status().isBadRequest());

        // Validate the Contractor in the database
        List<Contractor> contractorList = contractorRepository.findAll();
        assertThat(contractorList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkContractorNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = contractorRepository.findAll().size();
        // set the field null
        contractor.setContractorName(null);

        // Create the Contractor, which fails.

        restContractorMockMvc.perform(post("/api/contractors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contractor)))
            .andExpect(status().isBadRequest());

        List<Contractor> contractorList = contractorRepository.findAll();
        assertThat(contractorList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContractors() throws Exception {
        // Initialize the database
        contractorRepository.saveAndFlush(contractor);

        // Get all the contractorList
        restContractorMockMvc.perform(get("/api/contractors?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contractor.getId().intValue())))
            .andExpect(jsonPath("$.[*].contractorName").value(hasItem(DEFAULT_CONTRACTOR_NAME.toString())))
            .andExpect(jsonPath("$.[*].contractorTitle").value(hasItem(DEFAULT_CONTRACTOR_TITLE.toString())))
            .andExpect(jsonPath("$.[*].contractorDate").value(hasItem(DEFAULT_CONTRACTOR_DATE.toString())))
            .andExpect(jsonPath("$.[*].contractorDescription").value(hasItem(DEFAULT_CONTRACTOR_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].contractorAddress").value(hasItem(DEFAULT_CONTRACTOR_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].contractorWebsite").value(hasItem(DEFAULT_CONTRACTOR_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].contractorPhone").value(hasItem(DEFAULT_CONTRACTOR_PHONE.toString())))
            .andExpect(jsonPath("$.[*].contractorAvatarUrl").value(hasItem(DEFAULT_CONTRACTOR_AVATAR_URL.toString())));
    }
    

    @Test
    @Transactional
    public void getContractor() throws Exception {
        // Initialize the database
        contractorRepository.saveAndFlush(contractor);

        // Get the contractor
        restContractorMockMvc.perform(get("/api/contractors/{id}", contractor.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(contractor.getId().intValue()))
            .andExpect(jsonPath("$.contractorName").value(DEFAULT_CONTRACTOR_NAME.toString()))
            .andExpect(jsonPath("$.contractorTitle").value(DEFAULT_CONTRACTOR_TITLE.toString()))
            .andExpect(jsonPath("$.contractorDate").value(DEFAULT_CONTRACTOR_DATE.toString()))
            .andExpect(jsonPath("$.contractorDescription").value(DEFAULT_CONTRACTOR_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.contractorAddress").value(DEFAULT_CONTRACTOR_ADDRESS.toString()))
            .andExpect(jsonPath("$.contractorWebsite").value(DEFAULT_CONTRACTOR_WEBSITE.toString()))
            .andExpect(jsonPath("$.contractorPhone").value(DEFAULT_CONTRACTOR_PHONE.toString()))
            .andExpect(jsonPath("$.contractorAvatarUrl").value(DEFAULT_CONTRACTOR_AVATAR_URL.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingContractor() throws Exception {
        // Get the contractor
        restContractorMockMvc.perform(get("/api/contractors/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContractor() throws Exception {
        // Initialize the database
        contractorRepository.saveAndFlush(contractor);

        int databaseSizeBeforeUpdate = contractorRepository.findAll().size();

        // Update the contractor
        Contractor updatedContractor = contractorRepository.findById(contractor.getId()).get();
        // Disconnect from session so that the updates on updatedContractor are not directly saved in db
        em.detach(updatedContractor);
        updatedContractor
            .contractorName(UPDATED_CONTRACTOR_NAME)
            .contractorTitle(UPDATED_CONTRACTOR_TITLE)
            .contractorDate(UPDATED_CONTRACTOR_DATE)
            .contractorDescription(UPDATED_CONTRACTOR_DESCRIPTION)
            .contractorAddress(UPDATED_CONTRACTOR_ADDRESS)
            .contractorWebsite(UPDATED_CONTRACTOR_WEBSITE)
            .contractorPhone(UPDATED_CONTRACTOR_PHONE)
            .contractorAvatarUrl(UPDATED_CONTRACTOR_AVATAR_URL);

        restContractorMockMvc.perform(put("/api/contractors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedContractor)))
            .andExpect(status().isOk());

        // Validate the Contractor in the database
        List<Contractor> contractorList = contractorRepository.findAll();
        assertThat(contractorList).hasSize(databaseSizeBeforeUpdate);
        Contractor testContractor = contractorList.get(contractorList.size() - 1);
        assertThat(testContractor.getContractorName()).isEqualTo(UPDATED_CONTRACTOR_NAME);
        assertThat(testContractor.getContractorTitle()).isEqualTo(UPDATED_CONTRACTOR_TITLE);
        assertThat(testContractor.getContractorDate()).isEqualTo(UPDATED_CONTRACTOR_DATE);
        assertThat(testContractor.getContractorDescription()).isEqualTo(UPDATED_CONTRACTOR_DESCRIPTION);
        assertThat(testContractor.getContractorAddress()).isEqualTo(UPDATED_CONTRACTOR_ADDRESS);
        assertThat(testContractor.getContractorWebsite()).isEqualTo(UPDATED_CONTRACTOR_WEBSITE);
        assertThat(testContractor.getContractorPhone()).isEqualTo(UPDATED_CONTRACTOR_PHONE);
        assertThat(testContractor.getContractorAvatarUrl()).isEqualTo(UPDATED_CONTRACTOR_AVATAR_URL);
    }

    @Test
    @Transactional
    public void updateNonExistingContractor() throws Exception {
        int databaseSizeBeforeUpdate = contractorRepository.findAll().size();

        // Create the Contractor

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restContractorMockMvc.perform(put("/api/contractors")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contractor)))
            .andExpect(status().isBadRequest());

        // Validate the Contractor in the database
        List<Contractor> contractorList = contractorRepository.findAll();
        assertThat(contractorList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContractor() throws Exception {
        // Initialize the database
        contractorRepository.saveAndFlush(contractor);

        int databaseSizeBeforeDelete = contractorRepository.findAll().size();

        // Get the contractor
        restContractorMockMvc.perform(delete("/api/contractors/{id}", contractor.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Contractor> contractorList = contractorRepository.findAll();
        assertThat(contractorList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Contractor.class);
        Contractor contractor1 = new Contractor();
        contractor1.setId(1L);
        Contractor contractor2 = new Contractor();
        contractor2.setId(contractor1.getId());
        assertThat(contractor1).isEqualTo(contractor2);
        contractor2.setId(2L);
        assertThat(contractor1).isNotEqualTo(contractor2);
        contractor1.setId(null);
        assertThat(contractor1).isNotEqualTo(contractor2);
    }
}
