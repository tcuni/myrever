package com.tcutma.realstate.web.rest;

import com.tcutma.realstate.RiverApp;

import com.tcutma.realstate.domain.Location;
import com.tcutma.realstate.repository.LocationRepository;
import com.tcutma.realstate.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.tcutma.realstate.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the LocationResource REST controller.
 *
 * @see LocationResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RiverApp.class)
public class LocationResourceIntTest {

    private static final String DEFAULT_LOCATION_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION_FULL_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION_FULL_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION_NUMBER = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION_NUMBER = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION_ROAD = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION_ROAD = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION_WARD = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION_WARD = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION_DISTRICT = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION_DISTRICT = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION_PROVINCE = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION_PROVINCE = "BBBBBBBBBB";

    private static final String DEFAULT_LOCATION_GMAP_URL = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION_GMAP_URL = "BBBBBBBBBB";

    private static final Double DEFAULT_LONGITUDE = 1D;
    private static final Double UPDATED_LONGITUDE = 2D;

    private static final Double DEFAULT_LATITUDE = 1D;
    private static final Double UPDATED_LATITUDE = 2D;

    private static final String DEFAULT_LOCATION_ZIP_CODE = "AAAAAAAAAA";
    private static final String UPDATED_LOCATION_ZIP_CODE = "BBBBBBBBBB";

    @Autowired
    private LocationRepository locationRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restLocationMockMvc;

    private Location location;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final LocationResource locationResource = new LocationResource(locationRepository);
        this.restLocationMockMvc = MockMvcBuilders.standaloneSetup(locationResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Location createEntity(EntityManager em) {
        Location location = new Location()
            .locationName(DEFAULT_LOCATION_NAME)
            .locationFullAddress(DEFAULT_LOCATION_FULL_ADDRESS)
            .locationNumber(DEFAULT_LOCATION_NUMBER)
            .locationRoad(DEFAULT_LOCATION_ROAD)
            .locationWard(DEFAULT_LOCATION_WARD)
            .locationDistrict(DEFAULT_LOCATION_DISTRICT)
            .locationProvince(DEFAULT_LOCATION_PROVINCE)
            .locationGmapUrl(DEFAULT_LOCATION_GMAP_URL)
            .longitude(DEFAULT_LONGITUDE)
            .latitude(DEFAULT_LATITUDE)
            .locationZipCode(DEFAULT_LOCATION_ZIP_CODE);
        return location;
    }

    @Before
    public void initTest() {
        location = createEntity(em);
    }

    @Test
    @Transactional
    public void createLocation() throws Exception {
        int databaseSizeBeforeCreate = locationRepository.findAll().size();

        // Create the Location
        restLocationMockMvc.perform(post("/api/locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(location)))
            .andExpect(status().isCreated());

        // Validate the Location in the database
        List<Location> locationList = locationRepository.findAll();
        assertThat(locationList).hasSize(databaseSizeBeforeCreate + 1);
        Location testLocation = locationList.get(locationList.size() - 1);
        assertThat(testLocation.getLocationName()).isEqualTo(DEFAULT_LOCATION_NAME);
        assertThat(testLocation.getLocationFullAddress()).isEqualTo(DEFAULT_LOCATION_FULL_ADDRESS);
        assertThat(testLocation.getLocationNumber()).isEqualTo(DEFAULT_LOCATION_NUMBER);
        assertThat(testLocation.getLocationRoad()).isEqualTo(DEFAULT_LOCATION_ROAD);
        assertThat(testLocation.getLocationWard()).isEqualTo(DEFAULT_LOCATION_WARD);
        assertThat(testLocation.getLocationDistrict()).isEqualTo(DEFAULT_LOCATION_DISTRICT);
        assertThat(testLocation.getLocationProvince()).isEqualTo(DEFAULT_LOCATION_PROVINCE);
        assertThat(testLocation.getLocationGmapUrl()).isEqualTo(DEFAULT_LOCATION_GMAP_URL);
        assertThat(testLocation.getLongitude()).isEqualTo(DEFAULT_LONGITUDE);
        assertThat(testLocation.getLatitude()).isEqualTo(DEFAULT_LATITUDE);
        assertThat(testLocation.getLocationZipCode()).isEqualTo(DEFAULT_LOCATION_ZIP_CODE);
    }

    @Test
    @Transactional
    public void createLocationWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = locationRepository.findAll().size();

        // Create the Location with an existing ID
        location.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restLocationMockMvc.perform(post("/api/locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(location)))
            .andExpect(status().isBadRequest());

        // Validate the Location in the database
        List<Location> locationList = locationRepository.findAll();
        assertThat(locationList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkLongitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = locationRepository.findAll().size();
        // set the field null
        location.setLongitude(null);

        // Create the Location, which fails.

        restLocationMockMvc.perform(post("/api/locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(location)))
            .andExpect(status().isBadRequest());

        List<Location> locationList = locationRepository.findAll();
        assertThat(locationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkLatitudeIsRequired() throws Exception {
        int databaseSizeBeforeTest = locationRepository.findAll().size();
        // set the field null
        location.setLatitude(null);

        // Create the Location, which fails.

        restLocationMockMvc.perform(post("/api/locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(location)))
            .andExpect(status().isBadRequest());

        List<Location> locationList = locationRepository.findAll();
        assertThat(locationList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllLocations() throws Exception {
        // Initialize the database
        locationRepository.saveAndFlush(location);

        // Get all the locationList
        restLocationMockMvc.perform(get("/api/locations?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(location.getId().intValue())))
            .andExpect(jsonPath("$.[*].locationName").value(hasItem(DEFAULT_LOCATION_NAME.toString())))
            .andExpect(jsonPath("$.[*].locationFullAddress").value(hasItem(DEFAULT_LOCATION_FULL_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].locationNumber").value(hasItem(DEFAULT_LOCATION_NUMBER.toString())))
            .andExpect(jsonPath("$.[*].locationRoad").value(hasItem(DEFAULT_LOCATION_ROAD.toString())))
            .andExpect(jsonPath("$.[*].locationWard").value(hasItem(DEFAULT_LOCATION_WARD.toString())))
            .andExpect(jsonPath("$.[*].locationDistrict").value(hasItem(DEFAULT_LOCATION_DISTRICT.toString())))
            .andExpect(jsonPath("$.[*].locationProvince").value(hasItem(DEFAULT_LOCATION_PROVINCE.toString())))
            .andExpect(jsonPath("$.[*].locationGmapUrl").value(hasItem(DEFAULT_LOCATION_GMAP_URL.toString())))
            .andExpect(jsonPath("$.[*].longitude").value(hasItem(DEFAULT_LONGITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].latitude").value(hasItem(DEFAULT_LATITUDE.doubleValue())))
            .andExpect(jsonPath("$.[*].locationZipCode").value(hasItem(DEFAULT_LOCATION_ZIP_CODE.toString())));
    }
    

    @Test
    @Transactional
    public void getLocation() throws Exception {
        // Initialize the database
        locationRepository.saveAndFlush(location);

        // Get the location
        restLocationMockMvc.perform(get("/api/locations/{id}", location.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(location.getId().intValue()))
            .andExpect(jsonPath("$.locationName").value(DEFAULT_LOCATION_NAME.toString()))
            .andExpect(jsonPath("$.locationFullAddress").value(DEFAULT_LOCATION_FULL_ADDRESS.toString()))
            .andExpect(jsonPath("$.locationNumber").value(DEFAULT_LOCATION_NUMBER.toString()))
            .andExpect(jsonPath("$.locationRoad").value(DEFAULT_LOCATION_ROAD.toString()))
            .andExpect(jsonPath("$.locationWard").value(DEFAULT_LOCATION_WARD.toString()))
            .andExpect(jsonPath("$.locationDistrict").value(DEFAULT_LOCATION_DISTRICT.toString()))
            .andExpect(jsonPath("$.locationProvince").value(DEFAULT_LOCATION_PROVINCE.toString()))
            .andExpect(jsonPath("$.locationGmapUrl").value(DEFAULT_LOCATION_GMAP_URL.toString()))
            .andExpect(jsonPath("$.longitude").value(DEFAULT_LONGITUDE.doubleValue()))
            .andExpect(jsonPath("$.latitude").value(DEFAULT_LATITUDE.doubleValue()))
            .andExpect(jsonPath("$.locationZipCode").value(DEFAULT_LOCATION_ZIP_CODE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingLocation() throws Exception {
        // Get the location
        restLocationMockMvc.perform(get("/api/locations/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateLocation() throws Exception {
        // Initialize the database
        locationRepository.saveAndFlush(location);

        int databaseSizeBeforeUpdate = locationRepository.findAll().size();

        // Update the location
        Location updatedLocation = locationRepository.findById(location.getId()).get();
        // Disconnect from session so that the updates on updatedLocation are not directly saved in db
        em.detach(updatedLocation);
        updatedLocation
            .locationName(UPDATED_LOCATION_NAME)
            .locationFullAddress(UPDATED_LOCATION_FULL_ADDRESS)
            .locationNumber(UPDATED_LOCATION_NUMBER)
            .locationRoad(UPDATED_LOCATION_ROAD)
            .locationWard(UPDATED_LOCATION_WARD)
            .locationDistrict(UPDATED_LOCATION_DISTRICT)
            .locationProvince(UPDATED_LOCATION_PROVINCE)
            .locationGmapUrl(UPDATED_LOCATION_GMAP_URL)
            .longitude(UPDATED_LONGITUDE)
            .latitude(UPDATED_LATITUDE)
            .locationZipCode(UPDATED_LOCATION_ZIP_CODE);

        restLocationMockMvc.perform(put("/api/locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedLocation)))
            .andExpect(status().isOk());

        // Validate the Location in the database
        List<Location> locationList = locationRepository.findAll();
        assertThat(locationList).hasSize(databaseSizeBeforeUpdate);
        Location testLocation = locationList.get(locationList.size() - 1);
        assertThat(testLocation.getLocationName()).isEqualTo(UPDATED_LOCATION_NAME);
        assertThat(testLocation.getLocationFullAddress()).isEqualTo(UPDATED_LOCATION_FULL_ADDRESS);
        assertThat(testLocation.getLocationNumber()).isEqualTo(UPDATED_LOCATION_NUMBER);
        assertThat(testLocation.getLocationRoad()).isEqualTo(UPDATED_LOCATION_ROAD);
        assertThat(testLocation.getLocationWard()).isEqualTo(UPDATED_LOCATION_WARD);
        assertThat(testLocation.getLocationDistrict()).isEqualTo(UPDATED_LOCATION_DISTRICT);
        assertThat(testLocation.getLocationProvince()).isEqualTo(UPDATED_LOCATION_PROVINCE);
        assertThat(testLocation.getLocationGmapUrl()).isEqualTo(UPDATED_LOCATION_GMAP_URL);
        assertThat(testLocation.getLongitude()).isEqualTo(UPDATED_LONGITUDE);
        assertThat(testLocation.getLatitude()).isEqualTo(UPDATED_LATITUDE);
        assertThat(testLocation.getLocationZipCode()).isEqualTo(UPDATED_LOCATION_ZIP_CODE);
    }

    @Test
    @Transactional
    public void updateNonExistingLocation() throws Exception {
        int databaseSizeBeforeUpdate = locationRepository.findAll().size();

        // Create the Location

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restLocationMockMvc.perform(put("/api/locations")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(location)))
            .andExpect(status().isBadRequest());

        // Validate the Location in the database
        List<Location> locationList = locationRepository.findAll();
        assertThat(locationList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteLocation() throws Exception {
        // Initialize the database
        locationRepository.saveAndFlush(location);

        int databaseSizeBeforeDelete = locationRepository.findAll().size();

        // Get the location
        restLocationMockMvc.perform(delete("/api/locations/{id}", location.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Location> locationList = locationRepository.findAll();
        assertThat(locationList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Location.class);
        Location location1 = new Location();
        location1.setId(1L);
        Location location2 = new Location();
        location2.setId(location1.getId());
        assertThat(location1).isEqualTo(location2);
        location2.setId(2L);
        assertThat(location1).isNotEqualTo(location2);
        location1.setId(null);
        assertThat(location1).isNotEqualTo(location2);
    }
}
