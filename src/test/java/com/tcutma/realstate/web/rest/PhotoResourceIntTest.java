package com.tcutma.realstate.web.rest;

import com.tcutma.realstate.RiverApp;

import com.tcutma.realstate.domain.Photo;
import com.tcutma.realstate.repository.PhotoRepository;
import com.tcutma.realstate.service.PhotoService;
import com.tcutma.realstate.web.rest.errors.ExceptionTranslator;
import com.tcutma.realstate.service.dto.PhotoCriteria;
import com.tcutma.realstate.service.PhotoQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.tcutma.realstate.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.tcutma.realstate.domain.enumeration.ResourceType;
/**
 * Test class for the PhotoResource REST controller.
 *
 * @see PhotoResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RiverApp.class)
public class PhotoResourceIntTest {

    private static final String DEFAULT_SRC = "AAAAAAAAAA";
    private static final String UPDATED_SRC = "BBBBBBBBBB";

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_THUMBNAIL = "AAAAAAAAAA";
    private static final String UPDATED_THUMBNAIL = "BBBBBBBBBB";

    private static final Integer DEFAULT_THUMBNAIL_WIDTH = 1;
    private static final Integer UPDATED_THUMBNAIL_WIDTH = 2;

    private static final Integer DEFAULT_THUMBNAIL_HEIGHT = 1;
    private static final Integer UPDATED_THUMBNAIL_HEIGHT = 2;

    private static final String DEFAULT_THUMBNAIL_CAPTION = "AAAAAAAAAA";
    private static final String UPDATED_THUMBNAIL_CAPTION = "BBBBBBBBBB";

    private static final Integer DEFAULT_ORIENTATION = 1;
    private static final Integer UPDATED_ORIENTATION = 2;

    private static final String DEFAULT_CAPTION = "AAAAAAAAAA";
    private static final String UPDATED_CAPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TAGS = "AAAAAAAAAA";
    private static final String UPDATED_TAGS = "BBBBBBBBBB";

    private static final String DEFAULT_ALT = "AAAAAAAAAA";
    private static final String UPDATED_ALT = "BBBBBBBBBB";

    private static final Long DEFAULT_RESOURCE_ID = 1L;
    private static final Long UPDATED_RESOURCE_ID = 2L;

    private static final ResourceType DEFAULT_RESOURCE_TYPE = ResourceType.PROJECT;
    private static final ResourceType UPDATED_RESOURCE_TYPE = ResourceType.PROPERTY;

    private static final Long DEFAULT_FILE_SIZE = 1L;
    private static final Long UPDATED_FILE_SIZE = 2L;

    private static final Integer DEFAULT_RESOURCE_ORDER = 1;
    private static final Integer UPDATED_RESOURCE_ORDER = 2;

    private static final String DEFAULT_MIME_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_MIME_TYPE = "BBBBBBBBBB";

    @Autowired
    private PhotoRepository photoRepository;

    

    @Autowired
    private PhotoService photoService;

    @Autowired
    private PhotoQueryService photoQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restPhotoMockMvc;

    private Photo photo;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final PhotoResource photoResource = new PhotoResource(photoService, photoQueryService);
        this.restPhotoMockMvc = MockMvcBuilders.standaloneSetup(photoResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Photo createEntity(EntityManager em) {
        Photo photo = new Photo()
            .src(DEFAULT_SRC)
            .name(DEFAULT_NAME)
            .thumbnail(DEFAULT_THUMBNAIL)
            .thumbnailWidth(DEFAULT_THUMBNAIL_WIDTH)
            .thumbnailHeight(DEFAULT_THUMBNAIL_HEIGHT)
            .thumbnailCaption(DEFAULT_THUMBNAIL_CAPTION)
            .orientation(DEFAULT_ORIENTATION)
            .caption(DEFAULT_CAPTION)
            .tags(DEFAULT_TAGS)
            .alt(DEFAULT_ALT)
            .resourceId(DEFAULT_RESOURCE_ID)
            .resourceType(DEFAULT_RESOURCE_TYPE)
            .fileSize(DEFAULT_FILE_SIZE)
            .resourceOrder(DEFAULT_RESOURCE_ORDER)
            .mimeType(DEFAULT_MIME_TYPE);
        return photo;
    }

    @Before
    public void initTest() {
        photo = createEntity(em);
    }

    @Test
    @Transactional
    public void createPhoto() throws Exception {
        int databaseSizeBeforeCreate = photoRepository.findAll().size();

        // Create the Photo
        restPhotoMockMvc.perform(post("/api/photos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(photo)))
            .andExpect(status().isCreated());

        // Validate the Photo in the database
        List<Photo> photoList = photoRepository.findAll();
        assertThat(photoList).hasSize(databaseSizeBeforeCreate + 1);
        Photo testPhoto = photoList.get(photoList.size() - 1);
        assertThat(testPhoto.getSrc()).isEqualTo(DEFAULT_SRC);
        assertThat(testPhoto.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testPhoto.getThumbnail()).isEqualTo(DEFAULT_THUMBNAIL);
        assertThat(testPhoto.getThumbnailWidth()).isEqualTo(DEFAULT_THUMBNAIL_WIDTH);
        assertThat(testPhoto.getThumbnailHeight()).isEqualTo(DEFAULT_THUMBNAIL_HEIGHT);
        assertThat(testPhoto.getThumbnailCaption()).isEqualTo(DEFAULT_THUMBNAIL_CAPTION);
        assertThat(testPhoto.getOrientation()).isEqualTo(DEFAULT_ORIENTATION);
        assertThat(testPhoto.getCaption()).isEqualTo(DEFAULT_CAPTION);
        assertThat(testPhoto.getTags()).isEqualTo(DEFAULT_TAGS);
        assertThat(testPhoto.getAlt()).isEqualTo(DEFAULT_ALT);
        assertThat(testPhoto.getResourceId()).isEqualTo(DEFAULT_RESOURCE_ID);
        assertThat(testPhoto.getResourceType()).isEqualTo(DEFAULT_RESOURCE_TYPE);
        assertThat(testPhoto.getFileSize()).isEqualTo(DEFAULT_FILE_SIZE);
        assertThat(testPhoto.getResourceOrder()).isEqualTo(DEFAULT_RESOURCE_ORDER);
        assertThat(testPhoto.getMimeType()).isEqualTo(DEFAULT_MIME_TYPE);
    }

    @Test
    @Transactional
    public void createPhotoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = photoRepository.findAll().size();

        // Create the Photo with an existing ID
        photo.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPhotoMockMvc.perform(post("/api/photos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(photo)))
            .andExpect(status().isBadRequest());

        // Validate the Photo in the database
        List<Photo> photoList = photoRepository.findAll();
        assertThat(photoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllPhotos() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList
        restPhotoMockMvc.perform(get("/api/photos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(photo.getId().intValue())))
            .andExpect(jsonPath("$.[*].src").value(hasItem(DEFAULT_SRC.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].thumbnail").value(hasItem(DEFAULT_THUMBNAIL.toString())))
            .andExpect(jsonPath("$.[*].thumbnailWidth").value(hasItem(DEFAULT_THUMBNAIL_WIDTH)))
            .andExpect(jsonPath("$.[*].thumbnailHeight").value(hasItem(DEFAULT_THUMBNAIL_HEIGHT)))
            .andExpect(jsonPath("$.[*].thumbnailCaption").value(hasItem(DEFAULT_THUMBNAIL_CAPTION.toString())))
            .andExpect(jsonPath("$.[*].orientation").value(hasItem(DEFAULT_ORIENTATION)))
            .andExpect(jsonPath("$.[*].caption").value(hasItem(DEFAULT_CAPTION.toString())))
            .andExpect(jsonPath("$.[*].tags").value(hasItem(DEFAULT_TAGS.toString())))
            .andExpect(jsonPath("$.[*].alt").value(hasItem(DEFAULT_ALT.toString())))
            .andExpect(jsonPath("$.[*].resourceId").value(hasItem(DEFAULT_RESOURCE_ID.intValue())))
            .andExpect(jsonPath("$.[*].resourceType").value(hasItem(DEFAULT_RESOURCE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].fileSize").value(hasItem(DEFAULT_FILE_SIZE.intValue())))
            .andExpect(jsonPath("$.[*].resourceOrder").value(hasItem(DEFAULT_RESOURCE_ORDER)))
            .andExpect(jsonPath("$.[*].mimeType").value(hasItem(DEFAULT_MIME_TYPE.toString())));
    }
    

    @Test
    @Transactional
    public void getPhoto() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get the photo
        restPhotoMockMvc.perform(get("/api/photos/{id}", photo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(photo.getId().intValue()))
            .andExpect(jsonPath("$.src").value(DEFAULT_SRC.toString()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.thumbnail").value(DEFAULT_THUMBNAIL.toString()))
            .andExpect(jsonPath("$.thumbnailWidth").value(DEFAULT_THUMBNAIL_WIDTH))
            .andExpect(jsonPath("$.thumbnailHeight").value(DEFAULT_THUMBNAIL_HEIGHT))
            .andExpect(jsonPath("$.thumbnailCaption").value(DEFAULT_THUMBNAIL_CAPTION.toString()))
            .andExpect(jsonPath("$.orientation").value(DEFAULT_ORIENTATION))
            .andExpect(jsonPath("$.caption").value(DEFAULT_CAPTION.toString()))
            .andExpect(jsonPath("$.tags").value(DEFAULT_TAGS.toString()))
            .andExpect(jsonPath("$.alt").value(DEFAULT_ALT.toString()))
            .andExpect(jsonPath("$.resourceId").value(DEFAULT_RESOURCE_ID.intValue()))
            .andExpect(jsonPath("$.resourceType").value(DEFAULT_RESOURCE_TYPE.toString()))
            .andExpect(jsonPath("$.fileSize").value(DEFAULT_FILE_SIZE.intValue()))
            .andExpect(jsonPath("$.resourceOrder").value(DEFAULT_RESOURCE_ORDER))
            .andExpect(jsonPath("$.mimeType").value(DEFAULT_MIME_TYPE.toString()));
    }

    @Test
    @Transactional
    public void getAllPhotosBySrcIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where src equals to DEFAULT_SRC
        defaultPhotoShouldBeFound("src.equals=" + DEFAULT_SRC);

        // Get all the photoList where src equals to UPDATED_SRC
        defaultPhotoShouldNotBeFound("src.equals=" + UPDATED_SRC);
    }

    @Test
    @Transactional
    public void getAllPhotosBySrcIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where src in DEFAULT_SRC or UPDATED_SRC
        defaultPhotoShouldBeFound("src.in=" + DEFAULT_SRC + "," + UPDATED_SRC);

        // Get all the photoList where src equals to UPDATED_SRC
        defaultPhotoShouldNotBeFound("src.in=" + UPDATED_SRC);
    }

    @Test
    @Transactional
    public void getAllPhotosBySrcIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where src is not null
        defaultPhotoShouldBeFound("src.specified=true");

        // Get all the photoList where src is null
        defaultPhotoShouldNotBeFound("src.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where name equals to DEFAULT_NAME
        defaultPhotoShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the photoList where name equals to UPDATED_NAME
        defaultPhotoShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPhotosByNameIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where name in DEFAULT_NAME or UPDATED_NAME
        defaultPhotoShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the photoList where name equals to UPDATED_NAME
        defaultPhotoShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllPhotosByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where name is not null
        defaultPhotoShouldBeFound("name.specified=true");

        // Get all the photoList where name is null
        defaultPhotoShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnail equals to DEFAULT_THUMBNAIL
        defaultPhotoShouldBeFound("thumbnail.equals=" + DEFAULT_THUMBNAIL);

        // Get all the photoList where thumbnail equals to UPDATED_THUMBNAIL
        defaultPhotoShouldNotBeFound("thumbnail.equals=" + UPDATED_THUMBNAIL);
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnail in DEFAULT_THUMBNAIL or UPDATED_THUMBNAIL
        defaultPhotoShouldBeFound("thumbnail.in=" + DEFAULT_THUMBNAIL + "," + UPDATED_THUMBNAIL);

        // Get all the photoList where thumbnail equals to UPDATED_THUMBNAIL
        defaultPhotoShouldNotBeFound("thumbnail.in=" + UPDATED_THUMBNAIL);
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnail is not null
        defaultPhotoShouldBeFound("thumbnail.specified=true");

        // Get all the photoList where thumbnail is null
        defaultPhotoShouldNotBeFound("thumbnail.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailWidthIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailWidth equals to DEFAULT_THUMBNAIL_WIDTH
        defaultPhotoShouldBeFound("thumbnailWidth.equals=" + DEFAULT_THUMBNAIL_WIDTH);

        // Get all the photoList where thumbnailWidth equals to UPDATED_THUMBNAIL_WIDTH
        defaultPhotoShouldNotBeFound("thumbnailWidth.equals=" + UPDATED_THUMBNAIL_WIDTH);
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailWidthIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailWidth in DEFAULT_THUMBNAIL_WIDTH or UPDATED_THUMBNAIL_WIDTH
        defaultPhotoShouldBeFound("thumbnailWidth.in=" + DEFAULT_THUMBNAIL_WIDTH + "," + UPDATED_THUMBNAIL_WIDTH);

        // Get all the photoList where thumbnailWidth equals to UPDATED_THUMBNAIL_WIDTH
        defaultPhotoShouldNotBeFound("thumbnailWidth.in=" + UPDATED_THUMBNAIL_WIDTH);
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailWidthIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailWidth is not null
        defaultPhotoShouldBeFound("thumbnailWidth.specified=true");

        // Get all the photoList where thumbnailWidth is null
        defaultPhotoShouldNotBeFound("thumbnailWidth.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailWidthIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailWidth greater than or equals to DEFAULT_THUMBNAIL_WIDTH
        defaultPhotoShouldBeFound("thumbnailWidth.greaterOrEqualThan=" + DEFAULT_THUMBNAIL_WIDTH);

        // Get all the photoList where thumbnailWidth greater than or equals to UPDATED_THUMBNAIL_WIDTH
        defaultPhotoShouldNotBeFound("thumbnailWidth.greaterOrEqualThan=" + UPDATED_THUMBNAIL_WIDTH);
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailWidthIsLessThanSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailWidth less than or equals to DEFAULT_THUMBNAIL_WIDTH
        defaultPhotoShouldNotBeFound("thumbnailWidth.lessThan=" + DEFAULT_THUMBNAIL_WIDTH);

        // Get all the photoList where thumbnailWidth less than or equals to UPDATED_THUMBNAIL_WIDTH
        defaultPhotoShouldBeFound("thumbnailWidth.lessThan=" + UPDATED_THUMBNAIL_WIDTH);
    }


    @Test
    @Transactional
    public void getAllPhotosByThumbnailHeightIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailHeight equals to DEFAULT_THUMBNAIL_HEIGHT
        defaultPhotoShouldBeFound("thumbnailHeight.equals=" + DEFAULT_THUMBNAIL_HEIGHT);

        // Get all the photoList where thumbnailHeight equals to UPDATED_THUMBNAIL_HEIGHT
        defaultPhotoShouldNotBeFound("thumbnailHeight.equals=" + UPDATED_THUMBNAIL_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailHeightIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailHeight in DEFAULT_THUMBNAIL_HEIGHT or UPDATED_THUMBNAIL_HEIGHT
        defaultPhotoShouldBeFound("thumbnailHeight.in=" + DEFAULT_THUMBNAIL_HEIGHT + "," + UPDATED_THUMBNAIL_HEIGHT);

        // Get all the photoList where thumbnailHeight equals to UPDATED_THUMBNAIL_HEIGHT
        defaultPhotoShouldNotBeFound("thumbnailHeight.in=" + UPDATED_THUMBNAIL_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailHeightIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailHeight is not null
        defaultPhotoShouldBeFound("thumbnailHeight.specified=true");

        // Get all the photoList where thumbnailHeight is null
        defaultPhotoShouldNotBeFound("thumbnailHeight.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailHeightIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailHeight greater than or equals to DEFAULT_THUMBNAIL_HEIGHT
        defaultPhotoShouldBeFound("thumbnailHeight.greaterOrEqualThan=" + DEFAULT_THUMBNAIL_HEIGHT);

        // Get all the photoList where thumbnailHeight greater than or equals to UPDATED_THUMBNAIL_HEIGHT
        defaultPhotoShouldNotBeFound("thumbnailHeight.greaterOrEqualThan=" + UPDATED_THUMBNAIL_HEIGHT);
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailHeightIsLessThanSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailHeight less than or equals to DEFAULT_THUMBNAIL_HEIGHT
        defaultPhotoShouldNotBeFound("thumbnailHeight.lessThan=" + DEFAULT_THUMBNAIL_HEIGHT);

        // Get all the photoList where thumbnailHeight less than or equals to UPDATED_THUMBNAIL_HEIGHT
        defaultPhotoShouldBeFound("thumbnailHeight.lessThan=" + UPDATED_THUMBNAIL_HEIGHT);
    }


    @Test
    @Transactional
    public void getAllPhotosByThumbnailCaptionIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailCaption equals to DEFAULT_THUMBNAIL_CAPTION
        defaultPhotoShouldBeFound("thumbnailCaption.equals=" + DEFAULT_THUMBNAIL_CAPTION);

        // Get all the photoList where thumbnailCaption equals to UPDATED_THUMBNAIL_CAPTION
        defaultPhotoShouldNotBeFound("thumbnailCaption.equals=" + UPDATED_THUMBNAIL_CAPTION);
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailCaptionIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailCaption in DEFAULT_THUMBNAIL_CAPTION or UPDATED_THUMBNAIL_CAPTION
        defaultPhotoShouldBeFound("thumbnailCaption.in=" + DEFAULT_THUMBNAIL_CAPTION + "," + UPDATED_THUMBNAIL_CAPTION);

        // Get all the photoList where thumbnailCaption equals to UPDATED_THUMBNAIL_CAPTION
        defaultPhotoShouldNotBeFound("thumbnailCaption.in=" + UPDATED_THUMBNAIL_CAPTION);
    }

    @Test
    @Transactional
    public void getAllPhotosByThumbnailCaptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where thumbnailCaption is not null
        defaultPhotoShouldBeFound("thumbnailCaption.specified=true");

        // Get all the photoList where thumbnailCaption is null
        defaultPhotoShouldNotBeFound("thumbnailCaption.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByOrientationIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where orientation equals to DEFAULT_ORIENTATION
        defaultPhotoShouldBeFound("orientation.equals=" + DEFAULT_ORIENTATION);

        // Get all the photoList where orientation equals to UPDATED_ORIENTATION
        defaultPhotoShouldNotBeFound("orientation.equals=" + UPDATED_ORIENTATION);
    }

    @Test
    @Transactional
    public void getAllPhotosByOrientationIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where orientation in DEFAULT_ORIENTATION or UPDATED_ORIENTATION
        defaultPhotoShouldBeFound("orientation.in=" + DEFAULT_ORIENTATION + "," + UPDATED_ORIENTATION);

        // Get all the photoList where orientation equals to UPDATED_ORIENTATION
        defaultPhotoShouldNotBeFound("orientation.in=" + UPDATED_ORIENTATION);
    }

    @Test
    @Transactional
    public void getAllPhotosByOrientationIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where orientation is not null
        defaultPhotoShouldBeFound("orientation.specified=true");

        // Get all the photoList where orientation is null
        defaultPhotoShouldNotBeFound("orientation.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByOrientationIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where orientation greater than or equals to DEFAULT_ORIENTATION
        defaultPhotoShouldBeFound("orientation.greaterOrEqualThan=" + DEFAULT_ORIENTATION);

        // Get all the photoList where orientation greater than or equals to UPDATED_ORIENTATION
        defaultPhotoShouldNotBeFound("orientation.greaterOrEqualThan=" + UPDATED_ORIENTATION);
    }

    @Test
    @Transactional
    public void getAllPhotosByOrientationIsLessThanSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where orientation less than or equals to DEFAULT_ORIENTATION
        defaultPhotoShouldNotBeFound("orientation.lessThan=" + DEFAULT_ORIENTATION);

        // Get all the photoList where orientation less than or equals to UPDATED_ORIENTATION
        defaultPhotoShouldBeFound("orientation.lessThan=" + UPDATED_ORIENTATION);
    }


    @Test
    @Transactional
    public void getAllPhotosByCaptionIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where caption equals to DEFAULT_CAPTION
        defaultPhotoShouldBeFound("caption.equals=" + DEFAULT_CAPTION);

        // Get all the photoList where caption equals to UPDATED_CAPTION
        defaultPhotoShouldNotBeFound("caption.equals=" + UPDATED_CAPTION);
    }

    @Test
    @Transactional
    public void getAllPhotosByCaptionIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where caption in DEFAULT_CAPTION or UPDATED_CAPTION
        defaultPhotoShouldBeFound("caption.in=" + DEFAULT_CAPTION + "," + UPDATED_CAPTION);

        // Get all the photoList where caption equals to UPDATED_CAPTION
        defaultPhotoShouldNotBeFound("caption.in=" + UPDATED_CAPTION);
    }

    @Test
    @Transactional
    public void getAllPhotosByCaptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where caption is not null
        defaultPhotoShouldBeFound("caption.specified=true");

        // Get all the photoList where caption is null
        defaultPhotoShouldNotBeFound("caption.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByTagsIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where tags equals to DEFAULT_TAGS
        defaultPhotoShouldBeFound("tags.equals=" + DEFAULT_TAGS);

        // Get all the photoList where tags equals to UPDATED_TAGS
        defaultPhotoShouldNotBeFound("tags.equals=" + UPDATED_TAGS);
    }

    @Test
    @Transactional
    public void getAllPhotosByTagsIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where tags in DEFAULT_TAGS or UPDATED_TAGS
        defaultPhotoShouldBeFound("tags.in=" + DEFAULT_TAGS + "," + UPDATED_TAGS);

        // Get all the photoList where tags equals to UPDATED_TAGS
        defaultPhotoShouldNotBeFound("tags.in=" + UPDATED_TAGS);
    }

    @Test
    @Transactional
    public void getAllPhotosByTagsIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where tags is not null
        defaultPhotoShouldBeFound("tags.specified=true");

        // Get all the photoList where tags is null
        defaultPhotoShouldNotBeFound("tags.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByAltIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where alt equals to DEFAULT_ALT
        defaultPhotoShouldBeFound("alt.equals=" + DEFAULT_ALT);

        // Get all the photoList where alt equals to UPDATED_ALT
        defaultPhotoShouldNotBeFound("alt.equals=" + UPDATED_ALT);
    }

    @Test
    @Transactional
    public void getAllPhotosByAltIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where alt in DEFAULT_ALT or UPDATED_ALT
        defaultPhotoShouldBeFound("alt.in=" + DEFAULT_ALT + "," + UPDATED_ALT);

        // Get all the photoList where alt equals to UPDATED_ALT
        defaultPhotoShouldNotBeFound("alt.in=" + UPDATED_ALT);
    }

    @Test
    @Transactional
    public void getAllPhotosByAltIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where alt is not null
        defaultPhotoShouldBeFound("alt.specified=true");

        // Get all the photoList where alt is null
        defaultPhotoShouldNotBeFound("alt.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByResourceIdIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceId equals to DEFAULT_RESOURCE_ID
        defaultPhotoShouldBeFound("resourceId.equals=" + DEFAULT_RESOURCE_ID);

        // Get all the photoList where resourceId equals to UPDATED_RESOURCE_ID
        defaultPhotoShouldNotBeFound("resourceId.equals=" + UPDATED_RESOURCE_ID);
    }

    @Test
    @Transactional
    public void getAllPhotosByResourceIdIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceId in DEFAULT_RESOURCE_ID or UPDATED_RESOURCE_ID
        defaultPhotoShouldBeFound("resourceId.in=" + DEFAULT_RESOURCE_ID + "," + UPDATED_RESOURCE_ID);

        // Get all the photoList where resourceId equals to UPDATED_RESOURCE_ID
        defaultPhotoShouldNotBeFound("resourceId.in=" + UPDATED_RESOURCE_ID);
    }

    @Test
    @Transactional
    public void getAllPhotosByResourceIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceId is not null
        defaultPhotoShouldBeFound("resourceId.specified=true");

        // Get all the photoList where resourceId is null
        defaultPhotoShouldNotBeFound("resourceId.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByResourceIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceId greater than or equals to DEFAULT_RESOURCE_ID
        defaultPhotoShouldBeFound("resourceId.greaterOrEqualThan=" + DEFAULT_RESOURCE_ID);

        // Get all the photoList where resourceId greater than or equals to UPDATED_RESOURCE_ID
        defaultPhotoShouldNotBeFound("resourceId.greaterOrEqualThan=" + UPDATED_RESOURCE_ID);
    }

    @Test
    @Transactional
    public void getAllPhotosByResourceIdIsLessThanSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceId less than or equals to DEFAULT_RESOURCE_ID
        defaultPhotoShouldNotBeFound("resourceId.lessThan=" + DEFAULT_RESOURCE_ID);

        // Get all the photoList where resourceId less than or equals to UPDATED_RESOURCE_ID
        defaultPhotoShouldBeFound("resourceId.lessThan=" + UPDATED_RESOURCE_ID);
    }


    @Test
    @Transactional
    public void getAllPhotosByResourceTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceType equals to DEFAULT_RESOURCE_TYPE
        defaultPhotoShouldBeFound("resourceType.equals=" + DEFAULT_RESOURCE_TYPE);

        // Get all the photoList where resourceType equals to UPDATED_RESOURCE_TYPE
        defaultPhotoShouldNotBeFound("resourceType.equals=" + UPDATED_RESOURCE_TYPE);
    }

    @Test
    @Transactional
    public void getAllPhotosByResourceTypeIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceType in DEFAULT_RESOURCE_TYPE or UPDATED_RESOURCE_TYPE
        defaultPhotoShouldBeFound("resourceType.in=" + DEFAULT_RESOURCE_TYPE + "," + UPDATED_RESOURCE_TYPE);

        // Get all the photoList where resourceType equals to UPDATED_RESOURCE_TYPE
        defaultPhotoShouldNotBeFound("resourceType.in=" + UPDATED_RESOURCE_TYPE);
    }

    @Test
    @Transactional
    public void getAllPhotosByResourceTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceType is not null
        defaultPhotoShouldBeFound("resourceType.specified=true");

        // Get all the photoList where resourceType is null
        defaultPhotoShouldNotBeFound("resourceType.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByFileSizeIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where fileSize equals to DEFAULT_FILE_SIZE
        defaultPhotoShouldBeFound("fileSize.equals=" + DEFAULT_FILE_SIZE);

        // Get all the photoList where fileSize equals to UPDATED_FILE_SIZE
        defaultPhotoShouldNotBeFound("fileSize.equals=" + UPDATED_FILE_SIZE);
    }

    @Test
    @Transactional
    public void getAllPhotosByFileSizeIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where fileSize in DEFAULT_FILE_SIZE or UPDATED_FILE_SIZE
        defaultPhotoShouldBeFound("fileSize.in=" + DEFAULT_FILE_SIZE + "," + UPDATED_FILE_SIZE);

        // Get all the photoList where fileSize equals to UPDATED_FILE_SIZE
        defaultPhotoShouldNotBeFound("fileSize.in=" + UPDATED_FILE_SIZE);
    }

    @Test
    @Transactional
    public void getAllPhotosByFileSizeIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where fileSize is not null
        defaultPhotoShouldBeFound("fileSize.specified=true");

        // Get all the photoList where fileSize is null
        defaultPhotoShouldNotBeFound("fileSize.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByFileSizeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where fileSize greater than or equals to DEFAULT_FILE_SIZE
        defaultPhotoShouldBeFound("fileSize.greaterOrEqualThan=" + DEFAULT_FILE_SIZE);

        // Get all the photoList where fileSize greater than or equals to UPDATED_FILE_SIZE
        defaultPhotoShouldNotBeFound("fileSize.greaterOrEqualThan=" + UPDATED_FILE_SIZE);
    }

    @Test
    @Transactional
    public void getAllPhotosByFileSizeIsLessThanSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where fileSize less than or equals to DEFAULT_FILE_SIZE
        defaultPhotoShouldNotBeFound("fileSize.lessThan=" + DEFAULT_FILE_SIZE);

        // Get all the photoList where fileSize less than or equals to UPDATED_FILE_SIZE
        defaultPhotoShouldBeFound("fileSize.lessThan=" + UPDATED_FILE_SIZE);
    }


    @Test
    @Transactional
    public void getAllPhotosByResourceOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceOrder equals to DEFAULT_RESOURCE_ORDER
        defaultPhotoShouldBeFound("resourceOrder.equals=" + DEFAULT_RESOURCE_ORDER);

        // Get all the photoList where resourceOrder equals to UPDATED_RESOURCE_ORDER
        defaultPhotoShouldNotBeFound("resourceOrder.equals=" + UPDATED_RESOURCE_ORDER);
    }

    @Test
    @Transactional
    public void getAllPhotosByResourceOrderIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceOrder in DEFAULT_RESOURCE_ORDER or UPDATED_RESOURCE_ORDER
        defaultPhotoShouldBeFound("resourceOrder.in=" + DEFAULT_RESOURCE_ORDER + "," + UPDATED_RESOURCE_ORDER);

        // Get all the photoList where resourceOrder equals to UPDATED_RESOURCE_ORDER
        defaultPhotoShouldNotBeFound("resourceOrder.in=" + UPDATED_RESOURCE_ORDER);
    }

    @Test
    @Transactional
    public void getAllPhotosByResourceOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceOrder is not null
        defaultPhotoShouldBeFound("resourceOrder.specified=true");

        // Get all the photoList where resourceOrder is null
        defaultPhotoShouldNotBeFound("resourceOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllPhotosByResourceOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceOrder greater than or equals to DEFAULT_RESOURCE_ORDER
        defaultPhotoShouldBeFound("resourceOrder.greaterOrEqualThan=" + DEFAULT_RESOURCE_ORDER);

        // Get all the photoList where resourceOrder greater than or equals to UPDATED_RESOURCE_ORDER
        defaultPhotoShouldNotBeFound("resourceOrder.greaterOrEqualThan=" + UPDATED_RESOURCE_ORDER);
    }

    @Test
    @Transactional
    public void getAllPhotosByResourceOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where resourceOrder less than or equals to DEFAULT_RESOURCE_ORDER
        defaultPhotoShouldNotBeFound("resourceOrder.lessThan=" + DEFAULT_RESOURCE_ORDER);

        // Get all the photoList where resourceOrder less than or equals to UPDATED_RESOURCE_ORDER
        defaultPhotoShouldBeFound("resourceOrder.lessThan=" + UPDATED_RESOURCE_ORDER);
    }


    @Test
    @Transactional
    public void getAllPhotosByMimeTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where mimeType equals to DEFAULT_MIME_TYPE
        defaultPhotoShouldBeFound("mimeType.equals=" + DEFAULT_MIME_TYPE);

        // Get all the photoList where mimeType equals to UPDATED_MIME_TYPE
        defaultPhotoShouldNotBeFound("mimeType.equals=" + UPDATED_MIME_TYPE);
    }

    @Test
    @Transactional
    public void getAllPhotosByMimeTypeIsInShouldWork() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where mimeType in DEFAULT_MIME_TYPE or UPDATED_MIME_TYPE
        defaultPhotoShouldBeFound("mimeType.in=" + DEFAULT_MIME_TYPE + "," + UPDATED_MIME_TYPE);

        // Get all the photoList where mimeType equals to UPDATED_MIME_TYPE
        defaultPhotoShouldNotBeFound("mimeType.in=" + UPDATED_MIME_TYPE);
    }

    @Test
    @Transactional
    public void getAllPhotosByMimeTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        photoRepository.saveAndFlush(photo);

        // Get all the photoList where mimeType is not null
        defaultPhotoShouldBeFound("mimeType.specified=true");

        // Get all the photoList where mimeType is null
        defaultPhotoShouldNotBeFound("mimeType.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultPhotoShouldBeFound(String filter) throws Exception {
        restPhotoMockMvc.perform(get("/api/photos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(photo.getId().intValue())))
            .andExpect(jsonPath("$.[*].src").value(hasItem(DEFAULT_SRC.toString())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].thumbnail").value(hasItem(DEFAULT_THUMBNAIL.toString())))
            .andExpect(jsonPath("$.[*].thumbnailWidth").value(hasItem(DEFAULT_THUMBNAIL_WIDTH)))
            .andExpect(jsonPath("$.[*].thumbnailHeight").value(hasItem(DEFAULT_THUMBNAIL_HEIGHT)))
            .andExpect(jsonPath("$.[*].thumbnailCaption").value(hasItem(DEFAULT_THUMBNAIL_CAPTION.toString())))
            .andExpect(jsonPath("$.[*].orientation").value(hasItem(DEFAULT_ORIENTATION)))
            .andExpect(jsonPath("$.[*].caption").value(hasItem(DEFAULT_CAPTION.toString())))
            .andExpect(jsonPath("$.[*].tags").value(hasItem(DEFAULT_TAGS.toString())))
            .andExpect(jsonPath("$.[*].alt").value(hasItem(DEFAULT_ALT.toString())))
            .andExpect(jsonPath("$.[*].resourceId").value(hasItem(DEFAULT_RESOURCE_ID.intValue())))
            .andExpect(jsonPath("$.[*].resourceType").value(hasItem(DEFAULT_RESOURCE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].fileSize").value(hasItem(DEFAULT_FILE_SIZE.intValue())))
            .andExpect(jsonPath("$.[*].resourceOrder").value(hasItem(DEFAULT_RESOURCE_ORDER)))
            .andExpect(jsonPath("$.[*].mimeType").value(hasItem(DEFAULT_MIME_TYPE.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultPhotoShouldNotBeFound(String filter) throws Exception {
        restPhotoMockMvc.perform(get("/api/photos?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingPhoto() throws Exception {
        // Get the photo
        restPhotoMockMvc.perform(get("/api/photos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePhoto() throws Exception {
        // Initialize the database
        photoService.save(photo);

        int databaseSizeBeforeUpdate = photoRepository.findAll().size();

        // Update the photo
        Photo updatedPhoto = photoRepository.findById(photo.getId()).get();
        // Disconnect from session so that the updates on updatedPhoto are not directly saved in db
        em.detach(updatedPhoto);
        updatedPhoto
            .src(UPDATED_SRC)
            .name(UPDATED_NAME)
            .thumbnail(UPDATED_THUMBNAIL)
            .thumbnailWidth(UPDATED_THUMBNAIL_WIDTH)
            .thumbnailHeight(UPDATED_THUMBNAIL_HEIGHT)
            .thumbnailCaption(UPDATED_THUMBNAIL_CAPTION)
            .orientation(UPDATED_ORIENTATION)
            .caption(UPDATED_CAPTION)
            .tags(UPDATED_TAGS)
            .alt(UPDATED_ALT)
            .resourceId(UPDATED_RESOURCE_ID)
            .resourceType(UPDATED_RESOURCE_TYPE)
            .fileSize(UPDATED_FILE_SIZE)
            .resourceOrder(UPDATED_RESOURCE_ORDER)
            .mimeType(UPDATED_MIME_TYPE);

        restPhotoMockMvc.perform(put("/api/photos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedPhoto)))
            .andExpect(status().isOk());

        // Validate the Photo in the database
        List<Photo> photoList = photoRepository.findAll();
        assertThat(photoList).hasSize(databaseSizeBeforeUpdate);
        Photo testPhoto = photoList.get(photoList.size() - 1);
        assertThat(testPhoto.getSrc()).isEqualTo(UPDATED_SRC);
        assertThat(testPhoto.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testPhoto.getThumbnail()).isEqualTo(UPDATED_THUMBNAIL);
        assertThat(testPhoto.getThumbnailWidth()).isEqualTo(UPDATED_THUMBNAIL_WIDTH);
        assertThat(testPhoto.getThumbnailHeight()).isEqualTo(UPDATED_THUMBNAIL_HEIGHT);
        assertThat(testPhoto.getThumbnailCaption()).isEqualTo(UPDATED_THUMBNAIL_CAPTION);
        assertThat(testPhoto.getOrientation()).isEqualTo(UPDATED_ORIENTATION);
        assertThat(testPhoto.getCaption()).isEqualTo(UPDATED_CAPTION);
        assertThat(testPhoto.getTags()).isEqualTo(UPDATED_TAGS);
        assertThat(testPhoto.getAlt()).isEqualTo(UPDATED_ALT);
        assertThat(testPhoto.getResourceId()).isEqualTo(UPDATED_RESOURCE_ID);
        assertThat(testPhoto.getResourceType()).isEqualTo(UPDATED_RESOURCE_TYPE);
        assertThat(testPhoto.getFileSize()).isEqualTo(UPDATED_FILE_SIZE);
        assertThat(testPhoto.getResourceOrder()).isEqualTo(UPDATED_RESOURCE_ORDER);
        assertThat(testPhoto.getMimeType()).isEqualTo(UPDATED_MIME_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingPhoto() throws Exception {
        int databaseSizeBeforeUpdate = photoRepository.findAll().size();

        // Create the Photo

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restPhotoMockMvc.perform(put("/api/photos")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(photo)))
            .andExpect(status().isBadRequest());

        // Validate the Photo in the database
        List<Photo> photoList = photoRepository.findAll();
        assertThat(photoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePhoto() throws Exception {
        // Initialize the database
        photoService.save(photo);

        int databaseSizeBeforeDelete = photoRepository.findAll().size();

        // Get the photo
        restPhotoMockMvc.perform(delete("/api/photos/{id}", photo.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Photo> photoList = photoRepository.findAll();
        assertThat(photoList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Photo.class);
        Photo photo1 = new Photo();
        photo1.setId(1L);
        Photo photo2 = new Photo();
        photo2.setId(photo1.getId());
        assertThat(photo1).isEqualTo(photo2);
        photo2.setId(2L);
        assertThat(photo1).isNotEqualTo(photo2);
        photo1.setId(null);
        assertThat(photo1).isNotEqualTo(photo2);
    }
}
