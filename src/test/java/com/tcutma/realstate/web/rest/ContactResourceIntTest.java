package com.tcutma.realstate.web.rest;

import com.tcutma.realstate.RiverApp;

import com.tcutma.realstate.domain.Contact;
import com.tcutma.realstate.domain.User;
import com.tcutma.realstate.repository.ContactRepository;
import com.tcutma.realstate.service.ContactService;
import com.tcutma.realstate.web.rest.errors.ExceptionTranslator;
import com.tcutma.realstate.service.dto.ContactCriteria;
import com.tcutma.realstate.service.ContactQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.tcutma.realstate.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the ContactResource REST controller.
 *
 * @see ContactResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RiverApp.class)
public class ContactResourceIntTest {

    private static final String DEFAULT_CONTACT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_PHONE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_ADDRESS = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_WEBSITE = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_WEBSITE = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_AVATAR_URL = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_AVATAR_URL = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_FACEBOOK = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_FACEBOOK = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_TWITTER = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_TWITTER = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_INSTAGRAM = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_INSTAGRAM = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_LINKEDIN = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_LINKEDIN = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_GOOGLE_PLUS = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_GOOGLE_PLUS = "BBBBBBBBBB";

    private static final String DEFAULT_CONTACT_YOUTUBE = "AAAAAAAAAA";
    private static final String UPDATED_CONTACT_YOUTUBE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_CONTACT_STATUS = false;
    private static final Boolean UPDATED_CONTACT_STATUS = true;

    @Autowired
    private ContactRepository contactRepository;

    

    @Autowired
    private ContactService contactService;

    @Autowired
    private ContactQueryService contactQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restContactMockMvc;

    private Contact contact;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final ContactResource contactResource = new ContactResource(contactService, contactQueryService);
        this.restContactMockMvc = MockMvcBuilders.standaloneSetup(contactResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Contact createEntity(EntityManager em) {
        Contact contact = new Contact()
            .contactName(DEFAULT_CONTACT_NAME)
            .contactPhone(DEFAULT_CONTACT_PHONE)
            .contactAddress(DEFAULT_CONTACT_ADDRESS)
            .contactWebsite(DEFAULT_CONTACT_WEBSITE)
            .contactAvatarUrl(DEFAULT_CONTACT_AVATAR_URL)
            .contactFacebook(DEFAULT_CONTACT_FACEBOOK)
            .contactTwitter(DEFAULT_CONTACT_TWITTER)
            .contactInstagram(DEFAULT_CONTACT_INSTAGRAM)
            .contactLinkedin(DEFAULT_CONTACT_LINKEDIN)
            .contactGooglePlus(DEFAULT_CONTACT_GOOGLE_PLUS)
            .contactYoutube(DEFAULT_CONTACT_YOUTUBE)
            .contactStatus(DEFAULT_CONTACT_STATUS);
        return contact;
    }

    @Before
    public void initTest() {
        contact = createEntity(em);
    }

    @Test
    @Transactional
    public void createContact() throws Exception {
        int databaseSizeBeforeCreate = contactRepository.findAll().size();

        // Create the Contact
        restContactMockMvc.perform(post("/api/contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contact)))
            .andExpect(status().isCreated());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeCreate + 1);
        Contact testContact = contactList.get(contactList.size() - 1);
        assertThat(testContact.getContactName()).isEqualTo(DEFAULT_CONTACT_NAME);
        assertThat(testContact.getContactPhone()).isEqualTo(DEFAULT_CONTACT_PHONE);
        assertThat(testContact.getContactAddress()).isEqualTo(DEFAULT_CONTACT_ADDRESS);
        assertThat(testContact.getContactWebsite()).isEqualTo(DEFAULT_CONTACT_WEBSITE);
        assertThat(testContact.getContactAvatarUrl()).isEqualTo(DEFAULT_CONTACT_AVATAR_URL);
        assertThat(testContact.getContactFacebook()).isEqualTo(DEFAULT_CONTACT_FACEBOOK);
        assertThat(testContact.getContactTwitter()).isEqualTo(DEFAULT_CONTACT_TWITTER);
        assertThat(testContact.getContactInstagram()).isEqualTo(DEFAULT_CONTACT_INSTAGRAM);
        assertThat(testContact.getContactLinkedin()).isEqualTo(DEFAULT_CONTACT_LINKEDIN);
        assertThat(testContact.getContactGooglePlus()).isEqualTo(DEFAULT_CONTACT_GOOGLE_PLUS);
        assertThat(testContact.getContactYoutube()).isEqualTo(DEFAULT_CONTACT_YOUTUBE);
        assertThat(testContact.isContactStatus()).isEqualTo(DEFAULT_CONTACT_STATUS);
    }

    @Test
    @Transactional
    public void createContactWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = contactRepository.findAll().size();

        // Create the Contact with an existing ID
        contact.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restContactMockMvc.perform(post("/api/contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contact)))
            .andExpect(status().isBadRequest());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkContactNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactRepository.findAll().size();
        // set the field null
        contact.setContactName(null);

        // Create the Contact, which fails.

        restContactMockMvc.perform(post("/api/contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contact)))
            .andExpect(status().isBadRequest());

        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void checkContactPhoneIsRequired() throws Exception {
        int databaseSizeBeforeTest = contactRepository.findAll().size();
        // set the field null
        contact.setContactPhone(null);

        // Create the Contact, which fails.

        restContactMockMvc.perform(post("/api/contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contact)))
            .andExpect(status().isBadRequest());

        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllContacts() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList
        restContactMockMvc.perform(get("/api/contacts?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contact.getId().intValue())))
            .andExpect(jsonPath("$.[*].contactName").value(hasItem(DEFAULT_CONTACT_NAME.toString())))
            .andExpect(jsonPath("$.[*].contactPhone").value(hasItem(DEFAULT_CONTACT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].contactAddress").value(hasItem(DEFAULT_CONTACT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].contactWebsite").value(hasItem(DEFAULT_CONTACT_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].contactAvatarUrl").value(hasItem(DEFAULT_CONTACT_AVATAR_URL.toString())))
            .andExpect(jsonPath("$.[*].contactFacebook").value(hasItem(DEFAULT_CONTACT_FACEBOOK.toString())))
            .andExpect(jsonPath("$.[*].contactTwitter").value(hasItem(DEFAULT_CONTACT_TWITTER.toString())))
            .andExpect(jsonPath("$.[*].contactInstagram").value(hasItem(DEFAULT_CONTACT_INSTAGRAM.toString())))
            .andExpect(jsonPath("$.[*].contactLinkedin").value(hasItem(DEFAULT_CONTACT_LINKEDIN.toString())))
            .andExpect(jsonPath("$.[*].contactGooglePlus").value(hasItem(DEFAULT_CONTACT_GOOGLE_PLUS.toString())))
            .andExpect(jsonPath("$.[*].contactYoutube").value(hasItem(DEFAULT_CONTACT_YOUTUBE.toString())))
            .andExpect(jsonPath("$.[*].contactStatus").value(hasItem(DEFAULT_CONTACT_STATUS.booleanValue())));
    }
    

    @Test
    @Transactional
    public void getContact() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get the contact
        restContactMockMvc.perform(get("/api/contacts/{id}", contact.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(contact.getId().intValue()))
            .andExpect(jsonPath("$.contactName").value(DEFAULT_CONTACT_NAME.toString()))
            .andExpect(jsonPath("$.contactPhone").value(DEFAULT_CONTACT_PHONE.toString()))
            .andExpect(jsonPath("$.contactAddress").value(DEFAULT_CONTACT_ADDRESS.toString()))
            .andExpect(jsonPath("$.contactWebsite").value(DEFAULT_CONTACT_WEBSITE.toString()))
            .andExpect(jsonPath("$.contactAvatarUrl").value(DEFAULT_CONTACT_AVATAR_URL.toString()))
            .andExpect(jsonPath("$.contactFacebook").value(DEFAULT_CONTACT_FACEBOOK.toString()))
            .andExpect(jsonPath("$.contactTwitter").value(DEFAULT_CONTACT_TWITTER.toString()))
            .andExpect(jsonPath("$.contactInstagram").value(DEFAULT_CONTACT_INSTAGRAM.toString()))
            .andExpect(jsonPath("$.contactLinkedin").value(DEFAULT_CONTACT_LINKEDIN.toString()))
            .andExpect(jsonPath("$.contactGooglePlus").value(DEFAULT_CONTACT_GOOGLE_PLUS.toString()))
            .andExpect(jsonPath("$.contactYoutube").value(DEFAULT_CONTACT_YOUTUBE.toString()))
            .andExpect(jsonPath("$.contactStatus").value(DEFAULT_CONTACT_STATUS.booleanValue()));
    }

    @Test
    @Transactional
    public void getAllContactsByContactNameIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactName equals to DEFAULT_CONTACT_NAME
        defaultContactShouldBeFound("contactName.equals=" + DEFAULT_CONTACT_NAME);

        // Get all the contactList where contactName equals to UPDATED_CONTACT_NAME
        defaultContactShouldNotBeFound("contactName.equals=" + UPDATED_CONTACT_NAME);
    }

    @Test
    @Transactional
    public void getAllContactsByContactNameIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactName in DEFAULT_CONTACT_NAME or UPDATED_CONTACT_NAME
        defaultContactShouldBeFound("contactName.in=" + DEFAULT_CONTACT_NAME + "," + UPDATED_CONTACT_NAME);

        // Get all the contactList where contactName equals to UPDATED_CONTACT_NAME
        defaultContactShouldNotBeFound("contactName.in=" + UPDATED_CONTACT_NAME);
    }

    @Test
    @Transactional
    public void getAllContactsByContactNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactName is not null
        defaultContactShouldBeFound("contactName.specified=true");

        // Get all the contactList where contactName is null
        defaultContactShouldNotBeFound("contactName.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByContactPhoneIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactPhone equals to DEFAULT_CONTACT_PHONE
        defaultContactShouldBeFound("contactPhone.equals=" + DEFAULT_CONTACT_PHONE);

        // Get all the contactList where contactPhone equals to UPDATED_CONTACT_PHONE
        defaultContactShouldNotBeFound("contactPhone.equals=" + UPDATED_CONTACT_PHONE);
    }

    @Test
    @Transactional
    public void getAllContactsByContactPhoneIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactPhone in DEFAULT_CONTACT_PHONE or UPDATED_CONTACT_PHONE
        defaultContactShouldBeFound("contactPhone.in=" + DEFAULT_CONTACT_PHONE + "," + UPDATED_CONTACT_PHONE);

        // Get all the contactList where contactPhone equals to UPDATED_CONTACT_PHONE
        defaultContactShouldNotBeFound("contactPhone.in=" + UPDATED_CONTACT_PHONE);
    }

    @Test
    @Transactional
    public void getAllContactsByContactPhoneIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactPhone is not null
        defaultContactShouldBeFound("contactPhone.specified=true");

        // Get all the contactList where contactPhone is null
        defaultContactShouldNotBeFound("contactPhone.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByContactAddressIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactAddress equals to DEFAULT_CONTACT_ADDRESS
        defaultContactShouldBeFound("contactAddress.equals=" + DEFAULT_CONTACT_ADDRESS);

        // Get all the contactList where contactAddress equals to UPDATED_CONTACT_ADDRESS
        defaultContactShouldNotBeFound("contactAddress.equals=" + UPDATED_CONTACT_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllContactsByContactAddressIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactAddress in DEFAULT_CONTACT_ADDRESS or UPDATED_CONTACT_ADDRESS
        defaultContactShouldBeFound("contactAddress.in=" + DEFAULT_CONTACT_ADDRESS + "," + UPDATED_CONTACT_ADDRESS);

        // Get all the contactList where contactAddress equals to UPDATED_CONTACT_ADDRESS
        defaultContactShouldNotBeFound("contactAddress.in=" + UPDATED_CONTACT_ADDRESS);
    }

    @Test
    @Transactional
    public void getAllContactsByContactAddressIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactAddress is not null
        defaultContactShouldBeFound("contactAddress.specified=true");

        // Get all the contactList where contactAddress is null
        defaultContactShouldNotBeFound("contactAddress.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByContactWebsiteIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactWebsite equals to DEFAULT_CONTACT_WEBSITE
        defaultContactShouldBeFound("contactWebsite.equals=" + DEFAULT_CONTACT_WEBSITE);

        // Get all the contactList where contactWebsite equals to UPDATED_CONTACT_WEBSITE
        defaultContactShouldNotBeFound("contactWebsite.equals=" + UPDATED_CONTACT_WEBSITE);
    }

    @Test
    @Transactional
    public void getAllContactsByContactWebsiteIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactWebsite in DEFAULT_CONTACT_WEBSITE or UPDATED_CONTACT_WEBSITE
        defaultContactShouldBeFound("contactWebsite.in=" + DEFAULT_CONTACT_WEBSITE + "," + UPDATED_CONTACT_WEBSITE);

        // Get all the contactList where contactWebsite equals to UPDATED_CONTACT_WEBSITE
        defaultContactShouldNotBeFound("contactWebsite.in=" + UPDATED_CONTACT_WEBSITE);
    }

    @Test
    @Transactional
    public void getAllContactsByContactWebsiteIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactWebsite is not null
        defaultContactShouldBeFound("contactWebsite.specified=true");

        // Get all the contactList where contactWebsite is null
        defaultContactShouldNotBeFound("contactWebsite.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByContactAvatarUrlIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactAvatarUrl equals to DEFAULT_CONTACT_AVATAR_URL
        defaultContactShouldBeFound("contactAvatarUrl.equals=" + DEFAULT_CONTACT_AVATAR_URL);

        // Get all the contactList where contactAvatarUrl equals to UPDATED_CONTACT_AVATAR_URL
        defaultContactShouldNotBeFound("contactAvatarUrl.equals=" + UPDATED_CONTACT_AVATAR_URL);
    }

    @Test
    @Transactional
    public void getAllContactsByContactAvatarUrlIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactAvatarUrl in DEFAULT_CONTACT_AVATAR_URL or UPDATED_CONTACT_AVATAR_URL
        defaultContactShouldBeFound("contactAvatarUrl.in=" + DEFAULT_CONTACT_AVATAR_URL + "," + UPDATED_CONTACT_AVATAR_URL);

        // Get all the contactList where contactAvatarUrl equals to UPDATED_CONTACT_AVATAR_URL
        defaultContactShouldNotBeFound("contactAvatarUrl.in=" + UPDATED_CONTACT_AVATAR_URL);
    }

    @Test
    @Transactional
    public void getAllContactsByContactAvatarUrlIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactAvatarUrl is not null
        defaultContactShouldBeFound("contactAvatarUrl.specified=true");

        // Get all the contactList where contactAvatarUrl is null
        defaultContactShouldNotBeFound("contactAvatarUrl.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByContactFacebookIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactFacebook equals to DEFAULT_CONTACT_FACEBOOK
        defaultContactShouldBeFound("contactFacebook.equals=" + DEFAULT_CONTACT_FACEBOOK);

        // Get all the contactList where contactFacebook equals to UPDATED_CONTACT_FACEBOOK
        defaultContactShouldNotBeFound("contactFacebook.equals=" + UPDATED_CONTACT_FACEBOOK);
    }

    @Test
    @Transactional
    public void getAllContactsByContactFacebookIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactFacebook in DEFAULT_CONTACT_FACEBOOK or UPDATED_CONTACT_FACEBOOK
        defaultContactShouldBeFound("contactFacebook.in=" + DEFAULT_CONTACT_FACEBOOK + "," + UPDATED_CONTACT_FACEBOOK);

        // Get all the contactList where contactFacebook equals to UPDATED_CONTACT_FACEBOOK
        defaultContactShouldNotBeFound("contactFacebook.in=" + UPDATED_CONTACT_FACEBOOK);
    }

    @Test
    @Transactional
    public void getAllContactsByContactFacebookIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactFacebook is not null
        defaultContactShouldBeFound("contactFacebook.specified=true");

        // Get all the contactList where contactFacebook is null
        defaultContactShouldNotBeFound("contactFacebook.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByContactTwitterIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactTwitter equals to DEFAULT_CONTACT_TWITTER
        defaultContactShouldBeFound("contactTwitter.equals=" + DEFAULT_CONTACT_TWITTER);

        // Get all the contactList where contactTwitter equals to UPDATED_CONTACT_TWITTER
        defaultContactShouldNotBeFound("contactTwitter.equals=" + UPDATED_CONTACT_TWITTER);
    }

    @Test
    @Transactional
    public void getAllContactsByContactTwitterIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactTwitter in DEFAULT_CONTACT_TWITTER or UPDATED_CONTACT_TWITTER
        defaultContactShouldBeFound("contactTwitter.in=" + DEFAULT_CONTACT_TWITTER + "," + UPDATED_CONTACT_TWITTER);

        // Get all the contactList where contactTwitter equals to UPDATED_CONTACT_TWITTER
        defaultContactShouldNotBeFound("contactTwitter.in=" + UPDATED_CONTACT_TWITTER);
    }

    @Test
    @Transactional
    public void getAllContactsByContactTwitterIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactTwitter is not null
        defaultContactShouldBeFound("contactTwitter.specified=true");

        // Get all the contactList where contactTwitter is null
        defaultContactShouldNotBeFound("contactTwitter.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByContactInstagramIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactInstagram equals to DEFAULT_CONTACT_INSTAGRAM
        defaultContactShouldBeFound("contactInstagram.equals=" + DEFAULT_CONTACT_INSTAGRAM);

        // Get all the contactList where contactInstagram equals to UPDATED_CONTACT_INSTAGRAM
        defaultContactShouldNotBeFound("contactInstagram.equals=" + UPDATED_CONTACT_INSTAGRAM);
    }

    @Test
    @Transactional
    public void getAllContactsByContactInstagramIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactInstagram in DEFAULT_CONTACT_INSTAGRAM or UPDATED_CONTACT_INSTAGRAM
        defaultContactShouldBeFound("contactInstagram.in=" + DEFAULT_CONTACT_INSTAGRAM + "," + UPDATED_CONTACT_INSTAGRAM);

        // Get all the contactList where contactInstagram equals to UPDATED_CONTACT_INSTAGRAM
        defaultContactShouldNotBeFound("contactInstagram.in=" + UPDATED_CONTACT_INSTAGRAM);
    }

    @Test
    @Transactional
    public void getAllContactsByContactInstagramIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactInstagram is not null
        defaultContactShouldBeFound("contactInstagram.specified=true");

        // Get all the contactList where contactInstagram is null
        defaultContactShouldNotBeFound("contactInstagram.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByContactLinkedinIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactLinkedin equals to DEFAULT_CONTACT_LINKEDIN
        defaultContactShouldBeFound("contactLinkedin.equals=" + DEFAULT_CONTACT_LINKEDIN);

        // Get all the contactList where contactLinkedin equals to UPDATED_CONTACT_LINKEDIN
        defaultContactShouldNotBeFound("contactLinkedin.equals=" + UPDATED_CONTACT_LINKEDIN);
    }

    @Test
    @Transactional
    public void getAllContactsByContactLinkedinIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactLinkedin in DEFAULT_CONTACT_LINKEDIN or UPDATED_CONTACT_LINKEDIN
        defaultContactShouldBeFound("contactLinkedin.in=" + DEFAULT_CONTACT_LINKEDIN + "," + UPDATED_CONTACT_LINKEDIN);

        // Get all the contactList where contactLinkedin equals to UPDATED_CONTACT_LINKEDIN
        defaultContactShouldNotBeFound("contactLinkedin.in=" + UPDATED_CONTACT_LINKEDIN);
    }

    @Test
    @Transactional
    public void getAllContactsByContactLinkedinIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactLinkedin is not null
        defaultContactShouldBeFound("contactLinkedin.specified=true");

        // Get all the contactList where contactLinkedin is null
        defaultContactShouldNotBeFound("contactLinkedin.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByContactGooglePlusIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactGooglePlus equals to DEFAULT_CONTACT_GOOGLE_PLUS
        defaultContactShouldBeFound("contactGooglePlus.equals=" + DEFAULT_CONTACT_GOOGLE_PLUS);

        // Get all the contactList where contactGooglePlus equals to UPDATED_CONTACT_GOOGLE_PLUS
        defaultContactShouldNotBeFound("contactGooglePlus.equals=" + UPDATED_CONTACT_GOOGLE_PLUS);
    }

    @Test
    @Transactional
    public void getAllContactsByContactGooglePlusIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactGooglePlus in DEFAULT_CONTACT_GOOGLE_PLUS or UPDATED_CONTACT_GOOGLE_PLUS
        defaultContactShouldBeFound("contactGooglePlus.in=" + DEFAULT_CONTACT_GOOGLE_PLUS + "," + UPDATED_CONTACT_GOOGLE_PLUS);

        // Get all the contactList where contactGooglePlus equals to UPDATED_CONTACT_GOOGLE_PLUS
        defaultContactShouldNotBeFound("contactGooglePlus.in=" + UPDATED_CONTACT_GOOGLE_PLUS);
    }

    @Test
    @Transactional
    public void getAllContactsByContactGooglePlusIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactGooglePlus is not null
        defaultContactShouldBeFound("contactGooglePlus.specified=true");

        // Get all the contactList where contactGooglePlus is null
        defaultContactShouldNotBeFound("contactGooglePlus.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByContactYoutubeIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactYoutube equals to DEFAULT_CONTACT_YOUTUBE
        defaultContactShouldBeFound("contactYoutube.equals=" + DEFAULT_CONTACT_YOUTUBE);

        // Get all the contactList where contactYoutube equals to UPDATED_CONTACT_YOUTUBE
        defaultContactShouldNotBeFound("contactYoutube.equals=" + UPDATED_CONTACT_YOUTUBE);
    }

    @Test
    @Transactional
    public void getAllContactsByContactYoutubeIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactYoutube in DEFAULT_CONTACT_YOUTUBE or UPDATED_CONTACT_YOUTUBE
        defaultContactShouldBeFound("contactYoutube.in=" + DEFAULT_CONTACT_YOUTUBE + "," + UPDATED_CONTACT_YOUTUBE);

        // Get all the contactList where contactYoutube equals to UPDATED_CONTACT_YOUTUBE
        defaultContactShouldNotBeFound("contactYoutube.in=" + UPDATED_CONTACT_YOUTUBE);
    }

    @Test
    @Transactional
    public void getAllContactsByContactYoutubeIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactYoutube is not null
        defaultContactShouldBeFound("contactYoutube.specified=true");

        // Get all the contactList where contactYoutube is null
        defaultContactShouldNotBeFound("contactYoutube.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByContactStatusIsEqualToSomething() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactStatus equals to DEFAULT_CONTACT_STATUS
        defaultContactShouldBeFound("contactStatus.equals=" + DEFAULT_CONTACT_STATUS);

        // Get all the contactList where contactStatus equals to UPDATED_CONTACT_STATUS
        defaultContactShouldNotBeFound("contactStatus.equals=" + UPDATED_CONTACT_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactsByContactStatusIsInShouldWork() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactStatus in DEFAULT_CONTACT_STATUS or UPDATED_CONTACT_STATUS
        defaultContactShouldBeFound("contactStatus.in=" + DEFAULT_CONTACT_STATUS + "," + UPDATED_CONTACT_STATUS);

        // Get all the contactList where contactStatus equals to UPDATED_CONTACT_STATUS
        defaultContactShouldNotBeFound("contactStatus.in=" + UPDATED_CONTACT_STATUS);
    }

    @Test
    @Transactional
    public void getAllContactsByContactStatusIsNullOrNotNull() throws Exception {
        // Initialize the database
        contactRepository.saveAndFlush(contact);

        // Get all the contactList where contactStatus is not null
        defaultContactShouldBeFound("contactStatus.specified=true");

        // Get all the contactList where contactStatus is null
        defaultContactShouldNotBeFound("contactStatus.specified=false");
    }

    @Test
    @Transactional
    public void getAllContactsByUserIsEqualToSomething() throws Exception {
        // Initialize the database
        User user = UserResourceIntTest.createEntity(em);
        em.persist(user);
        em.flush();
        contact.setUser(user);
        contactRepository.saveAndFlush(contact);
        Long userId = user.getId();

        // Get all the contactList where user equals to userId
        defaultContactShouldBeFound("userId.equals=" + userId);

        // Get all the contactList where user equals to userId + 1
        defaultContactShouldNotBeFound("userId.equals=" + (userId + 1));
    }

    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultContactShouldBeFound(String filter) throws Exception {
        restContactMockMvc.perform(get("/api/contacts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(contact.getId().intValue())))
            .andExpect(jsonPath("$.[*].contactName").value(hasItem(DEFAULT_CONTACT_NAME.toString())))
            .andExpect(jsonPath("$.[*].contactPhone").value(hasItem(DEFAULT_CONTACT_PHONE.toString())))
            .andExpect(jsonPath("$.[*].contactAddress").value(hasItem(DEFAULT_CONTACT_ADDRESS.toString())))
            .andExpect(jsonPath("$.[*].contactWebsite").value(hasItem(DEFAULT_CONTACT_WEBSITE.toString())))
            .andExpect(jsonPath("$.[*].contactAvatarUrl").value(hasItem(DEFAULT_CONTACT_AVATAR_URL.toString())))
            .andExpect(jsonPath("$.[*].contactFacebook").value(hasItem(DEFAULT_CONTACT_FACEBOOK.toString())))
            .andExpect(jsonPath("$.[*].contactTwitter").value(hasItem(DEFAULT_CONTACT_TWITTER.toString())))
            .andExpect(jsonPath("$.[*].contactInstagram").value(hasItem(DEFAULT_CONTACT_INSTAGRAM.toString())))
            .andExpect(jsonPath("$.[*].contactLinkedin").value(hasItem(DEFAULT_CONTACT_LINKEDIN.toString())))
            .andExpect(jsonPath("$.[*].contactGooglePlus").value(hasItem(DEFAULT_CONTACT_GOOGLE_PLUS.toString())))
            .andExpect(jsonPath("$.[*].contactYoutube").value(hasItem(DEFAULT_CONTACT_YOUTUBE.toString())))
            .andExpect(jsonPath("$.[*].contactStatus").value(hasItem(DEFAULT_CONTACT_STATUS.booleanValue())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultContactShouldNotBeFound(String filter) throws Exception {
        restContactMockMvc.perform(get("/api/contacts?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingContact() throws Exception {
        // Get the contact
        restContactMockMvc.perform(get("/api/contacts/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateContact() throws Exception {
        // Initialize the database
        contactService.save(contact);

        int databaseSizeBeforeUpdate = contactRepository.findAll().size();

        // Update the contact
        Contact updatedContact = contactRepository.findById(contact.getId()).get();
        // Disconnect from session so that the updates on updatedContact are not directly saved in db
        em.detach(updatedContact);
        updatedContact
            .contactName(UPDATED_CONTACT_NAME)
            .contactPhone(UPDATED_CONTACT_PHONE)
            .contactAddress(UPDATED_CONTACT_ADDRESS)
            .contactWebsite(UPDATED_CONTACT_WEBSITE)
            .contactAvatarUrl(UPDATED_CONTACT_AVATAR_URL)
            .contactFacebook(UPDATED_CONTACT_FACEBOOK)
            .contactTwitter(UPDATED_CONTACT_TWITTER)
            .contactInstagram(UPDATED_CONTACT_INSTAGRAM)
            .contactLinkedin(UPDATED_CONTACT_LINKEDIN)
            .contactGooglePlus(UPDATED_CONTACT_GOOGLE_PLUS)
            .contactYoutube(UPDATED_CONTACT_YOUTUBE)
            .contactStatus(UPDATED_CONTACT_STATUS);

        restContactMockMvc.perform(put("/api/contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedContact)))
            .andExpect(status().isOk());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeUpdate);
        Contact testContact = contactList.get(contactList.size() - 1);
        assertThat(testContact.getContactName()).isEqualTo(UPDATED_CONTACT_NAME);
        assertThat(testContact.getContactPhone()).isEqualTo(UPDATED_CONTACT_PHONE);
        assertThat(testContact.getContactAddress()).isEqualTo(UPDATED_CONTACT_ADDRESS);
        assertThat(testContact.getContactWebsite()).isEqualTo(UPDATED_CONTACT_WEBSITE);
        assertThat(testContact.getContactAvatarUrl()).isEqualTo(UPDATED_CONTACT_AVATAR_URL);
        assertThat(testContact.getContactFacebook()).isEqualTo(UPDATED_CONTACT_FACEBOOK);
        assertThat(testContact.getContactTwitter()).isEqualTo(UPDATED_CONTACT_TWITTER);
        assertThat(testContact.getContactInstagram()).isEqualTo(UPDATED_CONTACT_INSTAGRAM);
        assertThat(testContact.getContactLinkedin()).isEqualTo(UPDATED_CONTACT_LINKEDIN);
        assertThat(testContact.getContactGooglePlus()).isEqualTo(UPDATED_CONTACT_GOOGLE_PLUS);
        assertThat(testContact.getContactYoutube()).isEqualTo(UPDATED_CONTACT_YOUTUBE);
        assertThat(testContact.isContactStatus()).isEqualTo(UPDATED_CONTACT_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingContact() throws Exception {
        int databaseSizeBeforeUpdate = contactRepository.findAll().size();

        // Create the Contact

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restContactMockMvc.perform(put("/api/contacts")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(contact)))
            .andExpect(status().isBadRequest());

        // Validate the Contact in the database
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteContact() throws Exception {
        // Initialize the database
        contactService.save(contact);

        int databaseSizeBeforeDelete = contactRepository.findAll().size();

        // Get the contact
        restContactMockMvc.perform(delete("/api/contacts/{id}", contact.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Contact> contactList = contactRepository.findAll();
        assertThat(contactList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Contact.class);
        Contact contact1 = new Contact();
        contact1.setId(1L);
        Contact contact2 = new Contact();
        contact2.setId(contact1.getId());
        assertThat(contact1).isEqualTo(contact2);
        contact2.setId(2L);
        assertThat(contact1).isNotEqualTo(contact2);
        contact1.setId(null);
        assertThat(contact1).isNotEqualTo(contact2);
    }
}
