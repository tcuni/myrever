package com.tcutma.realstate.web.rest;

import com.tcutma.realstate.RiverApp;

import com.tcutma.realstate.domain.Document;
import com.tcutma.realstate.repository.DocumentRepository;
import com.tcutma.realstate.service.DocumentService;
import com.tcutma.realstate.web.rest.errors.ExceptionTranslator;
import com.tcutma.realstate.service.dto.DocumentCriteria;
import com.tcutma.realstate.service.DocumentQueryService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.tcutma.realstate.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.tcutma.realstate.domain.enumeration.ResourceType;
/**
 * Test class for the DocumentResource REST controller.
 *
 * @see DocumentResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RiverApp.class)
public class DocumentResourceIntTest {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_SRC = "AAAAAAAAAA";
    private static final String UPDATED_SRC = "BBBBBBBBBB";

    private static final String DEFAULT_MIME_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_MIME_TYPE = "BBBBBBBBBB";

    private static final Long DEFAULT_FILE_SIZE = 1L;
    private static final Long UPDATED_FILE_SIZE = 2L;

    private static final ResourceType DEFAULT_RESOURCE_TYPE = ResourceType.PROJECT;
    private static final ResourceType UPDATED_RESOURCE_TYPE = ResourceType.PROPERTY;

    private static final Long DEFAULT_RESOURCE_ID = 1L;
    private static final Long UPDATED_RESOURCE_ID = 2L;

    private static final Integer DEFAULT_RESOURCE_ORDER = 1;
    private static final Integer UPDATED_RESOURCE_ORDER = 2;

    private static final String DEFAULT_THUMBNAIL = "AAAAAAAAAA";
    private static final String UPDATED_THUMBNAIL = "BBBBBBBBBB";

    private static final Integer DEFAULT_PAGES = 1;
    private static final Integer UPDATED_PAGES = 2;

    private static final String DEFAULT_CAPTION = "AAAAAAAAAA";
    private static final String UPDATED_CAPTION = "BBBBBBBBBB";

    private static final String DEFAULT_TAGS = "AAAAAAAAAA";
    private static final String UPDATED_TAGS = "BBBBBBBBBB";

    @Autowired
    private DocumentRepository documentRepository;

    

    @Autowired
    private DocumentService documentService;

    @Autowired
    private DocumentQueryService documentQueryService;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restDocumentMockMvc;

    private Document document;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final DocumentResource documentResource = new DocumentResource(documentService, documentQueryService);
        this.restDocumentMockMvc = MockMvcBuilders.standaloneSetup(documentResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Document createEntity(EntityManager em) {
        Document document = new Document()
            .name(DEFAULT_NAME)
            .src(DEFAULT_SRC)
            .mimeType(DEFAULT_MIME_TYPE)
            .fileSize(DEFAULT_FILE_SIZE)
            .resourceType(DEFAULT_RESOURCE_TYPE)
            .resourceId(DEFAULT_RESOURCE_ID)
            .resourceOrder(DEFAULT_RESOURCE_ORDER)
            .thumbnail(DEFAULT_THUMBNAIL)
            .pages(DEFAULT_PAGES)
            .caption(DEFAULT_CAPTION)
            .tags(DEFAULT_TAGS);
        return document;
    }

    @Before
    public void initTest() {
        document = createEntity(em);
    }

    @Test
    @Transactional
    public void createDocument() throws Exception {
        int databaseSizeBeforeCreate = documentRepository.findAll().size();

        // Create the Document
        restDocumentMockMvc.perform(post("/api/documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(document)))
            .andExpect(status().isCreated());

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll();
        assertThat(documentList).hasSize(databaseSizeBeforeCreate + 1);
        Document testDocument = documentList.get(documentList.size() - 1);
        assertThat(testDocument.getName()).isEqualTo(DEFAULT_NAME);
        assertThat(testDocument.getSrc()).isEqualTo(DEFAULT_SRC);
        assertThat(testDocument.getMimeType()).isEqualTo(DEFAULT_MIME_TYPE);
        assertThat(testDocument.getFileSize()).isEqualTo(DEFAULT_FILE_SIZE);
        assertThat(testDocument.getResourceType()).isEqualTo(DEFAULT_RESOURCE_TYPE);
        assertThat(testDocument.getResourceId()).isEqualTo(DEFAULT_RESOURCE_ID);
        assertThat(testDocument.getResourceOrder()).isEqualTo(DEFAULT_RESOURCE_ORDER);
        assertThat(testDocument.getThumbnail()).isEqualTo(DEFAULT_THUMBNAIL);
        assertThat(testDocument.getPages()).isEqualTo(DEFAULT_PAGES);
        assertThat(testDocument.getCaption()).isEqualTo(DEFAULT_CAPTION);
        assertThat(testDocument.getTags()).isEqualTo(DEFAULT_TAGS);
    }

    @Test
    @Transactional
    public void createDocumentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = documentRepository.findAll().size();

        // Create the Document with an existing ID
        document.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDocumentMockMvc.perform(post("/api/documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(document)))
            .andExpect(status().isBadRequest());

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll();
        assertThat(documentList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkNameIsRequired() throws Exception {
        int databaseSizeBeforeTest = documentRepository.findAll().size();
        // set the field null
        document.setName(null);

        // Create the Document, which fails.

        restDocumentMockMvc.perform(post("/api/documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(document)))
            .andExpect(status().isBadRequest());

        List<Document> documentList = documentRepository.findAll();
        assertThat(documentList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllDocuments() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList
        restDocumentMockMvc.perform(get("/api/documents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(document.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].src").value(hasItem(DEFAULT_SRC.toString())))
            .andExpect(jsonPath("$.[*].mimeType").value(hasItem(DEFAULT_MIME_TYPE.toString())))
            .andExpect(jsonPath("$.[*].fileSize").value(hasItem(DEFAULT_FILE_SIZE.intValue())))
            .andExpect(jsonPath("$.[*].resourceType").value(hasItem(DEFAULT_RESOURCE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].resourceId").value(hasItem(DEFAULT_RESOURCE_ID.intValue())))
            .andExpect(jsonPath("$.[*].resourceOrder").value(hasItem(DEFAULT_RESOURCE_ORDER)))
            .andExpect(jsonPath("$.[*].thumbnail").value(hasItem(DEFAULT_THUMBNAIL.toString())))
            .andExpect(jsonPath("$.[*].pages").value(hasItem(DEFAULT_PAGES)))
            .andExpect(jsonPath("$.[*].caption").value(hasItem(DEFAULT_CAPTION.toString())))
            .andExpect(jsonPath("$.[*].tags").value(hasItem(DEFAULT_TAGS.toString())));
    }
    

    @Test
    @Transactional
    public void getDocument() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get the document
        restDocumentMockMvc.perform(get("/api/documents/{id}", document.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(document.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()))
            .andExpect(jsonPath("$.src").value(DEFAULT_SRC.toString()))
            .andExpect(jsonPath("$.mimeType").value(DEFAULT_MIME_TYPE.toString()))
            .andExpect(jsonPath("$.fileSize").value(DEFAULT_FILE_SIZE.intValue()))
            .andExpect(jsonPath("$.resourceType").value(DEFAULT_RESOURCE_TYPE.toString()))
            .andExpect(jsonPath("$.resourceId").value(DEFAULT_RESOURCE_ID.intValue()))
            .andExpect(jsonPath("$.resourceOrder").value(DEFAULT_RESOURCE_ORDER))
            .andExpect(jsonPath("$.thumbnail").value(DEFAULT_THUMBNAIL.toString()))
            .andExpect(jsonPath("$.pages").value(DEFAULT_PAGES))
            .andExpect(jsonPath("$.caption").value(DEFAULT_CAPTION.toString()))
            .andExpect(jsonPath("$.tags").value(DEFAULT_TAGS.toString()));
    }

    @Test
    @Transactional
    public void getAllDocumentsByNameIsEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where name equals to DEFAULT_NAME
        defaultDocumentShouldBeFound("name.equals=" + DEFAULT_NAME);

        // Get all the documentList where name equals to UPDATED_NAME
        defaultDocumentShouldNotBeFound("name.equals=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllDocumentsByNameIsInShouldWork() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where name in DEFAULT_NAME or UPDATED_NAME
        defaultDocumentShouldBeFound("name.in=" + DEFAULT_NAME + "," + UPDATED_NAME);

        // Get all the documentList where name equals to UPDATED_NAME
        defaultDocumentShouldNotBeFound("name.in=" + UPDATED_NAME);
    }

    @Test
    @Transactional
    public void getAllDocumentsByNameIsNullOrNotNull() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where name is not null
        defaultDocumentShouldBeFound("name.specified=true");

        // Get all the documentList where name is null
        defaultDocumentShouldNotBeFound("name.specified=false");
    }

    @Test
    @Transactional
    public void getAllDocumentsBySrcIsEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where src equals to DEFAULT_SRC
        defaultDocumentShouldBeFound("src.equals=" + DEFAULT_SRC);

        // Get all the documentList where src equals to UPDATED_SRC
        defaultDocumentShouldNotBeFound("src.equals=" + UPDATED_SRC);
    }

    @Test
    @Transactional
    public void getAllDocumentsBySrcIsInShouldWork() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where src in DEFAULT_SRC or UPDATED_SRC
        defaultDocumentShouldBeFound("src.in=" + DEFAULT_SRC + "," + UPDATED_SRC);

        // Get all the documentList where src equals to UPDATED_SRC
        defaultDocumentShouldNotBeFound("src.in=" + UPDATED_SRC);
    }

    @Test
    @Transactional
    public void getAllDocumentsBySrcIsNullOrNotNull() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where src is not null
        defaultDocumentShouldBeFound("src.specified=true");

        // Get all the documentList where src is null
        defaultDocumentShouldNotBeFound("src.specified=false");
    }

    @Test
    @Transactional
    public void getAllDocumentsByMimeTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where mimeType equals to DEFAULT_MIME_TYPE
        defaultDocumentShouldBeFound("mimeType.equals=" + DEFAULT_MIME_TYPE);

        // Get all the documentList where mimeType equals to UPDATED_MIME_TYPE
        defaultDocumentShouldNotBeFound("mimeType.equals=" + UPDATED_MIME_TYPE);
    }

    @Test
    @Transactional
    public void getAllDocumentsByMimeTypeIsInShouldWork() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where mimeType in DEFAULT_MIME_TYPE or UPDATED_MIME_TYPE
        defaultDocumentShouldBeFound("mimeType.in=" + DEFAULT_MIME_TYPE + "," + UPDATED_MIME_TYPE);

        // Get all the documentList where mimeType equals to UPDATED_MIME_TYPE
        defaultDocumentShouldNotBeFound("mimeType.in=" + UPDATED_MIME_TYPE);
    }

    @Test
    @Transactional
    public void getAllDocumentsByMimeTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where mimeType is not null
        defaultDocumentShouldBeFound("mimeType.specified=true");

        // Get all the documentList where mimeType is null
        defaultDocumentShouldNotBeFound("mimeType.specified=false");
    }

    @Test
    @Transactional
    public void getAllDocumentsByFileSizeIsEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where fileSize equals to DEFAULT_FILE_SIZE
        defaultDocumentShouldBeFound("fileSize.equals=" + DEFAULT_FILE_SIZE);

        // Get all the documentList where fileSize equals to UPDATED_FILE_SIZE
        defaultDocumentShouldNotBeFound("fileSize.equals=" + UPDATED_FILE_SIZE);
    }

    @Test
    @Transactional
    public void getAllDocumentsByFileSizeIsInShouldWork() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where fileSize in DEFAULT_FILE_SIZE or UPDATED_FILE_SIZE
        defaultDocumentShouldBeFound("fileSize.in=" + DEFAULT_FILE_SIZE + "," + UPDATED_FILE_SIZE);

        // Get all the documentList where fileSize equals to UPDATED_FILE_SIZE
        defaultDocumentShouldNotBeFound("fileSize.in=" + UPDATED_FILE_SIZE);
    }

    @Test
    @Transactional
    public void getAllDocumentsByFileSizeIsNullOrNotNull() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where fileSize is not null
        defaultDocumentShouldBeFound("fileSize.specified=true");

        // Get all the documentList where fileSize is null
        defaultDocumentShouldNotBeFound("fileSize.specified=false");
    }

    @Test
    @Transactional
    public void getAllDocumentsByFileSizeIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where fileSize greater than or equals to DEFAULT_FILE_SIZE
        defaultDocumentShouldBeFound("fileSize.greaterOrEqualThan=" + DEFAULT_FILE_SIZE);

        // Get all the documentList where fileSize greater than or equals to UPDATED_FILE_SIZE
        defaultDocumentShouldNotBeFound("fileSize.greaterOrEqualThan=" + UPDATED_FILE_SIZE);
    }

    @Test
    @Transactional
    public void getAllDocumentsByFileSizeIsLessThanSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where fileSize less than or equals to DEFAULT_FILE_SIZE
        defaultDocumentShouldNotBeFound("fileSize.lessThan=" + DEFAULT_FILE_SIZE);

        // Get all the documentList where fileSize less than or equals to UPDATED_FILE_SIZE
        defaultDocumentShouldBeFound("fileSize.lessThan=" + UPDATED_FILE_SIZE);
    }


    @Test
    @Transactional
    public void getAllDocumentsByResourceTypeIsEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceType equals to DEFAULT_RESOURCE_TYPE
        defaultDocumentShouldBeFound("resourceType.equals=" + DEFAULT_RESOURCE_TYPE);

        // Get all the documentList where resourceType equals to UPDATED_RESOURCE_TYPE
        defaultDocumentShouldNotBeFound("resourceType.equals=" + UPDATED_RESOURCE_TYPE);
    }

    @Test
    @Transactional
    public void getAllDocumentsByResourceTypeIsInShouldWork() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceType in DEFAULT_RESOURCE_TYPE or UPDATED_RESOURCE_TYPE
        defaultDocumentShouldBeFound("resourceType.in=" + DEFAULT_RESOURCE_TYPE + "," + UPDATED_RESOURCE_TYPE);

        // Get all the documentList where resourceType equals to UPDATED_RESOURCE_TYPE
        defaultDocumentShouldNotBeFound("resourceType.in=" + UPDATED_RESOURCE_TYPE);
    }

    @Test
    @Transactional
    public void getAllDocumentsByResourceTypeIsNullOrNotNull() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceType is not null
        defaultDocumentShouldBeFound("resourceType.specified=true");

        // Get all the documentList where resourceType is null
        defaultDocumentShouldNotBeFound("resourceType.specified=false");
    }

    @Test
    @Transactional
    public void getAllDocumentsByResourceIdIsEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceId equals to DEFAULT_RESOURCE_ID
        defaultDocumentShouldBeFound("resourceId.equals=" + DEFAULT_RESOURCE_ID);

        // Get all the documentList where resourceId equals to UPDATED_RESOURCE_ID
        defaultDocumentShouldNotBeFound("resourceId.equals=" + UPDATED_RESOURCE_ID);
    }

    @Test
    @Transactional
    public void getAllDocumentsByResourceIdIsInShouldWork() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceId in DEFAULT_RESOURCE_ID or UPDATED_RESOURCE_ID
        defaultDocumentShouldBeFound("resourceId.in=" + DEFAULT_RESOURCE_ID + "," + UPDATED_RESOURCE_ID);

        // Get all the documentList where resourceId equals to UPDATED_RESOURCE_ID
        defaultDocumentShouldNotBeFound("resourceId.in=" + UPDATED_RESOURCE_ID);
    }

    @Test
    @Transactional
    public void getAllDocumentsByResourceIdIsNullOrNotNull() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceId is not null
        defaultDocumentShouldBeFound("resourceId.specified=true");

        // Get all the documentList where resourceId is null
        defaultDocumentShouldNotBeFound("resourceId.specified=false");
    }

    @Test
    @Transactional
    public void getAllDocumentsByResourceIdIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceId greater than or equals to DEFAULT_RESOURCE_ID
        defaultDocumentShouldBeFound("resourceId.greaterOrEqualThan=" + DEFAULT_RESOURCE_ID);

        // Get all the documentList where resourceId greater than or equals to UPDATED_RESOURCE_ID
        defaultDocumentShouldNotBeFound("resourceId.greaterOrEqualThan=" + UPDATED_RESOURCE_ID);
    }

    @Test
    @Transactional
    public void getAllDocumentsByResourceIdIsLessThanSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceId less than or equals to DEFAULT_RESOURCE_ID
        defaultDocumentShouldNotBeFound("resourceId.lessThan=" + DEFAULT_RESOURCE_ID);

        // Get all the documentList where resourceId less than or equals to UPDATED_RESOURCE_ID
        defaultDocumentShouldBeFound("resourceId.lessThan=" + UPDATED_RESOURCE_ID);
    }


    @Test
    @Transactional
    public void getAllDocumentsByResourceOrderIsEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceOrder equals to DEFAULT_RESOURCE_ORDER
        defaultDocumentShouldBeFound("resourceOrder.equals=" + DEFAULT_RESOURCE_ORDER);

        // Get all the documentList where resourceOrder equals to UPDATED_RESOURCE_ORDER
        defaultDocumentShouldNotBeFound("resourceOrder.equals=" + UPDATED_RESOURCE_ORDER);
    }

    @Test
    @Transactional
    public void getAllDocumentsByResourceOrderIsInShouldWork() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceOrder in DEFAULT_RESOURCE_ORDER or UPDATED_RESOURCE_ORDER
        defaultDocumentShouldBeFound("resourceOrder.in=" + DEFAULT_RESOURCE_ORDER + "," + UPDATED_RESOURCE_ORDER);

        // Get all the documentList where resourceOrder equals to UPDATED_RESOURCE_ORDER
        defaultDocumentShouldNotBeFound("resourceOrder.in=" + UPDATED_RESOURCE_ORDER);
    }

    @Test
    @Transactional
    public void getAllDocumentsByResourceOrderIsNullOrNotNull() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceOrder is not null
        defaultDocumentShouldBeFound("resourceOrder.specified=true");

        // Get all the documentList where resourceOrder is null
        defaultDocumentShouldNotBeFound("resourceOrder.specified=false");
    }

    @Test
    @Transactional
    public void getAllDocumentsByResourceOrderIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceOrder greater than or equals to DEFAULT_RESOURCE_ORDER
        defaultDocumentShouldBeFound("resourceOrder.greaterOrEqualThan=" + DEFAULT_RESOURCE_ORDER);

        // Get all the documentList where resourceOrder greater than or equals to UPDATED_RESOURCE_ORDER
        defaultDocumentShouldNotBeFound("resourceOrder.greaterOrEqualThan=" + UPDATED_RESOURCE_ORDER);
    }

    @Test
    @Transactional
    public void getAllDocumentsByResourceOrderIsLessThanSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where resourceOrder less than or equals to DEFAULT_RESOURCE_ORDER
        defaultDocumentShouldNotBeFound("resourceOrder.lessThan=" + DEFAULT_RESOURCE_ORDER);

        // Get all the documentList where resourceOrder less than or equals to UPDATED_RESOURCE_ORDER
        defaultDocumentShouldBeFound("resourceOrder.lessThan=" + UPDATED_RESOURCE_ORDER);
    }


    @Test
    @Transactional
    public void getAllDocumentsByThumbnailIsEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where thumbnail equals to DEFAULT_THUMBNAIL
        defaultDocumentShouldBeFound("thumbnail.equals=" + DEFAULT_THUMBNAIL);

        // Get all the documentList where thumbnail equals to UPDATED_THUMBNAIL
        defaultDocumentShouldNotBeFound("thumbnail.equals=" + UPDATED_THUMBNAIL);
    }

    @Test
    @Transactional
    public void getAllDocumentsByThumbnailIsInShouldWork() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where thumbnail in DEFAULT_THUMBNAIL or UPDATED_THUMBNAIL
        defaultDocumentShouldBeFound("thumbnail.in=" + DEFAULT_THUMBNAIL + "," + UPDATED_THUMBNAIL);

        // Get all the documentList where thumbnail equals to UPDATED_THUMBNAIL
        defaultDocumentShouldNotBeFound("thumbnail.in=" + UPDATED_THUMBNAIL);
    }

    @Test
    @Transactional
    public void getAllDocumentsByThumbnailIsNullOrNotNull() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where thumbnail is not null
        defaultDocumentShouldBeFound("thumbnail.specified=true");

        // Get all the documentList where thumbnail is null
        defaultDocumentShouldNotBeFound("thumbnail.specified=false");
    }

    @Test
    @Transactional
    public void getAllDocumentsByPagesIsEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where pages equals to DEFAULT_PAGES
        defaultDocumentShouldBeFound("pages.equals=" + DEFAULT_PAGES);

        // Get all the documentList where pages equals to UPDATED_PAGES
        defaultDocumentShouldNotBeFound("pages.equals=" + UPDATED_PAGES);
    }

    @Test
    @Transactional
    public void getAllDocumentsByPagesIsInShouldWork() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where pages in DEFAULT_PAGES or UPDATED_PAGES
        defaultDocumentShouldBeFound("pages.in=" + DEFAULT_PAGES + "," + UPDATED_PAGES);

        // Get all the documentList where pages equals to UPDATED_PAGES
        defaultDocumentShouldNotBeFound("pages.in=" + UPDATED_PAGES);
    }

    @Test
    @Transactional
    public void getAllDocumentsByPagesIsNullOrNotNull() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where pages is not null
        defaultDocumentShouldBeFound("pages.specified=true");

        // Get all the documentList where pages is null
        defaultDocumentShouldNotBeFound("pages.specified=false");
    }

    @Test
    @Transactional
    public void getAllDocumentsByPagesIsGreaterThanOrEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where pages greater than or equals to DEFAULT_PAGES
        defaultDocumentShouldBeFound("pages.greaterOrEqualThan=" + DEFAULT_PAGES);

        // Get all the documentList where pages greater than or equals to UPDATED_PAGES
        defaultDocumentShouldNotBeFound("pages.greaterOrEqualThan=" + UPDATED_PAGES);
    }

    @Test
    @Transactional
    public void getAllDocumentsByPagesIsLessThanSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where pages less than or equals to DEFAULT_PAGES
        defaultDocumentShouldNotBeFound("pages.lessThan=" + DEFAULT_PAGES);

        // Get all the documentList where pages less than or equals to UPDATED_PAGES
        defaultDocumentShouldBeFound("pages.lessThan=" + UPDATED_PAGES);
    }


    @Test
    @Transactional
    public void getAllDocumentsByCaptionIsEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where caption equals to DEFAULT_CAPTION
        defaultDocumentShouldBeFound("caption.equals=" + DEFAULT_CAPTION);

        // Get all the documentList where caption equals to UPDATED_CAPTION
        defaultDocumentShouldNotBeFound("caption.equals=" + UPDATED_CAPTION);
    }

    @Test
    @Transactional
    public void getAllDocumentsByCaptionIsInShouldWork() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where caption in DEFAULT_CAPTION or UPDATED_CAPTION
        defaultDocumentShouldBeFound("caption.in=" + DEFAULT_CAPTION + "," + UPDATED_CAPTION);

        // Get all the documentList where caption equals to UPDATED_CAPTION
        defaultDocumentShouldNotBeFound("caption.in=" + UPDATED_CAPTION);
    }

    @Test
    @Transactional
    public void getAllDocumentsByCaptionIsNullOrNotNull() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where caption is not null
        defaultDocumentShouldBeFound("caption.specified=true");

        // Get all the documentList where caption is null
        defaultDocumentShouldNotBeFound("caption.specified=false");
    }

    @Test
    @Transactional
    public void getAllDocumentsByTagsIsEqualToSomething() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where tags equals to DEFAULT_TAGS
        defaultDocumentShouldBeFound("tags.equals=" + DEFAULT_TAGS);

        // Get all the documentList where tags equals to UPDATED_TAGS
        defaultDocumentShouldNotBeFound("tags.equals=" + UPDATED_TAGS);
    }

    @Test
    @Transactional
    public void getAllDocumentsByTagsIsInShouldWork() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where tags in DEFAULT_TAGS or UPDATED_TAGS
        defaultDocumentShouldBeFound("tags.in=" + DEFAULT_TAGS + "," + UPDATED_TAGS);

        // Get all the documentList where tags equals to UPDATED_TAGS
        defaultDocumentShouldNotBeFound("tags.in=" + UPDATED_TAGS);
    }

    @Test
    @Transactional
    public void getAllDocumentsByTagsIsNullOrNotNull() throws Exception {
        // Initialize the database
        documentRepository.saveAndFlush(document);

        // Get all the documentList where tags is not null
        defaultDocumentShouldBeFound("tags.specified=true");

        // Get all the documentList where tags is null
        defaultDocumentShouldNotBeFound("tags.specified=false");
    }
    /**
     * Executes the search, and checks that the default entity is returned
     */
    private void defaultDocumentShouldBeFound(String filter) throws Exception {
        restDocumentMockMvc.perform(get("/api/documents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(document.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())))
            .andExpect(jsonPath("$.[*].src").value(hasItem(DEFAULT_SRC.toString())))
            .andExpect(jsonPath("$.[*].mimeType").value(hasItem(DEFAULT_MIME_TYPE.toString())))
            .andExpect(jsonPath("$.[*].fileSize").value(hasItem(DEFAULT_FILE_SIZE.intValue())))
            .andExpect(jsonPath("$.[*].resourceType").value(hasItem(DEFAULT_RESOURCE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].resourceId").value(hasItem(DEFAULT_RESOURCE_ID.intValue())))
            .andExpect(jsonPath("$.[*].resourceOrder").value(hasItem(DEFAULT_RESOURCE_ORDER)))
            .andExpect(jsonPath("$.[*].thumbnail").value(hasItem(DEFAULT_THUMBNAIL.toString())))
            .andExpect(jsonPath("$.[*].pages").value(hasItem(DEFAULT_PAGES)))
            .andExpect(jsonPath("$.[*].caption").value(hasItem(DEFAULT_CAPTION.toString())))
            .andExpect(jsonPath("$.[*].tags").value(hasItem(DEFAULT_TAGS.toString())));
    }

    /**
     * Executes the search, and checks that the default entity is not returned
     */
    private void defaultDocumentShouldNotBeFound(String filter) throws Exception {
        restDocumentMockMvc.perform(get("/api/documents?sort=id,desc&" + filter))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$").isArray())
            .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    @Transactional
    public void getNonExistingDocument() throws Exception {
        // Get the document
        restDocumentMockMvc.perform(get("/api/documents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDocument() throws Exception {
        // Initialize the database
        documentService.save(document);

        int databaseSizeBeforeUpdate = documentRepository.findAll().size();

        // Update the document
        Document updatedDocument = documentRepository.findById(document.getId()).get();
        // Disconnect from session so that the updates on updatedDocument are not directly saved in db
        em.detach(updatedDocument);
        updatedDocument
            .name(UPDATED_NAME)
            .src(UPDATED_SRC)
            .mimeType(UPDATED_MIME_TYPE)
            .fileSize(UPDATED_FILE_SIZE)
            .resourceType(UPDATED_RESOURCE_TYPE)
            .resourceId(UPDATED_RESOURCE_ID)
            .resourceOrder(UPDATED_RESOURCE_ORDER)
            .thumbnail(UPDATED_THUMBNAIL)
            .pages(UPDATED_PAGES)
            .caption(UPDATED_CAPTION)
            .tags(UPDATED_TAGS);

        restDocumentMockMvc.perform(put("/api/documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedDocument)))
            .andExpect(status().isOk());

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll();
        assertThat(documentList).hasSize(databaseSizeBeforeUpdate);
        Document testDocument = documentList.get(documentList.size() - 1);
        assertThat(testDocument.getName()).isEqualTo(UPDATED_NAME);
        assertThat(testDocument.getSrc()).isEqualTo(UPDATED_SRC);
        assertThat(testDocument.getMimeType()).isEqualTo(UPDATED_MIME_TYPE);
        assertThat(testDocument.getFileSize()).isEqualTo(UPDATED_FILE_SIZE);
        assertThat(testDocument.getResourceType()).isEqualTo(UPDATED_RESOURCE_TYPE);
        assertThat(testDocument.getResourceId()).isEqualTo(UPDATED_RESOURCE_ID);
        assertThat(testDocument.getResourceOrder()).isEqualTo(UPDATED_RESOURCE_ORDER);
        assertThat(testDocument.getThumbnail()).isEqualTo(UPDATED_THUMBNAIL);
        assertThat(testDocument.getPages()).isEqualTo(UPDATED_PAGES);
        assertThat(testDocument.getCaption()).isEqualTo(UPDATED_CAPTION);
        assertThat(testDocument.getTags()).isEqualTo(UPDATED_TAGS);
    }

    @Test
    @Transactional
    public void updateNonExistingDocument() throws Exception {
        int databaseSizeBeforeUpdate = documentRepository.findAll().size();

        // Create the Document

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restDocumentMockMvc.perform(put("/api/documents")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(document)))
            .andExpect(status().isBadRequest());

        // Validate the Document in the database
        List<Document> documentList = documentRepository.findAll();
        assertThat(documentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDocument() throws Exception {
        // Initialize the database
        documentService.save(document);

        int databaseSizeBeforeDelete = documentRepository.findAll().size();

        // Get the document
        restDocumentMockMvc.perform(delete("/api/documents/{id}", document.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Document> documentList = documentRepository.findAll();
        assertThat(documentList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Document.class);
        Document document1 = new Document();
        document1.setId(1L);
        Document document2 = new Document();
        document2.setId(document1.getId());
        assertThat(document1).isEqualTo(document2);
        document2.setId(2L);
        assertThat(document1).isNotEqualTo(document2);
        document1.setId(null);
        assertThat(document1).isNotEqualTo(document2);
    }
}
