package com.tcutma.realstate.web.rest;

import com.tcutma.realstate.RiverApp;

import com.tcutma.realstate.domain.FavouriteItem;
import com.tcutma.realstate.repository.FavouriteItemRepository;
import com.tcutma.realstate.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;


import static com.tcutma.realstate.web.rest.TestUtil.createFormattingConversionService;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.tcutma.realstate.domain.enumeration.ResourceType;
/**
 * Test class for the FavouriteItemResource REST controller.
 *
 * @see FavouriteItemResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = RiverApp.class)
public class FavouriteItemResourceIntTest {

    private static final Boolean DEFAULT_FAVOURITED = false;
    private static final Boolean UPDATED_FAVOURITED = true;

    private static final Long DEFAULT_RESOURCE_ID = 1L;
    private static final Long UPDATED_RESOURCE_ID = 2L;

    private static final ResourceType DEFAULT_RESOURCE_TYPE = ResourceType.PROJECT;
    private static final ResourceType UPDATED_RESOURCE_TYPE = ResourceType.PROPERTY;

    @Autowired
    private FavouriteItemRepository favouriteItemRepository;


    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restFavouriteItemMockMvc;

    private FavouriteItem favouriteItem;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        final FavouriteItemResource favouriteItemResource = new FavouriteItemResource(favouriteItemRepository);
        this.restFavouriteItemMockMvc = MockMvcBuilders.standaloneSetup(favouriteItemResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setConversionService(createFormattingConversionService())
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FavouriteItem createEntity(EntityManager em) {
        FavouriteItem favouriteItem = new FavouriteItem()
            .favourited(DEFAULT_FAVOURITED)
            .resourceId(DEFAULT_RESOURCE_ID)
            .resourceType(DEFAULT_RESOURCE_TYPE);
        return favouriteItem;
    }

    @Before
    public void initTest() {
        favouriteItem = createEntity(em);
    }

    @Test
    @Transactional
    public void createFavouriteItem() throws Exception {
        int databaseSizeBeforeCreate = favouriteItemRepository.findAll().size();

        // Create the FavouriteItem
        restFavouriteItemMockMvc.perform(post("/api/favourite-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favouriteItem)))
            .andExpect(status().isCreated());

        // Validate the FavouriteItem in the database
        List<FavouriteItem> favouriteItemList = favouriteItemRepository.findAll();
        assertThat(favouriteItemList).hasSize(databaseSizeBeforeCreate + 1);
        FavouriteItem testFavouriteItem = favouriteItemList.get(favouriteItemList.size() - 1);
        assertThat(testFavouriteItem.isFavourited()).isEqualTo(DEFAULT_FAVOURITED);
        assertThat(testFavouriteItem.getResourceId()).isEqualTo(DEFAULT_RESOURCE_ID);
        assertThat(testFavouriteItem.getResourceType()).isEqualTo(DEFAULT_RESOURCE_TYPE);
    }

    @Test
    @Transactional
    public void createFavouriteItemWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = favouriteItemRepository.findAll().size();

        // Create the FavouriteItem with an existing ID
        favouriteItem.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFavouriteItemMockMvc.perform(post("/api/favourite-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favouriteItem)))
            .andExpect(status().isBadRequest());

        // Validate the FavouriteItem in the database
        List<FavouriteItem> favouriteItemList = favouriteItemRepository.findAll();
        assertThat(favouriteItemList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllFavouriteItems() throws Exception {
        // Initialize the database
        favouriteItemRepository.saveAndFlush(favouriteItem);

        // Get all the favouriteItemList
        restFavouriteItemMockMvc.perform(get("/api/favourite-items?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(favouriteItem.getId().intValue())))
            .andExpect(jsonPath("$.[*].favourited").value(hasItem(DEFAULT_FAVOURITED.booleanValue())))
            .andExpect(jsonPath("$.[*].resourceId").value(hasItem(DEFAULT_RESOURCE_ID.intValue())))
            .andExpect(jsonPath("$.[*].resourceType").value(hasItem(DEFAULT_RESOURCE_TYPE.toString())));
    }
    

    @Test
    @Transactional
    public void getFavouriteItem() throws Exception {
        // Initialize the database
        favouriteItemRepository.saveAndFlush(favouriteItem);

        // Get the favouriteItem
        restFavouriteItemMockMvc.perform(get("/api/favourite-items/{id}", favouriteItem.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(favouriteItem.getId().intValue()))
            .andExpect(jsonPath("$.favourited").value(DEFAULT_FAVOURITED.booleanValue()))
            .andExpect(jsonPath("$.resourceId").value(DEFAULT_RESOURCE_ID.intValue()))
            .andExpect(jsonPath("$.resourceType").value(DEFAULT_RESOURCE_TYPE.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingFavouriteItem() throws Exception {
        // Get the favouriteItem
        restFavouriteItemMockMvc.perform(get("/api/favourite-items/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFavouriteItem() throws Exception {
        // Initialize the database
        favouriteItemRepository.saveAndFlush(favouriteItem);

        int databaseSizeBeforeUpdate = favouriteItemRepository.findAll().size();

        // Update the favouriteItem
        FavouriteItem updatedFavouriteItem = favouriteItemRepository.findById(favouriteItem.getId()).get();
        // Disconnect from session so that the updates on updatedFavouriteItem are not directly saved in db
        em.detach(updatedFavouriteItem);
        updatedFavouriteItem
            .favourited(UPDATED_FAVOURITED)
            .resourceId(UPDATED_RESOURCE_ID)
            .resourceType(UPDATED_RESOURCE_TYPE);

        restFavouriteItemMockMvc.perform(put("/api/favourite-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedFavouriteItem)))
            .andExpect(status().isOk());

        // Validate the FavouriteItem in the database
        List<FavouriteItem> favouriteItemList = favouriteItemRepository.findAll();
        assertThat(favouriteItemList).hasSize(databaseSizeBeforeUpdate);
        FavouriteItem testFavouriteItem = favouriteItemList.get(favouriteItemList.size() - 1);
        assertThat(testFavouriteItem.isFavourited()).isEqualTo(UPDATED_FAVOURITED);
        assertThat(testFavouriteItem.getResourceId()).isEqualTo(UPDATED_RESOURCE_ID);
        assertThat(testFavouriteItem.getResourceType()).isEqualTo(UPDATED_RESOURCE_TYPE);
    }

    @Test
    @Transactional
    public void updateNonExistingFavouriteItem() throws Exception {
        int databaseSizeBeforeUpdate = favouriteItemRepository.findAll().size();

        // Create the FavouriteItem

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restFavouriteItemMockMvc.perform(put("/api/favourite-items")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(favouriteItem)))
            .andExpect(status().isBadRequest());

        // Validate the FavouriteItem in the database
        List<FavouriteItem> favouriteItemList = favouriteItemRepository.findAll();
        assertThat(favouriteItemList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFavouriteItem() throws Exception {
        // Initialize the database
        favouriteItemRepository.saveAndFlush(favouriteItem);

        int databaseSizeBeforeDelete = favouriteItemRepository.findAll().size();

        // Get the favouriteItem
        restFavouriteItemMockMvc.perform(delete("/api/favourite-items/{id}", favouriteItem.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<FavouriteItem> favouriteItemList = favouriteItemRepository.findAll();
        assertThat(favouriteItemList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(FavouriteItem.class);
        FavouriteItem favouriteItem1 = new FavouriteItem();
        favouriteItem1.setId(1L);
        FavouriteItem favouriteItem2 = new FavouriteItem();
        favouriteItem2.setId(favouriteItem1.getId());
        assertThat(favouriteItem1).isEqualTo(favouriteItem2);
        favouriteItem2.setId(2L);
        assertThat(favouriteItem1).isNotEqualTo(favouriteItem2);
        favouriteItem1.setId(null);
        assertThat(favouriteItem1).isNotEqualTo(favouriteItem2);
    }
}
